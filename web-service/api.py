#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import os, re
import cherrypy
import simplejson
import hashlib, yaml

from webinterfaceabstract import WebInterfaceAbstract, TypeRequest
from include.statistiques import Statistiques, TypeCollect, TypeDate
from include.template import Template
from common.schedule import Schedule
from include.decorators import authenticated_only, admin_only, antidos, not_in_maintenance
import include.globals as globals

class Api(WebInterfaceAbstract):
    ###### API
    #
    # Examples
    #
    # api/start/1           # start a challenge (api/start/challenge_id)
    # api/reset/1           # destroy a VM and start same challenge (api/reset/vm_id)
    # api/quit/1            # destroy a VM (api/quit/vm_id)
    # api/isalive/1         # is VM alive (api/isalive/vm_id) ?
    # api/system_reset/1    # restart VM (api/system_reset/vm_id)
    # api/valide_iso/1      # mount iso (api/valide_iso/vm_id)
    # api/alive             # my VMs list
    # api/cleanall          # destroy all VMs
    # api/aliveall          # all VMs list
    # api/getallchallenges  # challenges list
    # api/resetdatabase     # clean database (restore to default)
    # api/stats             # get stats
    # api/createqcow2       # Create a QCow2 file
    # api/moveuploaded      # Move file from /serve/upload, to correct target
    # api/flushcache        # Clear all caches
    # api/maintenance/0     # force (or not) maintenance (0=False, 1=True)
    # api/maintenance_state # get state of maintenance mode
    # api/getimageslist/0   # get images list, from engine id (default -> kvm, docker=32, lxc=64, lxd=128, archimulator=256)
    def __init__(self, config):
        WebInterfaceAbstract.__init__(self, config)
        self.statistiques=Statistiques(self.dbrq, config['tmp_target_dir'], config['debug'])
        # scheduler for stats
        self.scheduleComputeStats=Schedule()
        self.scheduleComputeStats.timer(60, self.statistiques.computeStats)
        self.scheduleComputeStats.start()
        # DoS protect
        self.limit_usage=globals.SingletonAntiDos()
        self.limit_usage.start()

    def stop(self):
        self.debug("Stopping API")
        self.limit_usage.stop()
        self.scheduleComputeStats.stop()
        self.limit_usage.join()
        self.scheduleComputeStats.join()
        self.debug("API stopped")

    @cherrypy.expose
    @authenticated_only
    def index(self, **values):
        return b''

    @cherrypy.expose
    @authenticated_only
    @antidos
    def isalive(self, id=None, **values):
        res, vm_id_rq=self.checkId(id)
        if not res: return b''

        # default
        api_res=self.getVmInfoFromId(id)

        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()

    @cherrypy.expose
    @authenticated_only
    @antidos
    def get_msg_error(self, uuid=None, **values) -> bytes:
        # default
        api_res=dict(success=False)
        msg=self.getErrorMsgFromUUID(uuid)
        if msg:
            api_res['success']=True
            api_res['msg']=msg
        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()

    @cherrypy.expose
    @authenticated_only
    @antidos
    @not_in_maintenance
    def valide_iso(self, id=None, iso=None, **values):
        if id==None or iso==None:
            return b''
        res, vm_id_rq=self.checkId(id)
        if not res: return b''

        # default
        api_res=dict(success=False)
        res=self.getDicForVmIDifAllowed(vm_id_rq,all_columns=False, iso_columns=True)
        if res and len(res)>0:
            api_res=res[0]
            api_res['success']=False
            challenge_id=api_res['challenge']
            master=api_res['master']
            mount_iso=1 if ('mount_iso' in api_res.keys() or master) else 0
            uuid=api_res['uuid']
            this_team_id=self.getCurrentTeamId()
            if mount_iso==1:
                if master:
                    iso_path=self.config['iso_dir']
                else:
                    iso_path=self.dbrq.getIsoPathOfChallengeId(challenge_id)

                iso_files=self.coreVMS.getListIsoFiles(iso_path)
                if iso in iso_files or iso=="":
                    bootable=True if iso else False
                    iso_filename="" if iso=="" else "/{}/{}".format(iso_path, iso)
                    if iso_filename:
                        self.logInfo("{} (team {}) attach an ISO (uuid={}, iso_filename={}, bootable={})"
                            .format(self.getCurrentUsername(), this_team_id, uuid, iso_filename, bootable))
                    else:
                        self.logInfo("{} (team {}) eject cdrom (uuid={})".format(self.getCurrentUsername(), this_team_id, uuid))
                    res=self.coreVMS.attachISO(uuid,iso_filename, bootable)
                    if res:
                        api_res['success']=res

        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()


    @cherrypy.expose
    @authenticated_only
    @antidos
    @not_in_maintenance
    def system_reset(self, id=None, **values):
        res, vm_id_rq=self.checkId(id)
        if not res: return b''

        # default
        api_res=dict(success=False)
        res=self.getDicForVmIDifAllowed(vm_id_rq, all_columns=False)
        if res and len(res)>0:
            api_res=res[0]
            uuid=api_res['uuid']
            this_team_id=self.getCurrentTeamId()
            # send system_reset command to qemu
            self.logInfo("{} (team {}) send system_reset request (uuid={})".format(self.getCurrentUsername(), this_team_id, uuid))
            res=self.coreVMS.rebootVM(uuid)
            api_res['success']=res

        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()


    @cherrypy.expose
    @authenticated_only
    @antidos
    @not_in_maintenance
    def start(self, id=None, **values):
        res, id=self.checkId(id)
        if not res: return b''

        # default
        api_res=dict(success=False)

        state, res=self.startVM(id)
        if state:
            api_res['challenge']=id
            api_res['uuid']=res["uuid"]
            api_res['vm_id']=res["vm_id"]
            api_res['name']=res["name"]
            api_res['alive']=True
            api_res['success']=True
        else:
            api_res['message']=res

        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()


    @cherrypy.expose
    @authenticated_only
    @antidos
    @not_in_maintenance
    def quit(self, id=None, **values):
        res, vm_id_rq=self.checkId(id)
        if not res: return b''

        # default
        api_res=dict(success=False)

        state, res=self.stopVM(vm_id_rq)
        if state:
            api_res=res
        else:
            api_res['message']=res

        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()


    @cherrypy.expose
    @authenticated_only
    @antidos
    @not_in_maintenance
    def reset(self, id=None, **values):
        res, vm_id=self.checkId(id)
        if not res: return b''

        # default
        api_res=dict(success=False)

        state, res=self.stopVM(vm_id)
        if state:
            challenge_id=res['challenge']
            # start challenge
            state, res=self.startVM(challenge_id)
            if state:
                api_res['challenge']=challenge_id
                api_res['uuid']=res["uuid"]
                api_res['vm_id']=res["vm_id"]
                api_res['name']=res["name"]
                api_res['alive']=True
                api_res['success']=True
            else:
                api_res['message']=res
        else:
            api_res['message']=res

        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()


    @cherrypy.expose
    @authenticated_only
    @admin_only
    @antidos
    @not_in_maintenance
    def cleanall(self, **values):
        # default
        api_res=dict(success=False)
        username=self.getCurrentUsername()
        this_team_id=self.getCurrentTeamId()
        self.logWarning("User {} send a cleanall request".format(username))
        res, msg=self.stopAllVM(this_team_id)
        api_res['success']=res
        api_res['message']=msg

        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()

    @cherrypy.expose
    @authenticated_only
    @admin_only
    @antidos
    @not_in_maintenance
    def createqcow2(self, **values):
        # default
        api_res=dict(success=False, message="An error occurred")
        if 'name' in values and 'size' in values:
            name=values['name']
            size=0
            try:
                size=int(values['size'])
            except:
                pass
            if size!=0 and name.endswith(".qcow2"):
                res, msg=self.coreVMS.createqcow2(name, size)
                api_res['success']=res
                api_res['message']=msg
        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()


    @cherrypy.expose
    @authenticated_only
    @admin_only
    @antidos
    @not_in_maintenance
    def moveuploaded(self, **values):
        # default
        api_res=dict(success=False)
        if 'name' in values and 'type' in values and 'force' in values:
            name=values['name']
            tpe=values['type']
            force=True if values['force']=="true" else False
            res, msg=self.coreVMS.moveuploaded(name, tpe, force)
            api_res['success']=res
            api_res['message']=msg
        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()

    @cherrypy.expose
    @authenticated_only
    @admin_only
    @antidos
    def aliveall(self, **values):
        # default
        api_res=dict(success=False)

        vms=self.getDicForVmIDifAllowed(_type=TypeRequest.all_vms)
        if isinstance(vms, list):
            api_res['success']=True
            for vm in vms:
                vm_id=vm.pop('vm_id')
                api_res[vm_id]=vm

        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()

    @cherrypy.expose
    @authenticated_only
    @antidos
    def getallchallenges(self, **values):
        # default
        api_res=dict(success=False)
        challenges=self.dbrq.getTupleFromTable('id,name', 'challenges')
        if isinstance(challenges, list):
            api_res['success']=True
            for _id, name in challenges:
                api_res[_id]=name
        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()

    @cherrypy.expose
    @authenticated_only
    @admin_only
    @antidos
    def resetdatabase(self, **values):
        # default
        api_res=dict(success=False)
        res, msg=self.dbrq.reset_database()
        if res:
            api_res['success']=True
        else:
            self.logWarning(msg)
            api_res['success']=False

        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()

    @cherrypy.expose
    @authenticated_only
    @admin_only
    @antidos
    def flushcache(self, **values):
        self.debug("Api:flushcache")
        self.dbrq.flushCache()
        self.coreVMS.flushCache()
        tmp_tmpl=Template()
        tmp_tmpl.flushCache()
        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps({"success":True}).encode()

    @cherrypy.expose
    @authenticated_only
    @admin_only
    @antidos
    def maintenance(self, state=False, **values):
        # default
        api_res=dict(success=True)
        try:
            state=bool(int(state))
        except:
            return simplejson.dumps({"success": False}).encode()
        api_res['maintenance_admin']=state
        self.coreVMS.enableMaintModeAdmin(state)
        api_res['maintenance_tech']=self.coreVMS.isInMaintenanceTechMode()
        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()

    @cherrypy.expose
    @authenticated_only
    @antidos
    def maintenance_state(self, **values):
        # default
        api_res=dict(success=True)
        api_res['maintenance_admin']=self.coreVMS.isInMaintenanceAdminMode()
        api_res['maintenance_tech']=self.coreVMS.isInMaintenanceTechMode()
        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()

    @cherrypy.expose
    @authenticated_only
    @admin_only
    @antidos
    def getimageslist(self, engine_id=0, **values):
        try:
            engine_id=int(engine_id)
        except:
            engine_id=0
        # default
        api_res=dict(success=True)
        api_res['images']=self.coreVMS.getListImages(engine_id)
        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()

    @cherrypy.expose
    @authenticated_only
    @antidos
    def alive(self, **values):
        # default
        api_res=dict(success=False)

        vms=self.getDicForVmIDifAllowed(_type=TypeRequest.mine_vms)
        if isinstance(vms, list):
            api_res['success']=True
            for vm in vms:
                vm_id=vm.pop('vm_id')
                vm.pop('team')
                api_res[vm_id]=vm

        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()

    def checkYaml(self, filename, key_required):
        self.debug(f"checkYaml {filename} {key_required}")
        res=None
        try:
            with open(filename) as f:
                # ignore error if is a template
                data=re.sub(r'\{\{.+\}\}', '__TEMPLATE_ITEM__', f.read())
                res=yaml.safe_load(data)
        except OSError as e:
            return False, "{} (filename : {})".format(e.strerror, filename)
        except Exception as e:
            return False, "Impossible to parse yaml file {} : {}".format(filename, str(e))
        if res and key_required in res:
            return True, ""
        return False, "key '{}' missing in {}".format(key_required, filename)

    @cherrypy.expose
    @authenticated_only
    @admin_only
    @antidos
    def upload(self, type_file=None, ufile=None, tr=None, **values):
        if cherrypy.request.method == 'POST':
            try:
                # check token (replay protect)
                if not tr:
                    raise Exception('An error occured')
                resutoken, _=self.tokens.checkToken(tr, True)
                if not resutoken:
                    raise Exception('Sorry, token expired. Please retry ...')
                # normalize filename
                upload_filename = re.sub(r'[^a-zA-Z0-9\_\-\.]', '_', ufile.filename)
                content_type=str(ufile.content_type)
                upload_path=self.config['upload_dir']
                validate= {
                    'qcow2' :         { 'mime' : 'application/(octet-stream|x-qemu-disk)', 'endswith' : 'qcow2'},
                    'iso' :           { 'mime' : 'application/(octet-stream|x-cd-image|x-iso9660-image)',   'endswith' : 'iso'},
                    'yaml_archi' :    { 'mime' : 'application/(x-yaml|octet-stream)|text/plain', 'endswith' : 'yaml|yml'},
                    'yaml_kvm' :      { 'mime' : 'application/(x-yaml|octet-stream)|text/plain', 'endswith' : 'yaml|yml'},
                    'lxc_image' :     { 'mime' : 'application/(x-gzip|gzip)', 'endswith' : 'tar.gz'},
                    'lxd_image' :     { 'mime' : 'application/(x-gzip|gzip)', 'endswith' : 'tar.gz'},
                    'docker_image' :  { 'mime' : 'application/x-tar', 'endswith' : 'tar'},
                }
                if type_file in validate and re.match('^({})$'.format(validate[type_file]['mime']), content_type, re.IGNORECASE) and re.match(r'.*\.({})$'.format(validate[type_file]['endswith']),upload_filename , re.IGNORECASE):
                    self.debug("Try to upload : {} - type : {}".format(upload_filename, content_type))
                else:
                    raise Exception(f"This MIME type '{content_type}' is not allowed with this filename '{upload_filename}'")

                upload_file = os.path.normpath(os.path.join(upload_path, upload_filename))
                size = 0
                self.logWarning("Try to upload file {} (Type MIME : {})".format(upload_filename, content_type))
                sha1_hash = hashlib.sha1()
                sha256_hash = hashlib.sha256()

                with open(upload_file, 'wb') as out:
                    while True:
                        data = ufile.file.read(8192)
                        if not data:
                            break
                        sha1_hash.update(data)
                        sha256_hash.update(data)
                        out.write(data)
                        size += len(data)

                # check yaml
                if type_file in ('yaml_archi', 'yaml_kvm'):
                    key_required='archi' if type_file=='yaml_archi' else 'kvm'
                    res, msg=self.checkYaml(upload_file, key_required)
                    if not res:
                        raise Exception(msg)

                # it's now uploaded to /serve/upload
                self.logInfo("{} correctly uploaded to {}".format(upload_filename, upload_path))
                res={
                    'success':          True,
                    'upload_filename':  upload_filename,
                    'size':             size,
                    'content_type':     content_type,
                    'sha1sum':          sha1_hash.hexdigest(),
                    'sha256sum':        sha256_hash.hexdigest(),
                    'type_file':        type_file
                }
            except Exception as e:
                res={'success':False, 'msg': str(e), 'tr': self.tokens.getToken(self.tokens.encodeBase64(self.tokens.getRandomNumber(8)),True)}
            cherrypy.response.headers['Content-Type'] = 'application/json'
            return simplejson.dumps(res).encode()
        return b''

    @cherrypy.expose
    @authenticated_only
    @admin_only
    @antidos
    def stats(self, **values):
        # default
        api_res=dict(success=False)
        type_date=None
        if 'hour' in values or 'lasthour' in values:
            type_date=TypeDate.hour
        elif 'day' in values or 'lastday' in values:
            type_date=TypeDate.day
        if 'current' in values and type_date!=None:
            api_res['cnt_running_max']=self.dbrq.getStatsVmMax()
            api_res['cnt_current_running']=self.dbrq.getStatsVmRunning()
            api_res['cnt_vm_from_the_beginning']=self.dbrq.getStatsAllVmFromTheBeginning()
            api_res['per_vm']=self.dbrq.getStatsPerVm()
            api_res['per_team']=self.dbrq.getStatsVmPerTeam()
            last_item_started=self.statistiques.getLastOnListOf((TypeCollect.started, type_date))
            api_res['last_item_started']=last_item_started
            last_item_memory=self.statistiques.getLastOnListOf((TypeCollect.memory, type_date))
            api_res['last_item_memory']=last_item_memory
            last_item_storage=self.statistiques.getLastOnListOf((TypeCollect.storage, type_date))
            api_res['last_item_storage']=last_item_storage
            last_item_cpu_usage=self.statistiques.getLastOnListOf((TypeCollect.cpu_usage, type_date))
            api_res['last_item_cpu_usage']=last_item_cpu_usage
            api_res['success']=True
        elif  ('lasthour' in values or 'lastday' in values) and type_date!=None:
            labels_started, datas_started=self.statistiques.getFullListOf((TypeCollect.started, type_date))
            labels_memory, datas_memory=self.statistiques.getFullListOf((TypeCollect.memory, type_date))
            labels_storage, datas_storage=self.statistiques.getFullListOf((TypeCollect.storage, type_date))
            labels_cpu_usage, datas_cpu_usage=self.statistiques.getFullListOf((TypeCollect.cpu_usage, type_date))
            data_total_memory=self.statistiques.getMemoryTotal()
            data_total_storage=self.statistiques.getDiskTotal()
            api_res['labels_started']=labels_started
            api_res['datas_started']=datas_started
            api_res['labels_memory']=labels_memory
            api_res['datas_memory']=datas_memory
            api_res['data_total_memory']=data_total_memory
            api_res['labels_storage']=labels_storage
            api_res['datas_storage']=datas_storage
            api_res['data_total_storage']=data_total_storage
            api_res['labels_cpu_usage']=labels_cpu_usage
            api_res['datas_cpu_usage']=datas_cpu_usage
            api_res['success']=True

        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps(api_res).encode()


