#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys
import threading, time
import itertools

sys.path.append("../")
from include.host_ressources import HostRessources

class TypeDate():
    hour=0
    day=1

class TypeCollect():
    started=1
    memory=2
    storage=3
    cpu_usage=4
    # todo add here ...

class Statistiques():
    def __init__(self, dbrq, mount_path='/', debug_enabled=False):
        # database
        self.dbrq=dbrq
        # ressources
        self.hostrs=HostRessources(debug_enabled)
        # total memory
        self.total_memory=self.hostrs.getMemoryTotal()
        # total disk
        self.mount_path=mount_path
        self.total_disk=self.hostrs.getDiskTotal(self.mount_path)
        self.lock=threading.Lock()
        self.count=0
        self.max_hour_len=60
        self.compute_day_every_n_minutes=10
        self.max_day_len=int(24*(self.max_hour_len/self.compute_day_every_n_minutes))
        self.dicStat=dict()
        list_type_collect=[TypeCollect.__dict__[c] for c in TypeCollect.__dict__.keys() if "__" not in c]
        list_type_date=[TypeDate.__dict__[c] for c in TypeDate.__dict__.keys() if "__" not in c]
        for c in itertools.product(list_type_collect, list_type_date):
            self.dicStat[c]=[]
        self.memory_total=None
        self.disk_total=None
        self.debug_enabled=debug_enabled

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def getAverage(self, l):
        return sum(l)/len(l)

    def getFullListOf(self, type_of_stat):
        self.lock.acquire()
        res=self.dicStat[type_of_stat].copy()
        self.lock.release()
        return [c for c,_ in res], [d for _,d in res]

    def getLastOnListOf(self, type_of_stat):
        self.lock.acquire()
        last=None
        if len(self.dicStat[type_of_stat])>0:
            last=self.dicStat[type_of_stat][-1]
        self.lock.release()
        return last

    def appendToList(self, type_of_stat, data):
        self.lock.acquire()
        l_hour=self.dicStat[(type_of_stat, TypeDate.hour)]
        l_day=self.dicStat[(type_of_stat, TypeDate.day)]
        l_hour.append((self.getTime(), data))
        if  self.count>=self.compute_day_every_n_minutes:
            n_last_values=[d for _,d in l_hour[-self.compute_day_every_n_minutes:]]
            average=self.getAverage(n_last_values)
            # add average to day list
            l_day.append((self.getTime(), round(average, 2)))
            if len(l_day)>self.max_day_len:
                # remove first element of day
                l_day.remove(l_day[0])
        if len(l_hour)>=self.max_hour_len:
            # remove first element of hour
            l_hour.remove(l_hour[0])
        self.lock.release()

    def getTime(self):
        return time.strftime("%Hh%M")


    def computeStats(self):
        self.debug('Statistiques:computeStats')
        self.count+=1
        self.appendToList(TypeCollect.started, self.dbrq.getStatsVmRunning())
        self.appendToList(TypeCollect.memory, round(self.hostrs.getMemoryUsed()/1000000000, 1))
        self.appendToList(TypeCollect.storage, round(self.hostrs.getDiskUsed(self.mount_path)/1000000000, 1))
        self.appendToList(TypeCollect.cpu_usage, round(self.getAverage(self.hostrs.getCpuUsagePerCent()), 1))
        # todo add here ...
        if  self.count>=self.compute_day_every_n_minutes:
            self.count=0

    def getMemoryTotal(self):
        if self.memory_total!=None:
            return self.memory_total
        self.memory_total=round(self.total_memory/1000000000, 1)
        return self.memory_total

    def getDiskTotal(self):
        if self.disk_total!=None:
            return self.disk_total
        self.disk_total=round(self.total_disk/1000000000, 1)
        return self.disk_total


if __name__ == '__main__':
    from requestdatabase import RequestDatabase
    dbrq=RequestDatabase('../database.sqlite3', True)
    dbrq.start()
    stat=Statistiques(dbrq, '/', True)
    stat.computeStats()
    print(stat.getFullListOf((TypeCollect.started, TypeDate.hour)))
    print(stat.getLastOnListOf((TypeCollect.started, TypeDate.hour)))
    print(len(stat.dicStat[(TypeCollect.memory, TypeDate.hour)]))
    print(len(stat.dicStat[(TypeCollect.memory, TypeDate.day)]))
    print(stat.getMemoryTotal())
    print(stat.getDiskTotal())
    print(stat.dicStat)
    dbrq.stop()
    dbrq.join()
