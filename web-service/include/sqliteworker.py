#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

"""SqliteWorker class is a thread safe sqlite3 interface."""

import sys, os, threading, time
import sqlite3, hashlib
from queue import Queue

class SqliteWorker(threading.Thread):
    def __init__(self, database_file, debug_enabled=False):
        threading.Thread.__init__(self)
        self.database_file=database_file
        self.debug_enabled=debug_enabled
        self.conn=None
        self.eventTerminated = threading.Event()
        self.reqs=Queue()
        self.results_events={}
        self.lockResultDict=threading.Lock()
        self.lockRequest=threading.Lock()

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def warning(self, msg):
        # reimplement me
        sys.stderr.write("{}\n".format(msg))

    def logSqlRequests(self, msg):
        # reimplement me
        self.debug(msg)

    def _execRequest(self, request):
        if not self.conn:
            return False, []
        self.lockRequest.acquire()
        self.logSqlRequests(request)
        try:
            cursor = self.conn.cursor()
            cursor.execute(request)
        except Exception as e:
            self.lockRequest.release()
            self.warning("ERROR in sql request : {}".format(e))
            return False, []
        self.conn.commit()
        resu=cursor.fetchall()
        self.lockRequest.release()
        return True, resu

    def initialiseDatabase(self):
        # reimplement me
        pass

    def getToken(self, request):
        ts=time.time()
        salt=os.urandom(4)
        return hashlib.sha1("{}{}{}".format(salt, request, ts).encode()).digest()

    def execute(self, request):
        """
        'execute' is a blocking method

        result, values=con.execute('SELECT * FROM clients')

        Args:
            request : a query request

        Return:
            result : False on error
            values : a list with response of the query (if SELECT)

        """
        if self.eventTerminated.is_set():
            return False, []

        token=self.getToken(request)
        evt=threading.Event()
        self.lockResultDict.acquire()
        self.results_events.setdefault(token, evt)
        self.lockResultDict.release()
        self.reqs.put((token, request))

        is_ok=evt.wait(6) # lock until receive response or timeout

        self.lockResultDict.acquire()
        res=self.results_events.pop(token)
        self.lockResultDict.release()
        if not is_ok:
            self.warning("WARNING : request timeout")
        if is_ok and res and isinstance(res, tuple):
            return res
        return False, []

    def stop(self):
        self.eventTerminated.set()

    def run(self):
        """Thread loop"""
        if not os.path.isfile(self.database_file) and self.database_file!=':memory:':
            self.warning("Database file doesn't exist : {}".format(self.database_file))
            return
        try:
            self.conn = sqlite3.connect(self.database_file)
        except Exception as e:
            self.warning("ERROR Sqlite : {}".format(e))
            return

        self.initialiseDatabase()
        while not self.eventTerminated.is_set():
            time.sleep(0.1)
            while not self.reqs.empty() and not self.eventTerminated.is_set():
                try:
                    token, req=self.reqs.get(timeout=0.05)
                except:
                    continue
                res=self._execRequest(req)
                self.lockResultDict.acquire()
                if token in self.results_events:
                    evt=self.results_events[token]
                    self.results_events[token]=res
                    evt.set() # unlock thread
                self.lockResultDict.release()

if __name__ == "__main__":
    dbrq=SqliteWorker(':memory:', True)
    dbrq.start()
    dbrq.execute('CREATE TABLE clients (id integer PRIMARY KEY, name text NOT NULL)')
    dbrq.execute("INSERT INTO clients VALUES (1, 'A')")
    dbrq.execute("INSERT INTO clients VALUES (2, 'B')")
    result, values=dbrq.execute("SELECT * FROM clients")
    assert result==True
    assert values==[(1, 'A'), (2, 'B')]
    dbrq.stop()
    dbrq.join()
