#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys

sys.path.append("../")
from include.abstract_template import AbstractTemplate

class Widget(AbstractTemplate):
    def __init__(self):
        AbstractTemplate.__init__(self,"templates/widgets/")

    def load(self, r_file):
        self._load(r_file)

    def generate(self):
        for a, b in self.replacement:
            self.content=self.content.replace(a, b)
        return self.content

