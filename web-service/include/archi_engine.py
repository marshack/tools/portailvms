#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys
import socket, os
import time

sys.path.append("../")
from include.abstract_virt_engines import AbstractVirtEngines

class ArchiEngine(AbstractVirtEngines):
    def __init__(self, config, debug_enabled=False, callback_warning=None, callback_tosend=None, ):
        AbstractVirtEngines.__init__(self, "Archimulator", "archimulator", debug_enabled, callback_warning, callback_tosend)
        self.templatesDir=config['archi_templates_dir']
        self.tmpSockDir=config['tmp_socketvms_dir']
        self.cache_origin_yaml_list=None
        self.engine_features={
            256: "Archimulator"
        }

    def startDisposableVm(self, name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio=False, scsi_virtio=False, with_vnc=False, vnc_password=None, cpu=1):
        source=f"{self.templatesDir}/{source}"
        if not os.path.isfile(source):
            return False, "Source file '{}' doesn't exist".format(source)

        if vnc_password==None:
            vnc_password=""

        cmd= {"target": "startarchimulator",
            "name": name,
            "id": "{:02d}".format(id),
            "template": source,
            "uuid": uuid,
            "from_address": from_address,
            "to_address": to_address,
            "with_vnc": str(int(with_vnc)),
            "vnc_password": vnc_password
        }

        self.toSend(cmd)
        return True, None

    def startMasterVm(self, name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio=False, scsi_virtio=False, vnc_password=None, cpu=1):
        return False, "Sorry, it's impossible to create archi with portailvms"

    def sendCommand(self, uuid, cmd):
        self.debug("ArchiEngine:sendCommand - uuid={}, cmd='{}'".format(uuid, cmd))
        socket_ux_file="{}/sock_archi_{}".format(self.tmpSockDir, uuid)
        if os.path.exists(socket_ux_file):
            try:
                client = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
                client.connect(socket_ux_file)
                cmd+="\n"
                client.send(cmd.encode('utf-8'))
                time.sleep(0.2)
                client.close()
            except:
                self.logWarning("An error occured when sending command (uuid={}, cmd='{}')".format(uuid, cmd))
                return False
        else:
            self.logWarning("Couldn't Connect to {}".format(socket_ux_file))
            return False
        return True

    def destroyVM(self, uuid):
        self.debug("ArchiEngine:destroyVM - uuid={}".format(uuid))
        return self.sendCommand(uuid, "stop")

    def rebootVM(self, uuid):
        self.debug("ArchiEngine:rebootVM - uuid={}".format(uuid))
        return self.sendCommand(uuid, "restart")

    def getListImages(self):
        self.debug("ArchiEngine:getListImages")
        if self.cache_origin_yaml_list:
            return self.cache_origin_yaml_list
        try:
            l=os.listdir(self.templatesDir)
        except:
            self.logWarning("Impossible to get list of yaml files")
            l=[]
        list_yaml_files = [ c for c in l if c.lower().endswith('.yaml')]
        self.cache_origin_yaml_list=list_yaml_files
        return tuple(list_yaml_files)

    def flushCache(self):
        self.debug("ArchiEngine:flushCache")
        self.cache_origin_yaml_list=None


    def getUrlConsole(self, base, vm_id, password):
        return  f"https://{base}:{6900+vm_id}/vnc.html?password={password}&autoconnect=true"