#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys
try:
    import docker
except:
    docker=None

sys.path.append("../")
from include.abstract_virt_engines import AbstractVirtEngines

class DockerEngine(AbstractVirtEngines):
    def __init__(self, debug_enabled=False, callback_warning=None, callback_tosend=None):
        AbstractVirtEngines.__init__(self, "Docker", "docker", debug_enabled, callback_warning, callback_tosend)
        self.hostname_prefix='host-'
        self.cache_images_list=None
        try:
            self.client_docker=docker.from_env()
        except:
            self.client_docker=None
            self.available=False
            self.logWarning("Docker : Client Connection Failed")
        self.engine_features={
            32: "Docker"
        }

    def getNameFromUuid(self, uuid):
        return "{}{}".format(self.hostname_prefix, uuid)

    def checkImageExist(self, name):
        if name in self.getListImages():
            return True
        return False

    def isContainerExist(self, name):
        try:
            self.client_docker.containers.get(name)
            return True
        except:
            pass
        return False

    def startDisposableVm(self, name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio=False, scsi_virtio=False, with_vnc=False, vnc_password=None, cpu=1):
        if not self.checkImageExist(source):
            return False, "Image '{}' doesn't exist".format(source)

        cmd= {"target": "startephemeraldocker",
            "name": name,
            "id": "{:02d}".format(id),
            "ram": str(ram),
            "cpu": str(cpu),
            "disk": str(vol_size),
            "image": source,
            "uuid": uuid,
            "from_address": from_address,
            "to_address": to_address,
            "with_ttyd": str(int(with_vnc)),
            "ttyd_password": vnc_password
        }

        self.toSend(cmd)
        return True, None

    def startMasterVm(self, name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio=False, scsi_virtio=False, vnc_password=None, cpu=1):
        return False, "Sorry, it's impossible to create Docker images with portailvms"

    def destroyVM(self, uuid):
        self.debug(f"DockerEngine:destroyVM - uuid={uuid}")
        name=self.getNameFromUuid(uuid)
        try:
            container=self.client_docker.containers.get(name)
            container.stop(timeout=0)
        except:
            self.logWarning(f'Impossible to stop container. uuid={uuid}')
        if self.isContainerExist(name):
            # created but not started
            try:
                container=self.client_docker.containers.get(name)
                container.remove(force=True)
            except:
                pass
        return True

    def rebootVM(self, uuid):
        self.debug(f"DockerEngine:rebootVM - uuid={uuid}")
        name=self.getNameFromUuid(uuid)
        try:
            container=self.client_docker.containers.get(name)
            container.restart(timeout=0)
        except:
            self.logWarning(f'Impossible to restart container. uuid={uuid}')
            return False
        return True

    def attachISO(self, uuid, filename, bootable=False):
        return False # impossible to do that !

    def getListImages(self):
        self.debug("DockerEngine:getListImages")
        if not self.client_docker:
            # no docker client connection
            return ()
        if self.cache_images_list:
            return self.cache_images_list
        l=[]
        try:
            for image in self.client_docker.images.list():
                if image.tags:
                    l.extend(image.tags)
        except:
            self.logWarning("Impossible to get docker images list")
        self.cache_images_list=l
        return tuple(l)

    def flushCache(self):
        self.debug("DockerEngine:flushCache")
        self.cache_images_list=None


    def getUrlConsole(self, base, vm_id, password):
        return  f"https://{base}:{5900+vm_id}/?arg={password}"
