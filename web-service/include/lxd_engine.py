#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys

try:
    from pylxd import Client as ClientLxd
except:
    ClientLxd=None

sys.path.append("../")
from include.abstract_virt_engines import AbstractVirtEngines

class LxdEngine(AbstractVirtEngines):
    def __init__(self, debug_enabled=False, callback_warning=None, callback_tosend=None):
        AbstractVirtEngines.__init__(self, "LXD", "lxc", debug_enabled, callback_warning, callback_tosend)
        self.hostname_prefix='host-'
        try:
            self.client_lxd=ClientLxd()
        except:
            self.client_lxd=None
            self.available=False
            self.debug("LXD : Client Connection Failed")
        self.cache_images_list=None
        self.engine_features={
            128:"LXD"
        }

    def getNameFromUuid(self, uuid):
        return "{}{}".format(self.hostname_prefix, uuid)

    def checkImageExist(self, name):
        if name in self.getListImages():
            return True
        return False

    def getContainerByUuid(self, uuid):
        name=self.getNameFromUuid(uuid)
        try:
            return self.client_lxd.containers.get(name)
        except:
            pass
        return None

    def startDisposableVm(self, name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio=False, scsi_virtio=False, with_vnc=False, vnc_password=None, cpu=1):
        if not self.checkImageExist(source):
            return False, "Image '{}' doesn't exist".format(source)

        cmd= {"target": "startephemerallxd",
            "name": name,
            "id": "{:02d}".format(id),
            "ram": str(ram),
            "cpu": str(cpu),
            "disk": str(vol_size),
            "image": source,
            "uuid": uuid,
            "from_address": from_address,
            "to_address": to_address,
            "with_ttyd": str(int(with_vnc)),
            "ttyd_password": vnc_password
        }

        self.toSend(cmd)
        return True, None

    def startMasterVm(self, name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio=False, scsi_virtio=False, vnc_password=None, cpu=1):
        return False, "Sorry, it's impossible to create LXD images with portailvms"

    def destroyVM(self, uuid):
        self.debug(f"LxdEngine:destroyVM - uuid={uuid}")
        container=self.getContainerByUuid(uuid)
        if container:
            try:
                container.stop(force=True, wait=True)
            except:
                self.logWarning(f'Impossible to stop container. uuid={uuid}')
                return False
        container=self.getContainerByUuid(uuid)
        if container:
            # created but not started
            try:
                container.delete()
            except:
                self.logWarning(f'Impossible to destroy container. uuid={uuid}')
                return False
        return True

    def rebootVM(self, uuid):
        self.debug(f"LxdEngine:rebootVM - uuid={uuid}")
        container=self.getContainerByUuid(uuid)
        if container:
            try:
                container.restart(force=True)
            except:
                self.logWarning(f'Impossible to restart container. uuid={uuid}')
                return False
        return True

    def attachISO(self, uuid, filename, bootable=False):
        return False # impossible to do that !

    def getListImages(self):
        self.debug("LxdEngine:getListImages")
        if not self.client_lxd:
            # no lxd client connection
            return ()
        if self.cache_images_list:
            return self.cache_images_list
        l=[]
        # don't use 'images.get_by_alias', because not work well with pylxd version 2.2.6
        # TODO FIX in FUTURE RELEASE
        for image in self.client_lxd.images.all():
            for alias in image.aliases:
                if 'name' in alias and alias['name']:
                    l.append(alias['name'])
        self.cache_images_list=l
        return tuple(l)

    def flushCache(self):
        self.debug("LxdEngine:flushCache")
        self.cache_images_list=None


    def getUrlConsole(self, base, vm_id, password):
        return  f"https://{base}:{5900+vm_id}/?arg={password}"
