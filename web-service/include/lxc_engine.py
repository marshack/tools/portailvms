#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, os
try:
    import lxc
except:
    lxc=None

sys.path.append("../")
from include.abstract_virt_engines import AbstractVirtEngines

class LxcEngine(AbstractVirtEngines):
    def __init__(self, config, debug_enabled=False, callback_warning=None, callback_tosend=None):
        AbstractVirtEngines.__init__(self, "LXC", "lxc-info", debug_enabled, callback_warning, callback_tosend)
        self.hostname_prefix='host-'
        self.lxc_path=config['lxc_dir']
        self.lxc_images_path=os.path.abspath(f'{self.lxc_path}/images/')
        self.lxc_containers_path=os.path.abspath(f'{self.lxc_path}/containers/')
        try:
            self.client_lxc=True if lxc.version else False
        except:
            self.client_lxc=False
            self.available=False
            self.logWarning("LXC : Client Connection Failed")
        self.cache_images_list=None
        self.master_uuids={}
        self.engine_features={
            64: "LXC"
        }

    def getNameFromUuid(self, uuid):
        return "{}{}".format(self.hostname_prefix, uuid)

    def checkImageExist(self, name):
        if name in self.getListImages():
            return True
        return False

    def getContainerByUuid(self, uuid):
        name=self.getNameFromUuid(uuid)
        try:
            return lxc.Container(name)
        except:
            pass
        return None

    def uuidIsMaster(self, uuid):
        return uuid in self.master_uuids

    def getImageNameForUuidMaster(self, uuid):
        if uuid in self.master_uuids:
            return self.master_uuids[uuid]
        return ""

    def removeUuidMaster(self, uuid):
        if uuid in self.master_uuids:
            try:
                del(self.master_uuids[uuid])
            except:
                pass

    def startDisposableVm(self, name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio=False, scsi_virtio=False, with_vnc=False, vnc_password=None, cpu=1):
        if not self.checkImageExist(source):
            return False, "Image '{}' doesn't exist".format(source)

        cmd= {"target": "startephemerallxc",
            "name": name,
            "id": "{:02d}".format(id),
            'lxc_path': self.lxc_path,
            "ram": str(ram),
            "cpu": str(cpu),
            "disk": str(vol_size),
            "image": source,
            "uuid": uuid,
            "from_address": from_address,
            "to_address": to_address,
            "with_ttyd": str(int(with_vnc)),
            "ttyd_password": vnc_password
        }

        self.toSend(cmd)
        return True, None

    def startMasterVm(self, name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio=False, scsi_virtio=False, vnc_password=None, cpu=1):
        if not self.checkImageExist(source):
            return False, "Image '{}' doesn't exist".format(source)

        if vnc_password==None:
            vnc_password=""

        cmd= {"target": "startmasterlxc",
            "name": name,
            "id": "{:02d}".format(id),
            'lxc_path': self.lxc_path,
            "ram": str(ram),
            "cpu": str(cpu),
            "disk": str(vol_size),
            "image": source,
            "uuid": uuid,
            "from_address": from_address,
            "to_address": to_address,
            "ttyd_password": vnc_password
        }

        self.master_uuids[uuid]=source
        self.toSend(cmd)
        return True, None

    def destroyVM(self, uuid):
        self.debug(f"LxcEngine:destroyVM - uuid={uuid}")
        is_master=self.uuidIsMaster(uuid)
        if is_master:
            image=self.getImageNameForUuidMaster(uuid)
            container=lxc.Container(image, config_path=self.lxc_images_path)
        else:
            container=self.getContainerByUuid(uuid)
        if container:
            try:
                container.stop()
            except:
                self.logWarning(f'Impossible to stop container. uuid={uuid}')
                return False
        
        if is_master:
            self.removeUuidMaster(uuid)
            return # do not destroy image !

        container=self.getContainerByUuid(uuid)
        if container:
            # created but not started
            try:
                container.destroy()
            except:
                self.logWarning(f'Impossible to destroy container. uuid={uuid}')
                return False
        return True

    def rebootVM(self, uuid):
        self.debug(f"LxcEngine:rebootVM - uuid={uuid}")
        is_master=self.uuidIsMaster(uuid)
        if is_master:
            image=self.getImageNameForUuidMaster(uuid)
            container=lxc.Container(image, config_path=self.lxc_images_path)
        else:
            container=self.getContainerByUuid(uuid)
        if container:
            try:
                container.reboot()
            except:
                self.logWarning(f'Impossible to restart container. uuid={uuid}')
                return False
        return True

    def attachISO(self, uuid, filename, bootable=False):
        return False # impossible to do that !

    def getListImages(self):
        self.debug("LxcEngine:getListImages")
        if not self.client_lxc:
            # no lxc client connection
            return ()
        if self.cache_images_list:
            return self.cache_images_list
        l=[]
        try:
            l=lxc.list_containers(config_path=self.lxc_images_path)
            self.cache_images_list=l
        except:
            self.logWarning(f'LXC : Impossible to get images list')
        return tuple(l)

    def flushCache(self):
        self.debug("LxcEngine:flushCache")
        self.cache_images_list=None


    def getUrlConsole(self, base, vm_id, password):
        return  f"https://{base}:{5900+vm_id}/?arg={password}"
