#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import cherrypy
from urllib import parse
import include.globals as globals
import simplejson

def authenticated_only(func):
    def redirect(*args, **kwargs):
        url='/?next={}'.format(parse.quote(globals.getUrn()))
        raise cherrypy.HTTPRedirect(url)

    def wrapper(*args, **kwargs):
        if globals.isAuthenticated():
            return func(*args, **kwargs)
        # not authenticated
        return redirect(*args, **kwargs)

    return wrapper

def admin_only(func):
    def not_admin(*args, **kwargs):
        raise cherrypy.HTTPRedirect('/')

    def wrapper(*args, **kwargs):
        if globals.isAdmin():
            return func(*args, **kwargs)
        return not_admin(*args, **kwargs)

    return wrapper


def antidos(func):
    limit_usage=globals.SingletonAntiDos()
    def do_nothing(*args, **kwargs):
        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps({'success': False, 'dos': True, 'message': 'DoS detected ! Please, try later ...'}).encode()

    def wrapper(*args, **kwargs):
        try:
            limit_usage.addRecordAndRateLimit(cherrypy.session.id)
        except:
            return do_nothing(*args, **kwargs)
        return func(*args, **kwargs)

    return wrapper

def not_in_maintenance(func):
    def not_allowed(*args, **kwargs):
        cherrypy.response.headers['Content-Type'] = 'application/json'
        return simplejson.dumps({'success': False, 'maintenance': True, 'message': 'System is in maintenance mode'}).encode()

    def wrapper(*args, **kwargs):
        try:
            if not args[0].coreVMS.isInMaintenanceMode():
                return func(*args, **kwargs)
        except:
            pass
        return not_allowed(*args, **kwargs)

    return wrapper
