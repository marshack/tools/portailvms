#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, cherrypy, re

sys.path.append("../")
from include.anti_dos import AntiDos

def getUrn():
    url=cherrypy.url()
    m=re.search(r'^https{0,1}:\/\/[^/]*(\/.*)', url)
    if m and len(m.groups())>0:
        return re.sub("/{2,}", "/", m.group(1)) # replace more /
    return ''

def getSessionBool(key):
    try:
        cherrypy.session[key]
    except:
        return False
    if cherrypy.session[key]:
        return True
    return False

def getSessionValue(key):
    try:
        return cherrypy.session[key]
    except:
        pass
    return None

def setAuthenticated(is_auth):
    cherrypy.session['authenticated'] = is_auth

def isAuthenticated():
    return getSessionBool('authenticated')

def setIsAdmin(is_admin):
    cherrypy.session['admin'] = is_admin

def isAdmin():
    return getSessionBool('admin')

def setCurrentUsername(username):
    cherrypy.session['login']=username

def getCurrentUsername():
    return getSessionValue('login')

def setCurrentTeamId(team_id):
    cherrypy.session['team_id']=team_id

def getCurrentTeamId():
    return getSessionValue('team_id')

class SingletonAntiDos(object):
    def __init__(self, debug=False):
        try:
            SingletonAntiDos.antidos
        except:
            SingletonAntiDos.antidos=AntiDos(debug)
            SingletonAntiDos.antidos.addConstraint(max_calls=3, period=1)
            SingletonAntiDos.antidos.addConstraint(max_calls=5, period=10)
            SingletonAntiDos.antidos.addConstraint(max_calls=20, period=60)

    def addConstraint(self, max_calls, period):
        SingletonAntiDos.antidos.addConstraint(max_calls, period)

    def addRecordAndRateLimit(self, id):
        SingletonAntiDos.antidos.addRecordAndRateLimit(id)

    def start(self):
        if SingletonAntiDos.antidos.is_alive():
            return
        SingletonAntiDos.antidos.start()

    def stop(self):
        SingletonAntiDos.antidos.stop()

    def join(self):
        SingletonAntiDos.antidos.join()