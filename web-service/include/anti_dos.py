#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, time

sys.path.append("../")
from include.sqliteworker import SqliteWorker

class AntiDos(SqliteWorker):
    """
    This class limits usage of our API
    Usage (example) :
        antidos=AntiDos()
        antidos.addConstraint(max_calls=3, period=1)
        antidos.addConstraint(max_calls=5, period=10)
        antidos.start()

        # create a decorator :
        def limit_api(func):
            def do_nothing(*args, **kwargs):
                print("DOS DETECTED !")
            def wrapper(*args, **kwargs):
                try:
                    antidos.addRecordAndRateLimit(session_id)
                except:
                    return do_nothing(*args, **kwargs)
                return func(*args, **kwargs)
            return wrapper

        # and use it :
        @limit_api
        def myapi(self):
            ...

        # when quit, stop thread
        antidos.stop()
    """
    def __init__(self, debug=False):
        SqliteWorker.__init__(self, ':memory:', debug)
        self.last=self.now()
        self.constraintList=[]
        self.purge_seconds_elapsed=60 # default value

    def now(self):
        return int(time.time())

    def initialiseDatabase(self):
        self.debug("AntiDos:initialiseDatabase")
        # use _execRequest because it's running in the same thread (and event loop is not started)
        self._execRequest('CREATE TABLE IF NOT EXISTS `antidos` (`uuid` TEXT NOT NULL, `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)')

    def purgeDb(self):
        self.debug("AntiDos:purgeDb")
        self.last=self.now()
        self.execute('DELETE FROM `antidos` WHERE ((julianday(CURRENT_TIMESTAMP) - julianday(`timestamp`)) * 86400.0)>{}'.format(self.purge_seconds_elapsed))

    def addConstraint(self, max_calls, period):
        if (max_calls, period) not in self.constraintList:
            self.constraintList.append((max_calls, period))
            if period>self.purge_seconds_elapsed:
                self.purge_seconds_elapsed=period

    def addRecordAndRateLimit(self, id):
        self.debug("AntiDos:addRecordAndRateLimit - id = {}".format(id))
        self.execute("INSERT INTO `antidos` (`uuid`) VALUES ('{}')".format(id))
        for max_calls, period in self.constraintList:
            self.rateLimiter(id, max_calls, period)

        if self.now()>self.last+60:
            self.purgeDb()


    def rateLimiter(self, id, max_calls=10, period=1):
        resu, records=self.execute("SELECT count(*) FROM `antidos` WHERE `uuid`='{}' and ((julianday(CURRENT_TIMESTAMP) - julianday(`timestamp`)) * 86400.0)<{}".format(id, period))
        if resu and len(records)>0 and len(records[0])>0:
            res=records[0][0]
            if res>=max_calls:
                raise Exception("Rate limit exception occured (max_calls={}, period={})".format(max_calls, period))


if __name__ == "__main__":
    import signal
    def signal_handler(signal, frame):
        global finished
        antidos.stop()
        finished=True

    finished=False
    antidos=AntiDos()

    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    antidos.addConstraint(max_calls=3, period=1)
    antidos.addConstraint(max_calls=5, period=10)
    antidos.addConstraint(max_calls=20, period=60)
    antidos.start()

    wait=0.1
    while not finished:
        try:
            antidos.addRecordAndRateLimit('a')
        except Exception as e:
            print(e.args[0])
            wait=wait+0.3
        time.sleep(wait)

    antidos.join()
