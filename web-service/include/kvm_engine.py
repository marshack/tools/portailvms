#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys
import socket, os, select
import time

sys.path.append("../")
from include.abstract_virt_engines import AbstractVirtEngines

class KvmEngine(AbstractVirtEngines):
    def __init__(self, config, debug_enabled=False, callback_warning=None, callback_tosend=None):
        AbstractVirtEngines.__init__(self, "KVM", "kvm", debug_enabled, callback_warning, callback_tosend)
        self.originDir=config['origin_dir']
        self.tmpTargetDir=config['tmp_target_dir']
        self.tmpSockDir=config['tmp_socketvms_dir']
        self.keyboard=config['keyboard']
        self.cache_origin_qcow2_list=None
        self.isoAvailable=True
        self.engine_features={
            0:  "KVM (default, BIOS)",
            1:  "KVM (Windows guest, BIOS)",
            8:  "KVM (UEFI)",
            9:  "KVM (UEFI, menu enabled)",
            16: "KVM (CPU guest=CPU host, BIOS)"
        }

    def startDisposableVm(self, name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio=False, scsi_virtio=False, with_vnc=False, vnc_password=None, cpu=1):
        source="{}/{}".format(self.originDir, source)
        if not os.path.isfile(source):
            return False, "Source file '{}' doesn't exist".format(os.path.basename(source))

        if vnc_password==None:
            vnc_password=""

        cmd= {"target": "startdisposablevm",
            "name": name,
            "id": "{:02d}".format(id),
            "tmp_target_dir": self.tmpTargetDir,
            "ram": str(ram),
            "cpu": str(cpu),
            "source": source,
            "uuid": uuid,
            "from_address": from_address,
            "to_address": to_address,
            "net_virtio": str(int(net_virtio)),
            "scsi_virtio": str(int(scsi_virtio)),
            "with_vnc": str(int(with_vnc)),
            "vnc_password": vnc_password,
            "guest_type": str(int(guest_type)),
            "keyboard" : self.keyboard
        }

        self.toSend(cmd)
        return True, None

    def startMasterVm(self, name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio=False, scsi_virtio=False, vnc_password=None, cpu=1):
        source="{}/{}".format(self.originDir, source)
        if not os.path.isfile(source):
            return False, "Source file '{}' doesn't exist".format(os.path.basename(source))

        if vnc_password==None:
            vnc_password=""

        cmd= {"target": "startmaster",
            "name": name,
            "id": "{:02d}".format(id),
            "ram": str(ram),
            "cpu": str(cpu),
            "source": source,
            "uuid": uuid,
            "from_address": from_address,
            "to_address": to_address,
            "net_virtio": str(int(net_virtio)),
            "scsi_virtio": str(int(scsi_virtio)),
            "vnc_password": vnc_password,
            "guest_type": str(int(guest_type)),
            "keyboard" : self.keyboard
        }

        self.toSend(cmd)
        return True, None

    def sendQemuCommand(self, uuid, cmd):
        self.debug("KvmEngine:sendQemuCommand - uuid={}, cmd='{}'".format(uuid, cmd))
        socket_ux_file="{}/sock_{}".format(self.tmpSockDir, uuid)
        if os.path.exists(socket_ux_file):
            try:
                client = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
                client.connect(socket_ux_file)
                cmd+="\n"
                client.send(cmd.encode('utf-8'))
                time.sleep(0.2)
                r,_,_=select.select([client], [], [], 0.8)
                if client in r:
                    if not "QEMU" in client.recv(1024).decode():
                        raise Exception("Don't receive prompt from QEMU Console")
                else:
                    raise Exception("Don't receive data from QEMU Console")
                client.close()
            except Exception as e:
                self.logWarning("An error occured when sending command (uuid={}, cmd='{}') : {}".format(uuid, cmd, e))
                return False
        else:
            self.logWarning("Couldn't Connect to {}".format(socket_ux_file))
            return False
        return True

    def destroyVM(self, uuid):
        self.debug("KvmEngine:destroyVM - uuid={}".format(uuid))
        return self.sendQemuCommand(uuid, "quit")

    def rebootVM(self, uuid):
        self.debug("KvmEngine:rebootVM - uuid={}".format(uuid))
        return self.sendQemuCommand(uuid, "system_reset")

    def attachISO(self, uuid, filename, bootable=False):
        self.debug("KvmEngine:attachISO - uuid={}, filename={}, bootable={}".format(uuid, filename, bootable))
        if filename=="":
                if not self.sendQemuCommand(uuid, "eject -f ide1-cd0"):
                    return False
                return self.sendQemuCommand(uuid, "boot_set c")
        if not os.path.isfile(filename):
            self.logWarning("ISO file doesn't exist")
            return False
        if not self.sendQemuCommand(uuid, "change ide1-cd0 {}".format(filename)):
            return False
        if bootable:
            return self.sendQemuCommand(uuid, "boot_set d")
        else:
            return self.sendQemuCommand(uuid, "boot_set c")
        return True

    def getListImages(self):
        self.debug("KvmEngine:getListImages")
        if self.cache_origin_qcow2_list:
            return self.cache_origin_qcow2_list
        try:
            l=os.listdir(self.originDir)
        except:
            self.logWarning("Impossible to get list of origin qcow2 files")
            l=[]
        list_qcow2_files = [ c for c in l if c.lower().endswith('.qcow2')]
        self.cache_origin_qcow2_list=list_qcow2_files
        return tuple(list_qcow2_files)

    def flushCache(self):
        self.debug("KvmEngine:flushCache")
        self.cache_origin_qcow2_list=None


    def getUrlConsole(self, base, vm_id, password):
        return  f"https://{base}:{6900+vm_id}/vnc.html?password={password}&autoconnect=true"
