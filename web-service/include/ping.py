#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import threading, time
import sys
import subprocess
from queue import Queue

sys.path.append("../")
from common.schedule import Schedule

class Ping(threading.Thread):
    def __init__(self, callback_get_addresses, debug=False):
        threading.Thread.__init__(self)
        self.callback_get_addresses=callback_get_addresses
        self.debug_enabled=debug
        self.eventTerminated = threading.Event()
        self.eventPingRunning = threading.Event()
        self.schedule=Schedule()
        self.resu_dic={}
        self.lockResuPingDic=threading.Lock()
        self.lockStopping=threading.Lock()
        self.result=Queue(maxsize=5)

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def runPing(self, address):
        cmd=['ping', address, '-c','1','-w','1','-W','1']
        p=subprocess.Popen(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        self.result.put((address, p))

    def purge(self, current_addresses):
        todoremove=[]
        self.lockResuPingDic.acquire()
        for address in self.resu_dic.keys():
            if address not in current_addresses:
                todoremove.append(address)
        # remove
        for address in todoremove:
            try:
                self.resu_dic.pop(address)
            except:
                pass
        self.lockResuPingDic.release()

    def callback_timer(self):
        if self.eventPingRunning.is_set():
            print("Ping:callback_timer - ping currently running !")
            return
        self.debug('Ping:callback_timer - started')
        self.eventPingRunning.set()
        addresses=self.callback_get_addresses()
        self.purge(addresses)
        for address in addresses:
            if self.eventTerminated.is_set():
                break
            self.runPing(address)
            time.sleep(0.4)

        self.eventPingRunning.clear()
        self.debug('Ping:callback_timer - finished')

    def isMachineUp(self, address):
        resu=False
        self.lockResuPingDic.acquire()
        if address in self.resu_dic.keys():
            resu=self.resu_dic[address]
        self.lockResuPingDic.release()
        return resu

    def stop(self):
        self.lockStopping.acquire()
        self.debug("Stopping Ping")
        if self.eventTerminated.is_set():
            self.lockStopping.release()
            return # already stopped
        self.eventTerminated.set()
        self.schedule.stop()
        self.schedule.join()
        self.lockStopping.release()
        self.debug("Ping stopped")

    def run(self):
        # first time
        self.schedule.singleShot(8, self.callback_timer)
        self.schedule.timer(60, self.callback_timer)
        self.schedule.start()
        while not self.eventTerminated.is_set():
            time.sleep(0.2)
            while not self.result.empty() and not self.eventTerminated.is_set():
                address, proc=self.result.get()
                try:
                    res=proc.wait(2)
                except:
                    break
                self.lockResuPingDic.acquire()
                self.resu_dic[address] = True if res==0 else False
                self.lockResuPingDic.release()

if __name__ == "__main__":
    import signal
    def signal_handler(signal, frame):
        ping.stop()

    def get_addresses():
        return ('10.0.2.1', '10.0.2.21', '10.0.2.3')

    ping=Ping(get_addresses, True)
    
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    ping.start()
    ping.join()
    print('10.0.2.1', ping.isMachineUp('10.0.2.1'))
    print('10.0.2.2', ping.isMachineUp('10.0.2.2'))
    print('10.0.2.3', ping.isMachineUp('10.0.2.3'))
