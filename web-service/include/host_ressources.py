#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import psutil

class HostRessources():
    def __init__(self, debug_enabled=False):
        self.debug_enabled=debug_enabled

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def getMemoryTotal(self):
        return psutil.virtual_memory().total

    def getMemoryAvailable(self):
        return psutil.virtual_memory().available

    def getMemoryUsed(self):
        return psutil.virtual_memory().used

    def getCpuCount(self):
        return psutil.cpu_count()

    def getCpuUsagePerCent(self):
        return psutil.cpu_percent(percpu=True)

    def getDiskTotal(self, path):
        return psutil.disk_usage(path).total

    def getDiskFree(self, path):
        return psutil.disk_usage(path).free

    def getDiskUsed(self, path):
        return psutil.disk_usage(path).used

if __name__ == '__main__':
    hostrs=HostRessources(True)
    print (hostrs.getCpuCount())
    print (hostrs.getDiskFree('/'))
    print (hostrs.getDiskTotal('/'))
    print (hostrs.getDiskUsed('/'))
