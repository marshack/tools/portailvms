#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)


import sys, os, re
from datetime import datetime

sys.path.append("../")
from common.logs import Logs
from include.sqliteworker import SqliteWorker
from threading import Lock

class RequestDatabase(SqliteWorker):
    def __init__(self, database_file, debug_enabled=False, callback_warning=None, logsql_filename=None, maxbytes=150000, backupcount=9):
        SqliteWorker.__init__(self, database_file, debug_enabled)
        self.callback_warning=callback_warning
        self.lastReturnedId=None
        self.lock_id_available=Lock()
        self.cacheISO_PathForChallengeId={}
        self.logging=None
        self.verbose=True if os.getenv('PORTAILVMS_DEBUG_VERBOSE') and os.getenv('PORTAILVMS_DEBUG_VERBOSE')=='2' else False
        if logsql_filename:
            self.logging=Logs("sql_rq", logsql_filename, maxbytes, backupcount)
            self.logging.logInfo("Logging Sql started")

    def logWarning(self, msg):
        self.debug(f"SQL {msg}")
        if self.logging:
            self.logging.logWarning(msg)
        if self.callback_warning:
            self.callback_warning(msg)

    def warning(self, msg):
        # reimplement from SqliteWorker
        self.logWarning(msg)

    def logSqlRequests(self, msg):
        # reimplement from SqliteWorker
        msg=msg.replace('\n', ' ').strip()
        if self.verbose:
            self.debug(msg)
        if self.logging:
            if re.match(r'(DELETE|UPDATE|INSERT|DROP|ALTER|CREATE|PRAGMA)\s+',msg, re.IGNORECASE) and self.logging:
                self.logging.logInfo(msg)

    def getTupleFromTable(self, columns, table, clause=None, order=None):
        request="SELECT {} FROM {}".format(columns, table)
        if clause:
            request+=" WHERE {}".format(clause)
        if order:
            request+=" ORDER BY {} COLLATE NOCASE ASC".format(order)
        _ , fetch_all=self.execute(request)
        return fetch_all

    def getVmIdRunning(self):
        running=self.getTupleFromTable('vm_id', 'current_vm', 'running = 1')
        return [x[0] for x in running]

    def getIdAvailable(self, max=50):
        def findFirstFromStart(running, start, stop):
            for i in range(start, stop):
                if i not in running:
                    self.lastReturnedId=i
                    return i
            return None
        self.lock_id_available.acquire()
        if self.lastReturnedId==None or self.lastReturnedId>=max:
            self.lastReturnedId=-1

        running=self.getVmIdRunning()
        # start from last returned Id
        res=findFirstFromStart(running, self.lastReturnedId+1, max)
        if res==None:
            # no id available. start at 0
            res=findFirstFromStart(running, 0, max)
        self.lock_id_available.release()
        return res

    def isVmMasterRunning(self, challenge_id):
        req="SELECT `id` FROM `current_vm` WHERE running=1 and challenge_id={} and master=1".format(challenge_id)
        _, result=self.execute(req)
        if len(result)>0:
            return True
        return False

    def updateStatsVmMax(self):
        req="SELECT id FROM `current_vm` WHERE running = 1"
        _, running=self.execute(req)
        req="UPDATE `stats` set `simultaneous_max`={0} WHERE `id` = 1 AND `simultaneous_max`<{0}".format(len(running))
        self.execute(req)

    def getStatsVmMax(self):
        req="SELECT `simultaneous_max` FROM `stats` WHERE `id` = 1"
        _, result=self.execute(req)
        try:
            return result[0][0]
        except:
            pass
        return 0

    def getStatsAllVmFromTheBeginning(self):
        req="SELECT count('id') FROM `current_vm`"
        _, result=self.execute(req)
        try:
            return result[0][0]
        except:
            pass
        return 0

    def getTeamNameFromId(self, team_id):
        req=f"SELECT team_name FROM `teams` WHERE team_id = {team_id}"
        res, records=self.execute(req)
        if res and len(records)>0:
            try:
                return records[0][0]
            except:
                pass
        return str(team_id)

    def updateTeamNameFromId(self, team_id, team_name):
        req=f"SELECT id, team_id,team_name FROM `teams` WHERE team_id = {team_id}"
        res, records=self.execute(req)
        if res and len(records)>0:
            try:
                id,r_team_id,r_team_name=records[0]
                if team_id == r_team_id and team_name != r_team_name:
                    # update team_name
                    req=f"UPDATE `teams` set `team_name`='{team_name}' WHERE `id`={id}"
                    res, _=self.execute(req)
                    return res
            except:
                pass
        else:
            #unknown team, insert it
            req=f"INSERT INTO `teams` (team_id,team_name) VALUES ({team_id},'{team_name}')"
            res, _=self.execute(req)
            return res
        return True

    def getStatsVmRunning(self):
        req="SELECT `id` FROM `current_vm` WHERE running = 1"
        _, result=self.execute(req)
        return len(result)


    def getStatsPerVm(self):
        req="SELECT name, SUM(1), SUM(running) FROM `current_vm` INNER JOIN `challenges` ON challenges.id=current_vm.challenge_id GROUP BY name ORDER BY SUM(1) DESC"
        _, result=self.execute(req)
        return result

    def getStatsVmPerTeam(self):
        req="SELECT IFNULL(team_name, current_vm.team_id), COUNT(current_vm.id), SUM(running) FROM `current_vm` LEFT JOIN `teams` ON teams.team_id=current_vm.team_id GROUP BY team_name ORDER BY COUNT(current_vm.id) DESC"
        _, result=self.execute(req)
        return result

    def getVmRunning(self, vm_id=None, team_id=None):
        # Warning : getListForVmRunning require getVmRunning

        req="SELECT uuid,vm_id,challenge_id,IFNULL(team_name, current_vm.team_id),started,challenges.name,challenges.vnc,challenges.mount_iso,challenges.lifetime,vnc_password,master FROM `current_vm` LEFT JOIN `teams` ON teams.team_id=current_vm.team_id INNER JOIN challenges ON challenges.id=current_vm.challenge_id WHERE running = 1"
        if vm_id!=None:
            req+=" AND current_vm.vm_id='{}'".format(vm_id)
        if team_id!=None:
            req+=" AND current_vm.team_id='{}'".format(team_id)
        _, running=self.execute(req)
        return running

    def getListForVmRunning(self, vm_id=None, team_id=None, all_columns=True, iso_columns=False):
        l=[]
        for record in self.getVmRunning(vm_id, team_id):
            dic={}
            dic['vm_id']=record[1]
            dic['uuid']=record[0]
            dic['challenge']=record[2]
            if all_columns or iso_columns:
                dic['master']=record[10]
                if record[7]==1:
                    dic['mount_iso']=True
            if all_columns:
                dic['team']=record[3]
                dic['started']=record[4]
                dic['name']=record[5]
                dic['vnc']=True if (record[6] or dic['master']) else False
                if dic['vnc']:
                    dic['vnc_password']=record[9]
            if record[8]!=0 and record[10]==0:
                dic['lifetime']=record[8]
            l.append(dic)
        return l

    def getListForVmRunningWithLifetime(self):
        req="SELECT uuid, started, challenges.lifetime FROM `current_vm` INNER JOIN challenges ON challenges.id=current_vm.challenge_id WHERE running = 1 AND challenges.lifetime>0 AND master=0"
        _, running=self.execute(req)
        return running

    def getIsoPathOfChallengeId(self, challenge_id):
        if challenge_id in self.cacheISO_PathForChallengeId.keys():
            return self.cacheISO_PathForChallengeId[challenge_id]
        req="SELECT `iso_path` FROM `challenges` WHERE `mount_iso`=1 and `id`={}".format(challenge_id)
        res, records=self.execute(req)
        if res and len(records)>0:
            try:
                path=records[0][0]
                self.cacheISO_PathForChallengeId[challenge_id]=path
                return path
            except:
                pass
        return ""

    def cleanCachePathOfChallengeId(self, challenge_id):
        if challenge_id in self.cacheISO_PathForChallengeId.keys():
            try:
                self.cacheISO_PathForChallengeId.pop(challenge_id)
            except:
                pass

    def flushCache(self):
        self.cacheISO_PathForChallengeId.clear()

    def updateChallenge(self, id, name, guest_type, _file, ram, \
            vol_size, net_virtio, scsi_virtio, vnc, \
            mount_iso, iso_path, lifetime, enable, cpu):
        req=f"""UPDATE `challenges` set `name`='{name}',guest_type={guest_type},file='{_file}',
ram={ram}, cpu={cpu}, vol_size={vol_size},net_virtio={net_virtio},
scsi_virtio={scsi_virtio},vnc={vnc},mount_iso={mount_iso},
iso_path='{iso_path}',lifetime={lifetime},enable={enable} WHERE `id`={id}"""
        res, _=self.execute(req)
        self.cleanCachePathOfChallengeId(id)
        return res

    def deleteChallenge(self, id):
        req=f"DELETE FROM `challenges` where id={id}"
        res, _=self.execute(req)
        self.cleanCachePathOfChallengeId(id)
        return res

    def addNewChallenge(self, name, guest_type, _file, ram, \
            vol_size, net_virtio, scsi_virtio, vnc, \
            mount_iso, iso_path, lifetime, enable, cpu):
        req=f"""INSERT INTO `challenges` (name,guest_type,file,ram,vol_size,net_virtio,scsi_virtio,vnc,
mount_iso,iso_path,lifetime,enable, cpu) VALUES ('{name}',{guest_type},'{_file}',{ram},{vol_size},
{net_virtio},{scsi_virtio},{vnc},{mount_iso},'{iso_path}',{lifetime},{enable},{cpu})"""
        res, _=self.execute(req)
        return res

    def getGuestTypeForChallengeId(self, challenge_id):
        req=f"SELECT guest_type FROM `challenges` WHERE id = {challenge_id}"
        res, records=self.execute(req)
        if res and len(records)>0:
            try:
                return records[0][0]
            except:
                pass
        return None

    def addNewVmDisposable(self, uuid, challenge_id, team_id, vm_id, vnc_password="", master=0):
        # check if uuid exist
        res=self.getTupleFromTable("id","current_vm","uuid='{}'".format(uuid))
        if len(res)>0:
            self.logWarning("uuid already exist in database")
            return False

        current_timestamp=self.getCurrentTimestamp()
        req="INSERT INTO `current_vm` (uuid,running,started,challenge_id,team_id,vm_id,vnc_password,master) VALUES ('{}','{}',{},{},{},{},'{}',{})" \
            .format(uuid, 1, current_timestamp, challenge_id, team_id, vm_id, vnc_password,master)
        res, _=self.execute(req)

        # for stats
        self.updateStatsVmMax()
        return res

    def isVmRunning(self, uuid):
        running=self.getTupleFromTable('uuid', 'current_vm', 'running = 1 and uuid = "{}"'.format(uuid))
        return len(running)>0

    def setVmDisposableStopped(self, uuid):
        if not self.isVmRunning(uuid):
            return False
        current_timestamp=self.getCurrentTimestamp()
        req="UPDATE `current_vm` set `running`=0, `stopped`={} WHERE `uuid` = '{}' and running = 1" \
            .format(current_timestamp, uuid)
        res, _=self.execute(req)
        return res

    def setAllVmDisposableStopped(self):
        current_timestamp=self.getCurrentTimestamp()
        req="UPDATE `current_vm` set `running`=0, `stopped`={} WHERE running = 1" \
            .format(current_timestamp)
        res, _=self.execute(req)
        return res

    def getMemoryReserved(self):
        req="SELECT SUM(challenges.ram) FROM `current_vm` INNER JOIN challenges ON challenges.id=current_vm.challenge_id WHERE running = 1"
        res, _sum=self.execute(req)
        if res and len(_sum)>0:
            try:
                return int(_sum[0][0])
            except:
                pass
        return 0

    def reset_database(self):
        req="DELETE FROM `current_vm`"
        res, _ =self.execute(req)
        if not res:
            return False, "Impossible to delete rows (in table 'current_vm')"
        req="DELETE FROM `teams`"
        res, _ =self.execute(req)
        if not res:
            return False, "Impossible to delete rows (in table 'teams')"
        req="UPDATE `stats` set `simultaneous_max`=0 WHERE `id` = 1"
        res, _ =self.execute(req)
        if not res:
            return False, "Impossible to update database (table 'stats')"
        self.lastReturnedId=None
        return True, ""


    def getCurrentTimestamp(self):
        return datetime.timestamp(datetime.now())

    def stop(self):
        # reimplement from SqliteWorker
        self.eventTerminated.set()
        if self.logging:
            self.logging.logInfo("Logging Sql stopped")

if __name__ == "__main__":
    import os
    db_file = os.path.join(os.path.dirname(__file__), '../database.sqlite3')
    dbrq=RequestDatabase(db_file, True, logsql_filename='/tmp/sqlite.log')
    dbrq.start()
    challenges=dbrq.getTupleFromTable('id,name', 'challenges', 'id=1')
    print(challenges)
    print ("Id available :", dbrq.getIdAvailable())
    print("VM running :", dbrq.isVmRunning("1ebde062-5e42-4d6b-90d8-a13355c0b890"))
    dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b890", 1001, 0, 1)
    print("VM running :", dbrq.isVmRunning("1ebde062-5e42-4d6b-90d8-a13355c0b890"))
    print("List of VM running :", dbrq.getListForVmRunning(vm_id=1, team_id=1001))
    print ("Id available :", dbrq.getIdAvailable())
    dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b892", 1002, 0, 2)
    print ("VM of team ID 0", dbrq.getVmRunning(team_id=0))
    print ("VM of team ID 2", dbrq.getVmRunning(team_id=2))
    print ("Id available :", dbrq.getIdAvailable())
    print ("VMs running :", dbrq.getVmRunning())
    dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b892")
    print ("Id available :", dbrq.getIdAvailable())
    dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b890")
    print ("Id available :", dbrq.getIdAvailable())
    print ("VMs running :", dbrq.getVmRunning())
    print("VM running :", dbrq.isVmRunning("1ebde062-5e42-4d6b-90d8-a13355c0b890"))
    dbrq.execute("DELETE FROM `current_vm` WHERE `uuid` = '1ebde062-5e42-4d6b-90d8-a13355c0b890'")
    dbrq.execute("DELETE FROM `current_vm` WHERE `uuid` = '1ebde062-5e42-4d6b-90d8-a13355c0b892'")
    dbrq.stop()
    dbrq.join()
