#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)


class AbstractVirtEngines():
    def __init__(self, engine_name, program_name, debug_enabled=False, callback_warning=None, callback_tosend=None):
        self.engine_name=engine_name
        self.debug_enabled=debug_enabled
        self.callback_warning=callback_warning
        self.callback_tosend=callback_tosend
        self.available=self.checkPrerequisite(program_name)
        self.engine_features={}
        self.isoAvailable=False

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def logWarning(self, msg):
        self.debug(msg)
        if self.callback_warning:
            self.callback_warning(msg)

    def toSend(self, data):
        # try to send to server
        if self.callback_tosend:
            self.callback_tosend(data)

    def checkPrerequisite(self, program_name):
        if program_name:
            try:
                import subprocess
                subprocess.call([program_name, '--version'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
                self.debug(f"Info : Start engine {self.engine_name} (found '{program_name}' program)")
                return True
            except:
                self.debug(f"Warning : Command {program_name} missing (required by {self.engine_name} engine)")
        return False

    def isAvailable(self):
        return self.available

    def getEngineFeatures(self):
        return self.engine_features

    def startDisposableVm(self, name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio=False, scsi_virtio=False, with_vnc=False, vnc_password=None, cpu=1):
        self.debug("AbstractVirtEngines:startDisposableVm not implemented (out of context)")
        return True, None

    def startMasterVm(self, name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio=False, scsi_virtio=False, vnc_password=None, cpu=1):
        self.debug("AbstractVirtEngines:startMasterVm not implemented (out of context)")
        return True, None

    def destroyVM(self, uuid):
        self.debug("AbstractVirtEngines:destroyVM not implemented (out of context)")
        return True

    def rebootVM(self, uuid):
        self.debug("AbstractVirtEngines:rebootVM not implemented (out of context)")
        return True

    def attachISO(self, uuid, filename, bootable=False):
        self.debug("AbstractVirtEngines:attachISO not implemented (out of context)")
        return True

    def getListImages(self):
        self.debug("AbstractVirtEngines:getListImages not implemented (out of context)")
        return ()

    def flushCache(self):
        self.debug("AbstractVirtEngines:flushCache not implemented (out of context)")
        return True

    def getUrlConsole(self, base, vm_id, password):
        self.debug("AbstractVirtEngines:getUrlConsole not implemented (out of context)")
        return ""

    def isIsoAvailable(self):
        return self.isoAvailable