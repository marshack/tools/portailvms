#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import os, re
import hmac, hashlib, pickle
import select, threading

from multiprocessing.connection import Client
from multiprocessing import ProcessError


class ClientSocket(threading.Thread):
    def __init__(self, server_address, secret, \
                 callback_receive=None, \
                 debug_enabled=False):
        threading.Thread.__init__(self)
        self.server_address=server_address
        self.secret=secret
        self.callback_receive=callback_receive
        self.debug_enabled=debug_enabled
        self.verbose=True if os.getenv('PORTAILVMS_DEBUG_VERBOSE') else False
        self.debug("ClientSocket::ClientSocket")
        self.callbackloginfo=None
        self.callbacklogwarning=None
        self.callbackconnected=None
        self.callbackdisconnected=None
        self.conn=None
        self.eventTerminated = threading.Event()

    def setCallBackLogInfo(self, callbackloginfo):
        self.callbackloginfo=callbackloginfo

    def setCallBackLogWarning(self, callbacklogwarning):
        self.callbacklogwarning=callbacklogwarning

    def setCallBackConnected(self, callbackconnected):
        self.callbackconnected=callbackconnected

    def setCallBackDisconnected(self, callbackdisconnected):
        self.callbackdisconnected=callbackdisconnected

    def logInfo(self, msg):
        if callable(self.callbackloginfo): self.callbackloginfo(msg)
        else: self.debug(msg)

    def logWarning(self, msg):
        if callable(self.callbacklogwarning): self.callbacklogwarning(msg)
        else: self.debug(msg)

    def debug(self, msg):
        if self.debug_enabled:
            print (msg)

    def getSignature(self, data):
        return hmac.new(self.secret, data, hashlib.sha1).digest()

    def unpackData(self, data):
        if self.verbose:
            self.debug("ClientSocket::unpackData")
        if isinstance(data, tuple) and len(data)==2:
            stream, rcv_hmac=data
            comp_hash=self.getSignature(stream)
            if comp_hash!=rcv_hmac:
                raise Exception('bad signature')
            return pickle.loads(stream)

    def sendData(self, data):
        if self.conn and not self.eventTerminated.is_set():
            if self.verbose:
                self.debug("ClientSocket::sendData : {}".format(data))
            try:
                stream=pickle.dumps(data)
                sig=hmac.new(self.secret, stream, hashlib.sha1).digest()
                self.conn.send((stream, sig))
                return True
            except Exception as e:
                self.logWarning("{}".format(e))
        return False

    def run(self):
        self.debug("ClientSocket::run")
        family='AF_UNIX' # default
        tmp_address=self.server_address
        if re.match(r'^\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}\:\d+$', self.server_address):
            family='AF_INET'
            tmp_struct=self.server_address.split(':')
            port=6542 if len(tmp_struct)==1 else int(tmp_struct[1])
            tmp_address=(tmp_struct[0], port)
        try:
            self.conn = Client(tmp_address, family=family, authkey=self.secret)
            self.logInfo("connection client started")
            if callable(self.callbackconnected): self.callbackconnected()
        except ProcessError as e:
            self.logWarning("client error (ProcessError): {}".format(e))
            return
        except PermissionError as e:
            self.logWarning("client error (PermissionError): {}".format(e))
            return
        except Exception as e:
            self.logWarning("client error: {}".format(e))
            return

        r = [self.conn]; w = []; x = []
        while not self.eventTerminated.is_set():
            r, w, x = select.select(r, w, x, 0.8)
            if self.conn in r:
                data=None
                try:
                    msg=self.conn.recv()
                    data=self.unpackData(msg)
                    if self.verbose:
                        self.debug("rcv data : {}".format(data))
                except EOFError:
                    self.logWarning('connection broken')
                    self.conn=None
                    break
                except Exception as e:
                    if not self.eventTerminated.is_set():
                        self.logWarning("An error occured : {}".format(e))

                if data and self.callback_receive:
                        self.callback_receive(data)
            r = [self.conn]

        if callable(self.callbackdisconnected): self.callbackdisconnected()
        self.stop() # close socket properly

    def stop(self):
        if self.eventTerminated.is_set():
            return # already stopped
        self.eventTerminated.set()
        self.debug("ClientSocket::stop")
        try:
            self.conn.close()
        except:
            pass
        self.logInfo("connection client stopped")

if __name__ == "__main__":
    def signal_handler(signal, frame):
        print("Abort ...")
        myclient.stop()

    def callback_receive(data):
        print("callback_receive : {}".format(data))

    import signal
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)
    
    server_address = "/tmp/tmpsocket.sock"
    secret=b'\x96\x8d\r\xbd\xbc.d\xaaYblDK\xee\xd9\xc8\tD)\x083l\xacf\x87\x0b\xcb#\xf9e__'

    myclient=ClientSocket(server_address, secret, \
              callback_receive, debug_enabled=True)

    myclient.start()
    import time
    time.sleep(0.2)
    myclient.sendData({ "target": "sleep", "uuid" : "my_uuid_1", "wait" : "10"})
    ##
    ## Examples
    ##
    # myclient.sendData({ "target": "killfromuuid", "uuid" : "57a61e0b-1d91-432d-bb83-ebf329898993"})
    # myclient.sendData({'target': 'startdisposablevm', 'name': 'alpine_team0_id3', 'id': '03', 'tmp_target_dir': '/serve/tmp/', 'ram': '1024', 'cpu' :'2', 'source': '/serve/origin//alpine-3.11.qcow2', 'uuid': 'c771707a-d920-4d14-a3a4-bfece14fcef3', 'from_address': '10.10.20.1', 'to_address': '10.10.20.203', 'net_virtio': '1', 'scsi_virtio': '0', 'with_vnc': '1', 'vnc_password': 'hjwJpVz9lzpp', 'guest_type': '0', 'keyboard' : 'fr'})
    # myclient.sendData({'target': 'startephemerallxd', 'name': 'lighttpd_service_team0_id4', 'id': '04', 'ram': '512', 'cpu' :'2', 'disk': '1500', 'image': 'lighttpd-service', 'uuid': 'f3217a6b-1499-4dfb-8c93-29916a3a4092', 'from_address': '10.10.20.1', 'to_address': '10.10.20.204'})
    # myclient.sendData({'target': 'startarchimulator', 'name': 'game_of_drones__2_avec_vnc_sur_vm_windows_team0_id5', 'id': '05', 'template': '/serve/archimulator/templates//game_of_drones_2_with_vnc.yaml', 'uuid': 'aafdcd3f-df9e-4294-b700-f063a32c6147', 'from_address': '10.10.20.1', 'to_address': '10.10.20.205', 'with_vnc': '1', 'vnc_password': 'r8T9YKtZCYs3'})
    # myclient.sendData({'target': 'startmaster', 'name': 'alpine', 'id': '00', 'ram': '1024', 'cpu' :'2', 'source': '/serve/origin//alpine-3.11.qcow2', 'uuid': 'fc070ee9-f3d2-4153-ac68-c3b25074b744', 'from_address': '10.10.20.1', 'to_address': '10.10.20.200', 'net_virtio': '1', 'scsi_virtio': '0', 'vnc_password': '1pKDSn9sJsbf', 'guest_type': '0' , 'keyboard' : 'fr'})
    # myclient.sendData({ "target": "cleanall", "uuid" : "MAINTENANCE"})
    myclient.join()

