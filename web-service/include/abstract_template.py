#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)


class AbstractTemplate(object):
    cache={}

    def __init__(self, path):
        self.path=path
        self.content=""
        self.replacement=[]

    def _load(self, r_file):
        if r_file in AbstractTemplate.cache.keys():
            self.content += AbstractTemplate.cache[r_file]
        else:
            with open("{}/{}".format(self.path, r_file)) as f:
                data=f.read()
                # add to cache
                AbstractTemplate.cache[r_file]=data
                self.content += data

    def replace(self, a , b):
         self.replacement.append((str(a),str(b)))

    def flushCache(self):
        self.cache.clear()
