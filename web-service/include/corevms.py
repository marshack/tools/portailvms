#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, uuid
import os, re, threading
from passlib import pwd

sys.path.append("../")
from include.host_ressources import HostRessources
from include.kvm_engine import KvmEngine
from include.lxc_engine import LxcEngine
from include.lxd_engine import LxdEngine
from include.docker_engine import DockerEngine
from include.archi_engine import ArchiEngine
from include.client import ClientSocket
from common.schedule import Schedule

class CoreVMS():
    def __init__(self, config, callback_warning=None, callback_loginfo=None, callback_finished=None, callback_initialized=None):
        self.debug_enabled=config['debug']
        self.verbose=True if os.getenv('PORTAILVMS_DEBUG_VERBOSE') else False
        self.callback_warning=callback_warning
        self.callback_loginfo=callback_loginfo
        self.callback_finished=callback_finished
        self.callback_initialized=callback_initialized
        self.originDir=config['origin_dir']
        self.tmpTargetDir=config['tmp_target_dir']
        self.tmpSockDir=config['tmp_socketvms_dir']
        self.cache_ISO_filesForPath={}
        self.collection_guest_type={}
        self.results_events={}
        self.lockResultDict=threading.Lock()
        self.maintenance_tech=True
        self.maintenance_admin=False
        self.maintenance_msg="System is in maintenance mode"
        self.secret=config['secret']
        self.kvm_engine=KvmEngine(config, self.debug_enabled, callback_warning, self.sendData)
        self.lxc_engine=LxcEngine(config, self.debug_enabled, callback_warning, self.sendData)
        self.lxd_engine=LxdEngine(self.debug_enabled, callback_warning, self.sendData)
        self.docker_engine=DockerEngine(self.debug_enabled, callback_warning, self.sendData)
        self.archi_engine=ArchiEngine(config, self.debug_enabled, callback_warning, self.sendData)
        self.list_engines=(self.kvm_engine, self.docker_engine, self.lxc_engine, self.lxd_engine, self.archi_engine)
        self.client=None
        self.eventTerminated = threading.Event()
        self.createClientSocket()
        self.schedule=Schedule()
        self.schedule.timer(60, self.callback_timer_check_client)
        self.schedule.start()

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def logWarning(self, msg):
        self.debug(msg)
        if self.callback_warning:
            self.callback_warning(msg)

    def logInfo(self, msg):
        self.debug(msg)
        if self.callback_loginfo:
            self.callback_loginfo(msg)

    def stop(self):
        self.debug('CoreVMS:stop')
        self.eventTerminated.set()
        self.schedule.stop()
        self.client.stop()
        self.client.join()
        self.schedule.join()

    def generateUUID(self):
        return str(uuid.uuid4())

    def generatePassword(self):
        return pwd.genword(length=12)

    def enableMaintModeTech(self, enable=True):
        if self.maintenance_tech!=enable:
            self.maintenance_tech=enable
            msg="Technical maintenance mode : "
            msg+="On" if enable else "Off"
            self.logInfo(msg)

    def enableMaintModeAdmin(self, enable=True):
        if self.maintenance_admin!=enable:
            self.maintenance_admin=enable
            msg="Admin maintenance mode : "
            msg+="On" if enable else "Off"
            self.logInfo(msg)


    def getAllAvailableEngineFeatures(self):
        engine_feat={}
        for obj in self.list_engines:
            if obj.isAvailable():
                engine_feat.update(obj.getEngineFeatures())
        return engine_feat

    def isInMaintenanceMode(self):
        return self.maintenance_admin or self.maintenance_tech

    def isInMaintenanceAdminMode(self):
        return self.maintenance_admin

    def isInMaintenanceTechMode(self):
        return self.maintenance_tech

    def callback_receive(self, data):
        self.debug("CoreVMS:callback_receive")
        if "origin" in data and "uuid" in data and "success" in data:
            origin=data["origin"]
            res=data["success"]
            uuid=data["uuid"]
            if 'maintenance' in data:
                # receive from server, update locally
                self.enableMaintModeTech(data["maintenance"])

            # if use sync. see syncSendData method
            token=(origin, uuid)
            self.lockResultDict.acquire()
            if token in self.results_events:
                evt=self.results_events[token]
                self.results_events[token]=data
                evt.set() # unlock thread
            self.lockResultDict.release()

            if origin=="cleanall" and uuid=="MAINTENANCE":
                pass # Nothing to do
            elif origin in ("createqcow2", "moveuploaded"):
                pass # Nothing to do
            elif origin=="initialize" and uuid=="MAINTENANCE":
                uuids=tuple(self.collection_guest_type.keys())
                for uuid in uuids:
                    self.vmFinished(uuid, True, '')
                if callable(self.callback_initialized): self.callback_initialized() # update db to set all vm stopped
            else:
                if 'running' in data and not data["running"]:
                    msg=data["msg"] if 'msg' in data else ""
                    self.vmFinished(uuid, res, msg)

    def vmFinished(self, uuid, res, msg):
        if callable(self.callback_finished): self.callback_finished(uuid, res, msg)

    def setVmStoppedByUuid(self, uuid):
        if uuid in self.collection_guest_type.keys():
            self.collection_guest_type.pop(uuid)

    def getEngineObjectByEngineId(self, engine_id):
        for engine in self.list_engines:
            if engine_id in engine.engine_features:
                return engine
        return self.kvm_engine # default engine

    def routeEngine(self, uuid):
        guest_type=0
        if uuid in self.collection_guest_type.keys():
            guest_type=self.collection_guest_type[uuid]
        return self.getEngineObjectByEngineId(guest_type)

    def startDisposableVm(self, name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio=False, scsi_virtio=False, with_vnc=False, vnc_password=None, cpu=1):
        self.debug('CoreVMS:startDisposableVm\nname={}, id={}, vol_size={}, ram={}, cpu={}, guest_type={}, source={}, uuid={}, from_address={}, to_address={}, net_virtio={}, scsi_virtio={}, with_vnc={}, vnc_password={}'.format(
            name, id, vol_size, ram, cpu, guest_type, source, uuid, from_address, to_address, net_virtio, scsi_virtio, with_vnc, vnc_password))
        if self.maintenance_tech or self.maintenance_admin:
            return False, self.maintenance_msg

        if not self.isRessourcesAvailable(vol_size, ram):
            return False, "Resources unavailable"

        self.collection_guest_type[uuid]=guest_type
        return self.routeEngine(uuid).startDisposableVm(name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio, scsi_virtio, with_vnc, vnc_password, cpu)

    def startMasterVm(self, name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio=False, scsi_virtio=False, vnc_password=None, cpu=1):
        self.debug('CoreVMS:startMasterVm\nname={}, id={}, vol_size={}, ram={}, cpu={}, guest_type={}, source={}, uuid={}, from_address={}, to_address={}, net_virtio={}, scsi_virtio={}, vnc_password={}'.format(
            name, id, vol_size, ram, cpu, guest_type, source, uuid, from_address, to_address, net_virtio, scsi_virtio, vnc_password))
        if self.maintenance_tech or self.maintenance_admin:
            return False, self.maintenance_msg
        if not self.isRessourcesAvailable(vol_size, ram):
            return False, "Resources unavailable"

        self.collection_guest_type[uuid]=guest_type
        return self.routeEngine(uuid).startMasterVm(name, id, vol_size, ram, guest_type, source, uuid, from_address, to_address, net_virtio, scsi_virtio, vnc_password, cpu)

    def destroyVM(self, uuid):
        if self.maintenance_tech or self.maintenance_admin:
            return False

        res=self.routeEngine(uuid).destroyVM(uuid)
        if not res:
            # try to kill process
            self.logWarning("Impossible to destroy cleanly {} -> term/kill process".format(uuid))
            res, _=self.sendData({ "target": "killfromuuid", "uuid" : uuid})
        self.setVmStoppedByUuid(uuid)
        return res

    def rebootVM(self, uuid):
        if self.maintenance_tech or self.maintenance_admin:
            return False
        return self.routeEngine(uuid).rebootVM(uuid)

    def attachISO(self, uuid, filename, bootable=False):
        if self.maintenance_tech or self.maintenance_admin:
            return False
        return self.routeEngine(uuid).attachISO(uuid, filename, bootable)

    def getListIsoFiles(self, path):
        self.debug("CoreVMS:getListIsoFiles - path={}".format(path))
        if path in self.cache_ISO_filesForPath.keys():
            return self.cache_ISO_filesForPath[path]
        try:
            l=os.listdir(path)
        except:
            self.logWarning("Impossible to get list of ISO file")
            l=[]
        list_iso_files = sorted([ c for c in l if c.lower().endswith('.iso')], key=str.lower)
        self.cache_ISO_filesForPath[path]=list_iso_files
        return list_iso_files

    def getListImages(self, engine_id):
        return sorted(self.getEngineObjectByEngineId(engine_id).getListImages(), key=str.lower)

    def getUrlForConsole(self, engine_id, url_base, vm_id, password):
        return self.getEngineObjectByEngineId(engine_id).getUrlConsole(url_base, vm_id, password)

    def isIsoAvailable(self, uuid):
        return self.routeEngine(uuid).isIsoAvailable()

    def flushCache(self):
        self.cache_ISO_filesForPath.clear()
        for engine in self.list_engines:
            engine.flushCache()

    def isRessourcesAvailable(self, vol_size_req, ram_req, ram_reserved=0):
        self.debug('CoreVMS:isRessourcesAvailable - vol_size_req={}, ram_req={}, ram_reserved={}'.format(vol_size_req, ram_req, ram_reserved))
        # convert MB to Bytes
        ram_req=ram_req*1000
        vol_size_req=vol_size_req*1000
        ram_reserved=ram_reserved*1000
        disk_space_ok=False
        ram_space_ok=False
        hostrs=HostRessources()
        # check disk space
        disk_total=int(hostrs.getDiskTotal(self.tmpTargetDir)/1000)
        disk_available=int(hostrs.getDiskFree(self.tmpTargetDir)/1000)
        disk_total_limit=int(disk_total*90/100)
        if self.verbose:
            self.debug("disk available : {} (total : {})".format(disk_available, disk_total))
        disk_used=disk_total-disk_available
        if (vol_size_req + disk_used > disk_total_limit):
            self.logWarning("ressource disk unavailable !")
            return False
        else:
            disk_space_ok=True

        # check ram free space
        mem_total=int(hostrs.getMemoryTotal()/1000)
        mem_available=int(hostrs.getMemoryAvailable()/1000)
        mem_total_limit=int(mem_total*90/100)
        if self.verbose:
            self.debug("memory available : {} (total : {})".format(mem_available, mem_total))
        mem_used=mem_total-mem_available
        if (ram_req + mem_used > mem_total_limit) or (ram_req + ram_reserved > mem_total_limit):
            self.logWarning("ressource mem unavailable !")
            return False
        else:
            ram_space_ok=True
        return disk_space_ok and ram_space_ok


    def cleanall(self):
        self.debug("CoreVMS:cleanall")
        # stop all VM (kill !!) and purge tmpfiles
        if self.maintenance_tech or self.maintenance_admin:
            return False, self.maintenance_msg
        self.enableMaintModeTech(True)
        ret, msg=self.sendData({ "target": "cleanall", "uuid" : "MAINTENANCE"})
        self.collection_guest_type.clear()
        return ret, msg

    def createqcow2(self, name, size):
        self.debug("CoreVMS:createqcow2")
        if self.maintenance_tech or self.maintenance_admin:
            return False, self.maintenance_msg
        #normalize filename
        name=re.sub(r'[^A-Za-z0-9\.\_\-]', '_', name)
        filename="{}/{}".format(self.originDir, name)
        if os.path.isfile(filename):
            return False, "File {} already exists".format(filename)
        # check disk free space
        hostrs=HostRessources()
        dfree=int(hostrs.getDiskFree(self.tmpTargetDir)/1000000)
        if size>dfree:
            return False, "Not enough free space on the disk"
        res, msg=self.syncSendData({ "target": "createqcow2", "uuid" : name, "name" : name, "size": size}, timeout=4)
        if res: self.kvm_engine.flushCache() # to refresh file list
        return res, msg

    def moveuploaded(self, name, tpe, force=False):
        self.debug("CoreVMS:moveuploaded")
        if self.maintenance_tech or self.maintenance_admin:
            return False, self.maintenance_msg
        res, msg=self.syncSendData({ "target": "moveuploaded", "uuid" : name, "name" : name, "type": tpe, "force": str(int(force))}, timeout=90)
        if res:
            self.logInfo(f"{name} has been correctly moved")
            self.flushCache() # to refresh file list
        return res, msg

    # async sending
    def sendData(self, data):
        if self.client and self.client.is_alive():
            return self.client.sendData(data), ""
        return False, "An error occured, please try later ..."

    # sync sending (lock until data is received or timeout)
    def syncSendData(self, data, timeout=20):
        if self.client and self.client.is_alive():
            token=(data['target'], data['uuid'])
            evt=threading.Event()
            self.lockResultDict.acquire()
            self.results_events.setdefault(token, evt)
            self.lockResultDict.release()
            self.sendData(data)

            is_ok=evt.wait(timeout) # lock

            self.lockResultDict.acquire()
            res=self.results_events.pop(token)
            self.lockResultDict.release()

            if is_ok and res and isinstance(res, dict) and 'success' in res:
                msg=res['msg'] if 'msg' in res else ""
                return res['success'], msg

        return False, "An error occured, please try later ..."

    def clientConnected(self):
        self.debug("CoreVMS:clientConnected")
        self.enableMaintModeTech(False)

    def clientDisconnected(self):
        self.debug("CoreVMS:clientDisconnected")
        self.enableMaintModeTech(True)

    def createClientSocket(self):
        if self.client==None or not self.client.is_alive():
            self.client=ClientSocket("{}/launcher.sock".format(self.tmpSockDir), self.secret, self.callback_receive, debug_enabled=self.debug_enabled)
            self.client.setCallBackLogWarning(self.logWarning)
            self.client.setCallBackLogInfo(self.logInfo)
            self.client.setCallBackConnected(self.clientConnected)
            self.client.setCallBackDisconnected(self.clientDisconnected)
            self.client.start()

    def callback_timer_check_client(self):
        if not self.client.is_alive() and not self.eventTerminated.is_set():
            self.debug("Connection to the server seems closed. New attempt ... ")
            self.createClientSocket()

