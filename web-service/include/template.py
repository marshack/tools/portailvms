#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, re

sys.path.append("../")
from include.abstract_template import AbstractTemplate
from include.widget import Widget
import include.globals as globals

class Template(AbstractTemplate):
    def __init__(self):
        AbstractTemplate.__init__(self,"templates/pages/")
        self.message=""

    def load(self, r_file):
        self._load("header.html")
        self._load(r_file)
        self._load("footer.html")
    def info(self, message):
        tplwgt=Widget()
        tplwgt.load('messages/info.html')
        tplwgt.replace("__INFO_MESSAGE__", message)
        self.message+=tplwgt.generate()
    def warning(self, message):
        tplwgt=Widget()
        tplwgt.load('messages/warning.html')
        tplwgt.replace("__WARNING_MESSAGE__", message)
        self.message+=tplwgt.generate()
    def error(self, message):
        tplwgt=Widget()
        tplwgt.load('messages/error.html')
        tplwgt.replace("__ERROR_MESSAGE__", message)
        self.message+=tplwgt.generate()

    def generate(self, title="", page=""):
        username=globals.getCurrentUsername()
        is_admin=globals.isAdmin()
        is_auth=globals.isAuthenticated()
        self.replace("__TITLE__", title)

        if is_auth:
            tplwgt=Widget()
            tplwgt.load('navbar/authenticated.html')
            tplwgt.replace('__USERNAME__', username)
            self.replace("__AUTHENTICATED__",tplwgt.generate())

        if is_admin:
            tplwgt=Widget()
            tplwgt.load('navbar/administrator.html')
            self.replace("__ADMIN__",tplwgt.generate())

        self.replace("__ALERT_BOX__", self.message)
        self.replace("__NAV_CLASS_{}__".format(page.upper()),"active")
        for a, b in self.replacement:
            self.content=self.content.replace(a, b)
        # remove tags forgotten
        self.content=re.sub(r'__\S+__', '', self.content)
        return self.content


if __name__ == "__main__":
    tmpl=Template()
    tmpl.load('authenticated.html')
    tmpl.replace(" __VERSION__", "1.0.0")
    tmpl.info("message info")
    tmpl.warning("warning info")
    tmpl.error("error info")
    content=tmpl.generate("title", "currentuser")
    print(content)
