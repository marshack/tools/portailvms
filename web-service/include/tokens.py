#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import os
import hmac, hashlib
import time
import base64
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.backends import default_backend

class Tokens():
    def __init__(self, secret, lifetime=180, debug=False):
        self.secret_sign=self.generate_derived_key(secret, b'\x1d\x19\x00W\xf4\xb3\xb7\x99', 100001, 32)
        self.secret_crypt=self.generate_derived_key(secret, b'5\xd9\xc5l\x17\x8b\xfa\x9e', 100135, 32)
        self.lifetime=lifetime
        self.debug_enabled=debug
        self.tokenBlacklist=[]
        self.limit_blacklist_size=5000
        self.salt_size=6
        self.hmac_size=20

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def getCurrentTimestamp(self):
        return time.time()


    def generate_derived_key(self, key, salt, iterations=100000, length=56):
        assert key!=None
        assert length>=32

        backend = default_backend()
        # derive
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=length,
            salt=salt,
            iterations=iterations,
            backend=backend
        )
        return kdf.derive(key)

    def getRandomNumber(self, size):
        return os.urandom(size)

    def encodeBase64(self, data):
        return base64.b64encode(data).decode()

    def cipher(self, data):
        bs = int(algorithms.AES.block_size/8)
        backend = default_backend()
        iv = self.getRandomNumber(bs)
        cipher = Cipher(algorithms.AES(self.secret_crypt), modes.CBC(iv), backend=backend)
        encryptor = cipher.encryptor()
        padding_len = bs - divmod(len(data), bs)[1]
        padding=bytes([padding_len])*padding_len
        ct = encryptor.update(data + padding) + encryptor.finalize()
        return iv + ct

    def uncipher(self, data):
        bs = int(algorithms.AES.block_size/8)
        if len(data)<2*bs:
            return None

        iv=data[0:bs]
        ct=data[bs:]
        try:
            backend = default_backend()
            cipher = Cipher(algorithms.AES(self.secret_crypt), modes.CBC(iv), backend=backend)
            decryptor = cipher.decryptor()
            data_decrypted=decryptor.update(ct) + decryptor.finalize()
            padding_len = data_decrypted[-1]
            data_decrypted = data_decrypted[:-padding_len]
            return data_decrypted
        except:
            pass
        return None

    def getToken(self, data, timebased=False, encrypted=False):
        salt=self.getRandomNumber(self.salt_size)
        if timebased:
            data+="|{}".format(self.getCurrentTimestamp())
        data=data.encode()
        if encrypted:
            data=self.cipher(data)
        _hmac=hmac.new(self.secret_sign, salt+data, hashlib.sha1)
        token=base64.urlsafe_b64encode(salt+_hmac.digest()+data)
        return token.decode()

    def checkToken(self, token, timebased=False, encrypted=False):
        try:
            res=base64.urlsafe_b64decode(token)
        except Exception as e:
            self.debug("Token error : {}".format(e))
            return False, None

        if len(res)<self.salt_size+self.hmac_size:
            self.debug("Token is invalid")
            return False, None

        h=hashlib.sha1(res).digest()
        if h in self.tokenBlacklist:
            # already used
            self.debug('token already used !')
            return False, None
        else:
            # add it
            self.tokenBlacklist.append(h)
            # limit size of this list
            if len(self.tokenBlacklist)>self.limit_blacklist_size:
                self.tokenBlacklist.remove(self.tokenBlacklist[0])

        salt=res[0:self.salt_size]
        _hmac_rcv=res[self.salt_size:self.salt_size+self.hmac_size]
        data=res[self.salt_size+self.hmac_size:]
        current_hmac=hmac.new(self.secret_sign, salt+data, hashlib.sha1).digest()

        if current_hmac!=_hmac_rcv:
            self.debug('token : bad signature !')
            return False, None

        if encrypted:
            data=self.uncipher(data)
            if not data:
                self.debug('token : impossible to uncipher !')
                return False, None
        try:
            data=data.decode()
        except:
            return False, None

        if not timebased:
            return True, data

        try:
            data, time=data.rsplit('|', 1)
            currenttime=self.getCurrentTimestamp()
            time=float(time)
        except Exception as e:
            self.debug("Token error : {}".format(e))
            return False, None

        if currenttime-time>self.lifetime:
            self.debug('token expired !')
            return False, None
        else:
            return True, data

if __name__ == "__main__":
    tokens=Tokens(b'secret', lifetime=1, debug=True)
    t=tokens.getToken('A')
    isValid, data=tokens.checkToken(t)
    assert isValid==True
    assert data=='A'
    t=tokens.getToken('B', timebased=True)
    isValid, data=tokens.checkToken(t, True)
    assert isValid==True
    assert data=='B'
    # check expired
    import time
    t=tokens.getToken('C', timebased=True)
    time.sleep(1.2)
    isValid, data=tokens.checkToken(t, True)
    assert isValid==False
    assert data==None
    # check altered
    t=tokens.getToken('D', True)
    res=base64.urlsafe_b64decode(t)
    salt=res[0:6]
    _hmac=res[6:26]
    data=res[26:]
    data,timestamp=data.decode().split("|")
    data="E|{}".format(timestamp).encode()
    new_token=base64.urlsafe_b64encode(salt+_hmac+data).decode()
    isValid, data=tokens.checkToken(new_token, True)
    assert isValid==False
    assert data==None
    t=tokens.getToken('F', timebased=True, encrypted=True)
    isValid, data=tokens.checkToken(t, timebased=True, encrypted=True)
    assert isValid==True
    assert data=='F'
    t=tokens.getToken('F|G|H', timebased=True, encrypted=True)
    isValid, data=tokens.checkToken(t, timebased=True, encrypted=True)
    assert isValid==True
    assert data=='F|G|H'
