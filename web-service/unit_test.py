#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import os
import unittest, time
import base64
from include.tokens import Tokens
from include.requestdatabase import RequestDatabase
from include.statistiques import Statistiques, TypeCollect, TypeDate
from include.sqliteworker import SqliteWorker
from include.template import Template

class TemplateTest(unittest.TestCase):
    def setUp(self):
        self.tmpl=Template()
        self.tmpl.path="utests/templates/"

    def tearDown(self):
        pass

    def testRemoveAllTags(self):
        self.tmpl._load("sample1.txt")
        self.assertEqual(self.tmpl.generate(), "0  1  2  3")

    def testReplaceOneTag(self):
        self.tmpl._load("sample1.txt")
        self.tmpl.replace("__QWERTY1__", "/")
        self.assertEqual(self.tmpl.generate(), "0 / 1  2  3")

    def testReplaceAllTags(self):
        self.tmpl._load("sample1.txt")
        self.tmpl.replace("__QWERTY1__", "/")
        self.tmpl.replace("__QWERTY2__", "/")
        self.tmpl.replace("__QWERTY3__", "/")
        self.assertEqual(self.tmpl.generate(), "0 / 1 / 2 / 3")

    def testReplaceMultilines(self):
        self.tmpl._load("sample2.txt")
        self.tmpl.replace("__QWERTY1__", "/")
        self.tmpl.replace("__QWERTY2__", "/")
        self.tmpl.replace("__QWERTY3__", "/")
        self.tmpl.replace("__QWERTY9__", "/")
        self.assertEqual(self.tmpl.generate(), "0 / 1 / 2 / 3\n0 / 1 / 2 / 3\n8 / 9")

    def testMisc(self):
        self.tmpl._load("sample3.txt")
        self.assertEqual(self.tmpl.generate(), "0 _DONOTREMOVE_ 1  2 _DONOTREMOVE__2_ 3 __DONOTREMOVE_ 4")

class TokensTest(unittest.TestCase):
    def setUp(self):
        self.tokens=Tokens(b'secret', lifetime=0.2)

    def tearDown(self):
        pass

    def test_goodToken(self):
        t=self.tokens.getToken('A')
        isValid, data=self.tokens.checkToken(t)
        self.assertEqual(data, 'A')
        self.assertTrue(isValid)

    def test_goodTimeBasedToken(self):
        t=self.tokens.getToken('B', True)
        isValid, data=self.tokens.checkToken(t, True)
        self.assertEqual(data, 'B')
        self.assertTrue(isValid)

    def test_expiredToken(self):
        t=self.tokens.getToken('C', True)
        time.sleep(0.3)
        isValid, data=self.tokens.checkToken(t, True)
        self.assertEqual(data, None)
        self.assertFalse(isValid)

    def test_alreadyUsedToken(self):
        t=self.tokens.getToken('D', True)
        isValid, data=self.tokens.checkToken(t, True)
        self.assertEqual(data, 'D')
        self.assertTrue(isValid)
        isValid, data=self.tokens.checkToken(t, True)
        self.assertEqual(data, None)
        self.assertFalse(isValid)

    def test_alteredToken(self):
        t=self.tokens.getToken('E')
        res=base64.urlsafe_b64decode(t)
        salt=res[0:6]
        _hmac=res[6:26]
        data=res[26:]
        data=b"F"
        new_token=base64.urlsafe_b64encode(salt+_hmac+data).decode()
        isValid, data=self.tokens.checkToken(new_token)
        self.assertEqual(data, None)
        self.assertFalse(isValid)

    def test_badSecret(self):
        tokens2=Tokens(b'secret2', lifetime=0.2, debug=True)
        t=tokens2.getToken('G')
        isValid, data=self.tokens.checkToken(t)
        self.assertEqual(data, None)
        self.assertFalse(isValid)

    def test_encryptedToken(self):
        t=self.tokens.getToken('H', encrypted=True)
        isValid, data=self.tokens.checkToken(t, encrypted=True)
        self.assertEqual(data, 'H')
        self.assertTrue(isValid)

    def test_encryptedTokenWithBadSecret(self):
        t=self.tokens.getToken('I', encrypted=True)
        self.tokens.secret_crypt=self.tokens.generate_derived_key(b'secret2', b'5\xd9\xc5l\x17\x8b\xfa\x9e', 100135, 32)
        isValid, data=self.tokens.checkToken(t, encrypted=True)
        self.assertEqual(data, None)
        self.assertFalse(isValid)

    def test_dataContainsSeparator(self):
        t=self.tokens.getToken('F|G|H', timebased=True, encrypted=True)
        isValid, data=self.tokens.checkToken(t, timebased=True, encrypted=True)
        self.assertEqual(data, 'F|G|H')
        self.assertTrue(isValid)

class SqliteWorkerTest(unittest.TestCase):
    def setUp(self):
        self.dbrq=SqliteWorker(':memory:')
        self.dbrq.warning=lambda *args: None
        self.dbrq.start()
        self.dbrq.execute('CREATE TABLE clients (id integer PRIMARY KEY, name text NOT NULL)')

    def tearDown(self):
        self.dbrq.stop()
        self.dbrq.join()

    def test_execute(self):
        result, values=self.dbrq.execute("INSERT INTO clients VALUES (1, 'A')")
        self.assertTrue(result)
        self.assertEqual(values, [])
        result, values=self.dbrq.execute("INSERT INTO clients VALUES (2, 'B')")
        self.assertTrue(result)
        self.assertEqual(values, [])
        result, values=self.dbrq.execute("SELECT * FROM clients")
        self.assertTrue(result)
        self.assertEqual(values, [(1, 'A'), (2, 'B')])
        result, values=self.dbrq.execute("DELETE FROM clients WHERE 1=1")
        self.assertTrue(result)
        self.assertEqual(values, [])

    def test_execute_bad_requests(self):
        # no such table: unknown
        result, values=self.dbrq.execute("INSERT INTO unknown VALUES (1, 'A')")
        self.assertFalse(result)
        self.assertEqual(values, [])
        # datatype mismatch
        result, values=self.dbrq.execute("INSERT INTO clients VALUES ('A', 'A')")
        self.assertFalse(result)
        self.assertEqual(values, [])
        # syntax error
        result, values=self.dbrq.execute("BAD SYNTAX INTO clients VALUES (1, 'A')")
        self.assertFalse(result)
        self.assertEqual(values, [])

class RequestDatabaseTest(unittest.TestCase):
    def setUp(self):
        self.tmpDatabaseFile="utests/tmp_database.sqlite3"
        os.system("cp utests/database.sqlite3 {}".format(self.tmpDatabaseFile))
        self.dbrq=RequestDatabase(self.tmpDatabaseFile)
        self.dbrq.start()

    def tearDown(self):
        self.dbrq.stop()
        os.remove(self.tmpDatabaseFile)

    def test_getTupleFromTable(self):
        res=self.dbrq.getTupleFromTable('id,name', 'challenges', 'id=2')
        self.assertEqual(res, [(2, 'Ubuntu16.04')])

    def test_getIdAvailable(self):
        res=self.dbrq.getIdAvailable(max=3)
        self.assertEqual(res, 0)
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b891", 1, 0, res)
        res=self.dbrq.getIdAvailable(max=3)
        self.assertEqual(res, 1)
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b892", 2, 0, res)
        res=self.dbrq.getIdAvailable(max=3)
        self.assertEqual(res, 2)
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b893", 1, 0, res)
        res=self.dbrq.getIdAvailable(max=3)
        # no id available
        self.assertEqual(res, None)
        self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b892")
        res=self.dbrq.getIdAvailable(max=3)
        self.assertEqual(res, 1)
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b892", 2, 0, res)
        self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b891")
        res=self.dbrq.getIdAvailable(max=3)
        self.assertEqual(res, 0)
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b894", 1, 0, res)
        self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b892")
        res=self.dbrq.getIdAvailable(max=3)
        self.assertEqual(res, 1)
        self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b893")
        self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b894")
        res=self.dbrq.getIdAvailable(max=3)
        self.assertEqual(res, 2)
        self.dbrq.lastReturnedId=None
        for i in range(0, 14):
            res=self.dbrq.getIdAvailable(max=5)
            self.assertEqual(res, (i % 5))

    def test_getVmIdRunning(self):
        res=self.dbrq.getVmIdRunning()
        self.assertEqual(res, [])
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b890", 1, 0, 1)
        res=self.dbrq.getVmIdRunning()
        self.assertEqual(res, [1])
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b892", 2, 1, 2)
        res=self.dbrq.getVmIdRunning()
        self.assertEqual(res, [1, 2])
        self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b892")
        res=self.dbrq.getVmIdRunning()
        self.assertEqual(res, [1])
        self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b890")
        res=self.dbrq.getVmIdRunning()
        self.assertEqual(res, [])

    def test_getVmRunning(self):
        res=self.dbrq.getVmRunning()
        self.assertEqual(res, [])
        res=self.dbrq.getVmRunning(team_id=0)
        self.assertEqual(res, [])
        res=self.dbrq.getVmRunning(team_id=1)
        self.assertEqual(res, [])
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b890", 1, 0, 1)
        res=self.dbrq.getVmRunning()
        self.assertEqual(res[0][0], '1ebde062-5e42-4d6b-90d8-a13355c0b890')
        res=self.dbrq.getVmRunning(team_id=0)
        self.assertEqual(res[0][0], '1ebde062-5e42-4d6b-90d8-a13355c0b890')
        res=self.dbrq.getVmRunning(team_id=1)
        self.assertEqual(res, [])
        res=self.dbrq.getVmRunning(vm_id=1)
        self.assertEqual(res[0][0], '1ebde062-5e42-4d6b-90d8-a13355c0b890')
        res=self.dbrq.getVmRunning(vm_id=0)
        self.assertEqual(res, [])
        res=self.dbrq.getVmRunning(team_id=0, vm_id=1)
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b891", 1, 0, 3)
        res=self.dbrq.getVmRunning(vm_id=1)
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0][0], '1ebde062-5e42-4d6b-90d8-a13355c0b890')
        res=self.dbrq.getVmRunning(vm_id=3)
        self.assertEqual(res[0][0], '1ebde062-5e42-4d6b-90d8-a13355c0b891')
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b892", 2, 1, 2)
        res=self.dbrq.getVmRunning()
        self.assertEqual(res[0][0], '1ebde062-5e42-4d6b-90d8-a13355c0b890')
        self.assertEqual(res[1][0], '1ebde062-5e42-4d6b-90d8-a13355c0b891')
        self.assertEqual(len(res), 3)
        res=self.dbrq.getVmRunning(team_id=0)
        self.assertEqual(res[0][0], '1ebde062-5e42-4d6b-90d8-a13355c0b890')
        res=self.dbrq.getVmRunning(team_id=1)
        self.assertEqual(res[0][0], '1ebde062-5e42-4d6b-90d8-a13355c0b892')
        self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b890")
        res=self.dbrq.getVmRunning()
        self.assertEqual(res[0][0], '1ebde062-5e42-4d6b-90d8-a13355c0b891')
        res=self.dbrq.getVmRunning(team_id=0)
        self.assertEqual(res[0][0], '1ebde062-5e42-4d6b-90d8-a13355c0b891')
        res=self.dbrq.getVmRunning(team_id=1)
        self.assertEqual(res[0][0], '1ebde062-5e42-4d6b-90d8-a13355c0b892')
        self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b892")
        res=self.dbrq.getVmRunning()
        self.assertEqual(res[0][0], '1ebde062-5e42-4d6b-90d8-a13355c0b891')
        self.assertEqual(len(res), 1)
        self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b891")
        res=self.dbrq.getVmRunning(team_id=0)
        self.assertEqual(res, [])
        res=self.dbrq.getVmRunning(team_id=1)
        self.assertEqual(res, [])
        res=self.dbrq.getVmRunning()
        self.assertEqual(res, [])

    def test_getVmRunning_withTeamName(self):
        self.assertEqual([], self.dbrq.getVmRunning())
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b890", 1, 0, 1)
        res=self.dbrq.getVmRunning(team_id=0)
        self.assertEqual(0, res[0][3])
        self.dbrq.updateTeamNameFromId(0, 'admin')
        res=self.dbrq.getVmRunning(team_id=0)
        self.assertEqual('admin', res[0][3])

    def test_getListForVmRunning(self):
        res=self.dbrq.getListForVmRunning()
        self.assertEqual(len(res), 0)
        self.assertEqual(res, [])
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b890", 1, 0, 1)
        res=self.dbrq.getListForVmRunning()
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0]['vm_id'], 1)
        self.assertEqual(res[0]['uuid'], "1ebde062-5e42-4d6b-90d8-a13355c0b890")
        self.assertEqual(res[0]['challenge'], 1)
        self.assertEqual(res[0]['team'], 0)
        self.assertEqual(res[0]['vnc'], True)
        self.assertEqual('mount_iso' in res[0].keys(), False)
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b891", 2, 0, 2)
        res=self.dbrq.getListForVmRunning()
        self.assertEqual(len(res), 2)
        self.assertEqual(res[0]['vm_id'], 1)
        self.assertEqual(res[0]['uuid'], "1ebde062-5e42-4d6b-90d8-a13355c0b890")
        self.assertEqual(res[0]['challenge'], 1)
        self.assertEqual(res[0]['team'], 0)
        self.assertEqual(res[0]['vnc'], True)
        self.assertEqual('mount_iso' in res[0].keys(), False)
        self.assertEqual(res[1]['vm_id'], 2)
        self.assertEqual(res[1]['uuid'], "1ebde062-5e42-4d6b-90d8-a13355c0b891")
        self.assertEqual(res[1]['challenge'], 2)
        self.assertEqual(res[1]['team'], 0)
        self.assertEqual(res[1]['vnc'], True)
        self.assertEqual(res[1]['mount_iso'], True)

    def test_getListForVmRunning_withTeamName(self):
        self.assertEqual([], self.dbrq.getListForVmRunning())
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b890", 1, 0, 1)
        res=self.dbrq.getListForVmRunning()
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0]['team'], 0)
        self.dbrq.updateTeamNameFromId(0, 'admin')
        res=self.dbrq.getListForVmRunning()
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0]['team'], 'admin')

    def test_isVmRunning(self):
        res=self.dbrq.isVmRunning("1ebde062-5e42-4d6b-90d8-a13355c0b890")
        self.assertFalse(res)
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b890", 1, 0, 1)
        res=self.dbrq.isVmRunning("1ebde062-5e42-4d6b-90d8-a13355c0b890")
        self.assertTrue(res)
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b892", 2, 1, 2)
        res=self.dbrq.isVmRunning("1ebde062-5e42-4d6b-90d8-a13355c0b890")
        self.assertTrue(res)
        res=self.dbrq.isVmRunning("1ebde062-5e42-4d6b-90d8-a13355c0b892")
        self.assertTrue(res)
        self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b890")
        res=self.dbrq.isVmRunning("1ebde062-5e42-4d6b-90d8-a13355c0b890")
        self.assertFalse(res)
        res=self.dbrq.isVmRunning("1ebde062-5e42-4d6b-90d8-a13355c0b892")
        self.assertTrue(res)
        self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b892")
        res=self.dbrq.isVmRunning("1ebde062-5e42-4d6b-90d8-a13355c0b890")
        self.assertFalse(res)
        res=self.dbrq.isVmRunning("1ebde062-5e42-4d6b-90d8-a13355c0b892")
        self.assertFalse(res)

    def test_updateTeamNameFromId(self):
        self.assertEqual('0', self.dbrq.getTeamNameFromId(0))
        self.assertTrue(self.dbrq.updateTeamNameFromId(0, 'admin'))
        self.assertEqual('admin', self.dbrq.getTeamNameFromId(0))

    def test_getStatsVmPerTeam(self):
        self.assertEqual([], self.dbrq.getStatsVmPerTeam())
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b890", 1, 0, 1)
        self.assertEqual([(0, 1, 1)], self.dbrq.getStatsVmPerTeam())
        self.assertTrue(self.dbrq.updateTeamNameFromId(0, 'admin'))
        self.assertEqual([('admin', 1, 1)], self.dbrq.getStatsVmPerTeam())

    def test_getIsoPathOfChallengeId(self):
        self.assertEqual(self.dbrq.getIsoPathOfChallengeId(1), "")
        self.assertEqual(self.dbrq.getIsoPathOfChallengeId(2), "/serve/ISO/")

    def test_addNewVmDisposable(self):
        res=self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b890", 1, 0, 1)
        self.assertTrue(res)
        # already exist
        res=self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b890", 2, 0, 1)
        self.assertFalse(res)

    def test_setVmDisposableStopped(self):
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b890", 1, 0, 1)
        res=self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b892")
        self.assertFalse(res)
        res=self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b890")
        self.assertTrue(res)
        # already stopped
        res=self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b890")
        self.assertFalse(res)

    def test_getMemoryReserved(self):
        self.assertEqual(0, self.dbrq.getMemoryReserved())
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b891", 1, 0, 1)
        self.assertEqual(4096, self.dbrq.getMemoryReserved())
        self.dbrq.addNewVmDisposable("1ebde062-5e42-4d6b-90d8-a13355c0b892", 1, 0, 2)
        self.assertEqual(8192, self.dbrq.getMemoryReserved())
        self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b891")
        self.assertEqual(4096, self.dbrq.getMemoryReserved())
        self.dbrq.setVmDisposableStopped("1ebde062-5e42-4d6b-90d8-a13355c0b892")
        self.assertEqual(0, self.dbrq.getMemoryReserved())

    def test_addUpdateDeleteChallenge(self):
        res=self.dbrq.addNewChallenge('test_4qc',2,'file',1024,2048,0,1,0,1,'path',3600,1, 2)
        self.assertTrue(res)
        res=self.dbrq.getTupleFromTable('*', 'challenges', 'id>4')
        self.assertEqual(res, [(5,'test_4qc',2,'file',1024,2048,0,1,0,1,'path',3600,1,2)])
        res=self.dbrq.updateChallenge(5,'test_6qc',128,'file2',512,1024,1,0,1,0,'path2',7200,0,1)
        self.assertTrue(res)
        res=self.dbrq.getTupleFromTable('*', 'challenges', 'id=5')
        self.assertEqual(res, [(5,'test_6qc',128,'file2',512,1024,1,0,1,0,'path2',7200,0,1)])
        res=self.dbrq.deleteChallenge(5)
        self.assertTrue(res)
        res=self.dbrq.getTupleFromTable('*', 'challenges', 'id=5')
        self.assertEqual(res, [])

    def test_getGuestTypeForChallengeId(self):
        res=self.dbrq.addNewChallenge('test_4qc',2,'file',1024,2048,0,1,0,1,'path',3600,1, 2)
        res=self.dbrq.getGuestTypeForChallengeId(5)
        self.assertEqual(res, 2)

class StatistiquesTest(unittest.TestCase):
    def setUp(self):
        TypeCollect.unittest=0
        self.stats=Statistiques(None)

    def tearDown(self):
        pass

    def test_getAverage(self):
        self.assertEqual(5, self.stats.getAverage([0,5,10]))

    def test_appendToList(self):
        self.stats.appendToList(TypeCollect.unittest, 150)
        hour, value=self.stats.dicStat[(TypeCollect.unittest, TypeDate.hour)][0]
        self.assertTrue(isinstance(hour, str))
        self.assertTrue("h" in hour)
        self.assertEqual(150, value)
        self.stats.appendToList(TypeCollect.unittest, 1035)
        _, value=self.stats.dicStat[(TypeCollect.unittest, TypeDate.hour)][1]
        self.assertEqual(1035, value)


    def test_getFullListOf(self):
        check_resu_list=[]
        for c in range(10):
            check_resu_list.append(c)
            self.stats.count+=1
            self.stats.appendToList(TypeCollect.unittest, c)

        hour,values=self.stats.getFullListOf((TypeCollect.unittest, TypeDate.hour))
        self.assertEqual(len(hour), 10)
        self.assertEqual(len(values), 10)
        self.assertEqual(check_resu_list, values)
        day,values=self.stats.getFullListOf((TypeCollect.unittest, TypeDate.day))
        self.assertEqual(len(day), 1)
        self.assertEqual(len(values), 1)
        self.assertEqual([self.stats.getAverage(check_resu_list)], values)

    def test_getLastOnListOf(self):
        check_resu_list=[]
        last=0
        for c in range(10):
            check_resu_list.append(c)
            last=c
            self.stats.count+=1
            self.stats.appendToList(TypeCollect.unittest, c)

        hour,value=self.stats.getLastOnListOf((TypeCollect.unittest, TypeDate.hour))
        self.assertEqual(len(hour), 5) # len of string (format date)
        self.assertEqual(last, value)
        day,value=self.stats.getLastOnListOf((TypeCollect.unittest, TypeDate.day))
        self.assertEqual(len(day), 5)
        self.assertEqual(self.stats.getAverage(check_resu_list), value)

    def test_getMemoryTotal(self):
        self.assertTrue(isinstance(self.stats.getMemoryTotal(), float))

    def test_getDiskTotal(self):
        self.assertTrue(isinstance(self.stats.getDiskTotal(), float))



if __name__ == '__main__':
    unittest.main()
