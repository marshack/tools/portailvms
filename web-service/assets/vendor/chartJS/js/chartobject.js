function getChartObject(id, label, borderColor, float=false) {
    tmpchart=new Chart(document.getElementById(id), {
    type: 'line',
    data: {
        labels: [],
        datasets: [{ 
            data: [],
            label: label,
            borderColor: borderColor,
            fill: false
        }
        ]
    },
    options: {
        title: {
        display: false,
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                }
            }]
        }
    }
    });

    if (! float)
        tmpchart.options.scales.yAxes[0].ticks.callback=function callback(value) {if (value % 1 === 0) {return value;}};

    return tmpchart;
}


