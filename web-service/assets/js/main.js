function datehm(seconds){
    var date = new Date(null);
    date.setSeconds(seconds);
    hour=date.toISOString().substr(11, 2);
    min=date.toISOString().substr(14, 2);
    var hms = hour+" h "+min+" m";
    return hms;

}

function messageBoxAbstract(alert_class, msg, icon_class='', autoclose=true) {
    content="<div class='alert alert-"+alert_class+" alert-dismissible fade show'>";
    content+="   <div class='container'>";
    if (! autoclose) {
        content+="      <button type='button' class='close' data-dismiss='alert'>&times;</button>";
    }
    content+="      <div class='alert-icon'><i class='fas "+icon_class+"'></i> "+msg+"</div>";
    content+="   </div>";
    content+="</div>";
    $("#notifications").html(content);
    if (autoclose) {
        autoCloseMessageBox(".alert-"+alert_class);
    }
}

function autoCloseMessageBox(id) {
    $(id).delay(5000).slideUp(200, function() {
        $(id).alert('close');
      });
}

function messageBoxInfo(msg) {
    $("#notifications").html(messageBoxAbstract("success", msg, "fa-check-circle"));
}

function messageBoxWarning(msg) {
    $("#notifications").html(messageBoxAbstract("warning", msg, "fa-exclamation-circle", false));
}

function messageBoxError(msg) {
    $("#notifications").html(messageBoxAbstract("danger", msg, "fa-times", false));
}

var title_origin = document.title;
var interval = null;

function blink(title_alternate){
    if (interval==null) {
        interval = setInterval(function() {
            if(document.title == title_alternate){
                document.title = title_origin;
            } else {
                document.title = title_alternate;
            }
        }, 1000);
    }
}

function stop(){
    clearInterval(interval);
    document.title = title_origin;
}

function block() {
    $.blockUI({ css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    } });
}

function unBlock() {
    $.unblockUI();
}
