#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import os, re, grp, pwd, time
import socket, ipaddress
from urllib import parse
import cherrypy
from threading import Lock
try:
    import ldap
except:
    ldap=None

import external.simplepam as simplepam
from common.logs import Logs
from include.corevms import CoreVMS
from include.tokens import Tokens
from include.requestdatabase import RequestDatabase
import include.globals as globals
from include.ping import Ping
from include.widget import Widget

class TypeRequest():
    single_vm = 0    # a single virtual machine (if is allowed)
    mine_vms  = 1    # only my virtual machines
    all_vms   = 2    # all virtuals machines (admin only)

class WebInterfaceAbstract(object):
    msg_error_UUID_dict={}
    lock_msg_error_UUID_dict=Lock()
    def __init__(self, config):
        cherrypy.engine.subscribe('stop', self.stop)
        self.config=config
        self.debug_enabled=config['debug']
        # Logging
        try:
            WebInterfaceAbstract.logging
        except:
            WebInterfaceAbstract.logging=Logs("pvms-web", config['log_web_filename'], config['maxbytes'], config['backupcount'])
            self.logInfo("Service started")
        # database
        try:
            WebInterfaceAbstract.dbrq
        except:
            WebInterfaceAbstract.dbrq=RequestDatabase(config['db_file'], self.debug_enabled, self.logWarning, config['logsql_filename'], config['maxbytes'], config['backupcount'])
        # ping
        try:
            WebInterfaceAbstract.ping
        except:
            WebInterfaceAbstract.ping=Ping(self.get_adresses, self.debug_enabled)
        # tokens
        try:
            WebInterfaceAbstract.tokens
        except:
            WebInterfaceAbstract.tokens=Tokens(config['secret'], debug=self.debug_enabled)
        # Interact with kvm
        try:
            WebInterfaceAbstract.coreVMS
        except:
            WebInterfaceAbstract.coreVMS=CoreVMS(config, \
                callback_warning=self.logWarning, callback_loginfo=self.logInfo, \
                callback_finished=self.slotVmTerminated, \
                callback_initialized=self.setInitialized)
        # stores IP addresses associated with uuids
        try:
            WebInterfaceAbstract.collection_guest_address
        except:
            WebInterfaceAbstract.collection_guest_address={}

    def stop(self):
        self.debug('Please reimplement me ...')

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def logInfo(self, msg):
        WebInterfaceAbstract.logging.logInfo("[{}] {}".format(self.getRemoteIpAddress(), msg))

    def logWarning(self, msg):
        WebInterfaceAbstract.logging.logWarning("[{}] {}".format(self.getRemoteIpAddress(), msg))

    def urlencode(self, query):
        return parse.quote(query)

    def urldecode(self, query):
        return parse.unquote(query)

    def get_adresses(self):
        self.debug('Please reimplement me ...')

    def getAddressByOffset(self, address, offset):
        return str(ipaddress.ip_address(address) + offset)

    def posixAuthenticate(self, username, password):
        if self.checkUserExist(username) and simplepam.authenticate(username, password) and \
                (self.checkUserInGroupMember(username, self.config['admin_grp']) \
                or self.checkUserInGroupMember(username, self.config['user_grp'])):
            self.setAuthenticated(True)
            self.setCurrentUsername(username)
            team_id=self.getPrimaryGroupIdFromUserName(username)
            self.setCurrentTeamId(team_id)
            self.dbrq.updateTeamNameFromId(team_id, username)
            # is admin ?
            self.setIsAdmin(False)
            if self.checkUserInGroupMember(username, self.config['admin_grp']):
                self.setIsAdmin(True)
                self.setCurrentTeamId(0)
                self.dbrq.updateTeamNameFromId(0, self.config['admin_grp'])
            return True
        return False

    def ldapAuthenticate(self, username, password):
        # check username
        if not re.match(r"^[a-zA-Z0-9\.\@\_\-]+$", username):
            self.logWarning(f"Ldap username format error (receive : '{username}')")
            return False
        ldap_url=self.config['ldap_url']
        ldap_base_dn=self.config['ldap_base_dn']
        ldap_username=self.config['ldap_username']
        ldap_credential=self.config['ldap_credential']
        ldap_search=self.config['ldap_search']
        ldap_unique_id_field=self.config['ldap_unique_id_field']
        ldap_ca_certfile=self.config['ldap_ca_certfile']
        if ldap and ldap_url and ldap_base_dn and ldap_username and ldap_credential and ldap_search:
            try:
                if ldap_url.lower().startswith("ldaps://"):
                    ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_DEMAND)
                if ldap_ca_certfile:
                    if os.path.exists(ldap_ca_certfile):
                        ldap.set_option(ldap.OPT_X_TLS_CACERTFILE, ldap_ca_certfile)
                    else:
                        self.logWarning(f"{ldap_ca_certfile} doesn't exist")
                ldap_client = ldap.initialize(ldap_url)
                ldap_client.set_option(ldap.OPT_REFERRALS, 0)
                ldap_client.set_option(ldap.OPT_PROTOCOL_VERSION, 3)
                ldap_client.set_option(ldap.OPT_NETWORK_TIMEOUT, 5.0)
                ldap_search=f"{ldap_search}".format(username=username)
                search_filter = f"({ldap_search})"
                search_scope = ldap.SCOPE_SUBTREE
                ldap_client.simple_bind_s(ldap_username, ldap_credential)
                result_set = []
                ldap_result_id = ldap_client.search(ldap_base_dn, search_scope, search_filter)
                while True:
                    result_type, result_data = ldap_client.result(ldap_result_id, 0)
                    if (result_data == []):
                        break
                    else:
                        if result_type == ldap.RES_SEARCH_ENTRY:
                            result_set.append(result_data)
                for record in result_set:
                    for res in record[0]:
                        if isinstance(res, dict) and ('uidNumber' in res or 'objectSid' in res) and ldap_unique_id_field in res:
                            unique_id=res[ldap_unique_id_field][0].decode()
                            if 'uidNumber' in res:
                                uid=int(res['uidNumber'][0])
                            else:
                                objectSid=res['objectSid'][0]
                                uid=int.from_bytes(objectSid[24:28], byteorder='little')
                            if uid==0: # team_id 0 is reserved for admin
                                raise Exception("UID=0 is not allowed")
                            try:
                                grp.getgrgid(uid)
                                raise Exception("Group id collision (same GID POSIX vs LDAP). A local GID already exists")
                            except:
                                pass
                            # check login/password for this user
                            ldap_client.simple_bind_s(f"{ldap_unique_id_field}={unique_id},{ldap_base_dn}", password)
                            self.debug(f"LDAP authenticate - username='{username}' cn='{unique_id}' uid='{uid}'")
                            self.setAuthenticated(True)
                            self.setCurrentUsername(username)
                            self.setCurrentTeamId(uid) # team ID
                            self.dbrq.updateTeamNameFromId(uid, username)
                            ldap_client.unbind()
                            return True
            except ldap.INVALID_CREDENTIALS:
                self.logWarning("LDAP : Invalid credentials")
            except Exception as e:
                self.debug("LDAP error : {}".format(e))
            try:
                ldap_client.unbind()
            except:
                pass
        time.sleep(1)
        return False

    def checkUserExist(self, user):
        try:
            pwd.getpwnam(user)
            return True
        except KeyError:
            pass
        return False

    def checkUserInGroupMember(self, user, group):
        try:
            groupinfo = grp.getgrnam(group)
            if user in groupinfo.gr_mem:
                return True
        except:
            pass
        return False

    def getPrimaryGroupIdFromUserName(self, user):
        try:
            return pwd.getpwnam(user).pw_gid
        except:
            return 9999 # unknown group

    def setAuthenticated(self, is_auth):
        globals.setAuthenticated(is_auth)

    def isAuthenticated(self):
        return globals.isAuthenticated()

    def setIsAdmin(self, is_admin):
        globals.setIsAdmin(is_admin)

    def isAdmin(self):
        return globals.isAdmin()

    def setCurrentUsername(self, username):
        globals.setCurrentUsername(username)

    def getCurrentUsername(self):
        return globals.getCurrentUsername()

    def setCurrentTeamId(self, team_id):
        globals.setCurrentTeamId(team_id)

    def getCurrentTeamId(self):
        return globals.getCurrentTeamId()

    def checkId(self, id):
        if id==None:
            return False, None
        # check type
        try:
            id=int(id)
            return True, id
        except:
            pass
        return False, None

    def getRemoteIpAddress(self):
        from_address=cherrypy.request.remote.ip
        if 'X-Real-Ip' in cherrypy.request.headers.keys():
            # reverse proxy
            address=cherrypy.request.headers['X-Real-Ip']
            if self.isIPaddressValid(address):
                from_address=address
        return from_address

    def slotVmTerminated(self, uuid, res, msg):
        self.debug(f"WebInterfaceAbstract:slotVmTerminated - res={res}")
        self.logInfo("VM with uuid {} finished".format(uuid))
        if not res:
            self.addErrorMsgForUUID(uuid, msg)
        self.setVmStoppedByUuid(uuid)

    def setVmStoppedByUuid(self, uuid):
        if uuid in self.collection_guest_address.keys():
            self.collection_guest_address.pop(uuid)
        # inform coreVMS
        self.coreVMS.setVmStoppedByUuid(uuid)
        # update database
        return self.dbrq.setVmDisposableStopped(uuid)

    def stopVmByUuid(self, uuid):
        # send quit command to qemu
        res=self.coreVMS.destroyVM(uuid)
        self.setVmStoppedByUuid(uuid)
        return res

    def stopAllVM(self, team_id=0):
        self.debug("WebInterfaceAbstract:stopAllVM")
        for vm in self.dbrq.getListForVmRunning(all_columns=False):
            uuid=vm['uuid']
            # clean stop (send qemu command 'quit')
            self.logInfo("{} (team {}) send quit request (uuid={})".format(self.getCurrentUsername(), team_id, uuid))
            self.stopVmByUuid(uuid)

        # dirty stop and remove tmp files (kill qemu, ...)
        return self.coreVMS.cleanall()

    def setInitialized(self):
        self.debug("WebInterfaceAbstract:setInitialized")
        # launcher has just started, update database
        return self.dbrq.setAllVmDisposableStopped()

    # stop vm, if allowed
    def stopVM(self, id):
        try:
            id=int(id)
        except:
            return False, "id not provided"

        if self.coreVMS.isInMaintenanceMode():
            return False, "Maintenance mode is enable. Try later ..."

        res=self.getDicForVmIDifAllowed(id, all_columns=False)
        if res and len(res)>0:
            api_res=res[0]
            uuid=api_res['uuid']
            this_team_id=self.getCurrentTeamId()
            # send quit command to qemu
            self.logInfo("{} (team {}) send quit request (uuid={})".format(self.getCurrentUsername(), this_team_id, uuid))
            res=self.stopVmByUuid(uuid)
            api_res['alive']=False
            api_res['success']=res
            return True, api_res
        return False, "Impossible to stop VM {}.".format(id)

    # start challenge, if allowed
    def startVM(self, id):
        self.debug("WebInterfaceAbstract:startVM")
        try:
            id=int(id)
        except:
            return False, "id not provided"

        if self.coreVMS.isInMaintenanceMode():
            return False, "Maintenance mode is enable. Try later ..."

        team_id=self.getCurrentTeamId()
        if id!=None and team_id!=None and team_id>=0:
            # check if master running
            if self.dbrq.isVmMasterRunning(id):
                return False, 'Sorry, VM Master {} is started. Please, try later ...'.format(id)

            # check user quota (no limit for group adm)
            res=self.dbrq.getVmRunning(team_id=team_id)
            if len(res)>= self.config['max_vm_per_user'] and team_id!=0:
                self.logWarning("Maximum number of VM reached for user with id {} (max: {})".format(team_id, self.config['max_vm_per_user']))
                return False, 'Sorry, maximum number of VM reached (max: {}/user). Stop your other(s) virtual(s) machine(s)'.format(self.config['max_vm_per_user'])

            clause='id={}'.format(id)
            if not self.isAdmin():
                clause+=" AND enable=1"

            challenge=self.dbrq.getTupleFromTable('id,name,file,ram,vol_size,net_virtio,scsi_virtio,vnc,mount_iso,iso_path,guest_type,cpu', 'challenges', clause)
            if len(challenge)==0:
                self.logWarning("Forbidden to start this id {}".format(id))
                return False, 'Sorry, forbidden to start this ...'
            else:
                challenge=challenge[0]
                chall_id=challenge[0]
                name=challenge[1]
                src_file=challenge[2]
                ram=challenge[3]
                disk_size=challenge[4]
                net_virtio=challenge[5]
                scsi_virtio=challenge[6]
                with_vnc=challenge[7]
                with_iso=challenge[8]
                guest_type=challenge[10]
                cpu=challenge[11]
                uuid=self.coreVMS.generateUUID()
                vnc_password=self.coreVMS.generatePassword()

                vm_id=self.dbrq.getIdAvailable(max=self.config['max_vm'])
                if vm_id==None:
                    self.logWarning("Maximum number of VM reached (max : {})".format(self.config['max_vm']))
                    return False, 'Sorry, maximum number of VM reached (max : {}). Please, try later ...'.format(self.config['max_vm'])

                # check ressources
                mem_reserved=self.dbrq.getMemoryReserved()
                if not self.coreVMS.isRessourcesAvailable(disk_size, ram, mem_reserved):
                    return False, 'Sorry, ressources unavailables. Please, try later ...'

                clean_name=''.join((e if e.isalnum() else '_' for e in name.lower()))
                current_vm_name=re.sub(r'\_+', '_', "{}_team{}_id{}".format(clean_name, team_id, vm_id))
                # update DB
                self.dbrq.addNewVmDisposable(uuid, chall_id, team_id, vm_id, vnc_password)

                from_address=self.getRemoteIpAddress()
                # store IP address that started this VM
                self.collection_guest_address[uuid]=from_address
                to_address=self.getAddressByOffset(self.config['start_address_vm'], vm_id)

                self.logInfo("start VM by {} (team {}) - name={}, chall_id={}, uuid={}".format(self.getCurrentUsername(), team_id, current_vm_name, chall_id, uuid))
                res, msg=self.coreVMS.startDisposableVm(current_vm_name, vm_id, disk_size, ram, guest_type, \
                    src_file, uuid, from_address, to_address, \
                    net_virtio=net_virtio, scsi_virtio=scsi_virtio, with_vnc=with_vnc, vnc_password=vnc_password, cpu=cpu)

                if not res: # an error occured
                    self.logWarning(msg)
                    self.setVmStoppedByUuid(uuid)
                    return False, 'An error occured. Please contact an Administrator : {}'.format(msg)

                return True, { "vm_id":vm_id, "name":name, "uuid":uuid, "to_address":to_address, "vnc_password":vnc_password, "with_iso":with_iso }
        return False

    def checkServiceListening(self, address, port):
        listening=False
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.settimeout(2)
            s.connect((address, port))
            listening=True
            s.close()
        except:
            pass
        return listening

    def getVmInfoFromId(self, id=None):
        res, vm_id_rq=self.checkId(id)
        if not res: return

        # default
        api_res=dict(success=False)

        res=self.getDicForVmIDifAllowed(vm_id_rq)
        if res and len(res)>0:
            api_res=res[0]
            api_res['success']=True
            api_res['alive']=True
            if api_res['vnc']==True:
                api_res['vnc_ready']=False
                if api_res['uptime']>60:
                    api_res['vnc_ready']=True # after 1m, it's normally ready, don't check ...
                elif api_res['uptime']<10:
                    pass # don't check first time
                else:
                    # try to localhost
                    if self.checkServiceListening("127.0.0.1", 5900+vm_id_rq):
                        api_res['vnc_ready']=True
                    else:
                        # try to target address and port 5901 (for archimulator only)
                        if self.checkServiceListening(api_res["address"], 5901):
                            api_res['vnc_ready']=True
        return api_res

    def makeSelectList(self, selected_id, challenges, disable=False):
        res=""
        for _id, name in challenges:
            tplwgt=Widget()
            tplwgt.load('start/challenges_list_items.html')
            if _id==selected_id:
                tplwgt.replace("__SELECTED__", "selected='selected'")
            if disable:
                tplwgt.replace("__DISABLED__", "disabled")
            tplwgt.replace("__CHALLENGE_ID__", str(_id))
            tplwgt.replace("__CHALLENGE_NAME__", name)
            res+=tplwgt.generate()
        return res

    def isIPaddressValid(self, address):
        try:
            socket.inet_aton(address)
            return True
        except:
            pass
        return False

    def getUrlForConsole(self, challenge_id, vm_id, password):
        guest_type=self.dbrq.getGuestTypeForChallengeId(challenge_id)
        if guest_type!=None:
            return self.coreVMS.getUrlForConsole(guest_type, self.config['vnc_public_address'], vm_id, password)
        return ""

    def getDicForVmIDifAllowed(self, vm_id=None, _type=TypeRequest.single_vm, all_columns=True, iso_columns=False):
        api_res=None

        is_admin=self.isAdmin()
        this_team_id=self.getCurrentTeamId()
        if _type==TypeRequest.single_vm and vm_id!=None: # a single virtual machine (if is allowed)
            if this_team_id==0 and is_admin:
                this_team_id=None # I am an admin (no team Id is required)
            api_res=self.dbrq.getListForVmRunning(vm_id=vm_id, team_id=this_team_id, all_columns=all_columns, iso_columns=iso_columns)
        elif _type==TypeRequest.mine_vms: # only my virtual machines
            api_res=self.dbrq.getListForVmRunning(team_id=this_team_id, all_columns=all_columns, iso_columns=iso_columns)
        elif _type==TypeRequest.all_vms and this_team_id==0 and is_admin: # all virtuals machines (admin only)
            api_res=self.dbrq.getListForVmRunning(all_columns=all_columns, iso_columns=iso_columns)
        else:
            return None

        if api_res and len(api_res)>0 and all_columns:
            api_res=self.addMissingItemInVmList(api_res)

        api_res=self.disableVncIfNotOriginIpAddress(api_res)
        return api_res

    def disableVncIfNotOriginIpAddress(self, listVM):
        # check if this user started this virtual env (iptables rules allow only origin ip address)
        current_remote_ip=self.getRemoteIpAddress()
        for vm in listVM:
            origin_remote_ip=current_remote_ip # if not found, allow vnc for this team
            uuid=vm['uuid']
            if uuid in self.collection_guest_address.keys():
                origin_remote_ip=self.collection_guest_address[uuid]
            if origin_remote_ip!=current_remote_ip:
                vm['vnc']=False
                if 'vnc_password' in vm:
                    vm.pop('vnc_password')
        return listVM

    def addMissingItemInVmList(self, listVM):
        # add ping result, addresses IP and compute uptime
        for vm in listVM:
            # ping
            address=self.getAddressByOffset(self.config['start_address_vm'], vm['vm_id'])
            vm['ping']=self.ping.isMachineUp(address)
            # other ...
            vm['address']=address
            # uptime
            tmpstamp=self.dbrq.getCurrentTimestamp()
            vm['uptime']=int(tmpstamp-vm['started'])
            vm.pop('started')
        return listVM

    def purgeErrorMsg(self):
        todo_remove=[]
        self.lock_msg_error_UUID_dict.acquire()
        for k,v in self.msg_error_UUID_dict.items():
            _, t= v
            if time.time()-t >80:
                todo_remove.append(k)
        for uuid in todo_remove:
            try:
                del(self.msg_error_UUID_dict[uuid])
            except:
                pass
        self.lock_msg_error_UUID_dict.release()

    def addErrorMsgForUUID(self, uuid, msg):
        self.lock_msg_error_UUID_dict.acquire()
        self.msg_error_UUID_dict[uuid]=(msg, time.time())
        self.lock_msg_error_UUID_dict.release()
        self.purgeErrorMsg()

    def getErrorMsgFromUUID(self, uuid) -> str:
        msg=""
        self.lock_msg_error_UUID_dict.acquire()
        if uuid in self.msg_error_UUID_dict:
            if not self.isAdmin():
                msg="An error occured, please contact an Administrator"
            else:
                try:
                    msg, _=self.msg_error_UUID_dict[uuid]
                except:
                    pass
        self.lock_msg_error_UUID_dict.release()
        self.purgeErrorMsg()
        return msg
