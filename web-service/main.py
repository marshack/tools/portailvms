#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import os, sys, re
import cherrypy
import configparser, argparse
import base64
import logging
import logging.handlers

from include.template import Template
from include.widget import Widget
from common.schedule import Schedule
from include.decorators import authenticated_only, admin_only
import include.globals as globals
from webinterfaceabstract import WebInterfaceAbstract
from api import Api
from administrate import Administrate

from common.globals import __version__
from common.globals import getConfigValue, createDirIfNotExist

# default values
config_app={
    'debug':False,
    'tmp_session_dir':"/tmp/portailvms/sessions/",
    'tmp_socketvms_dir':"/tmp/portailvms/sock/",
    'port':8080,
    'host':"0.0.0.0",
    'admin_grp':"adm",
    'user_grp':"users",
    'secret':None,
    'origin_dir':"/serve/origin/",
    'tmp_target_dir':"/serve/tmp/",
    'iso_dir':"/serve/ISO/",
    'keyboard':"fr",
    'lxc_dir':'/serve/portailvmsd/lxc/',
    'archi_templates_dir' :'/etc/archimulator/templates/',
    'upload_dir': '/serve/upload/',
    'max_size': 128,
    'start_address_vm':"172.24.8.200",
    'max_vm':40,
    'max_vm_per_user':2,
    'vnc_public_address':"172.24.8.9",
    'db_file':os.path.join(os.path.dirname(__file__), 'database.sqlite3'),
    'title':"PortailVMS",
    # proxy
    'proxy_on':False,
    'base_url':'',
    # logs
    'log_web_filename':None,
    'http_requests_filename':None,
    'http_service_filename':None,
    'logsql_filename':None,
    'maxbytes':150000,
    'backupcount':9,
    # ldap
    'ldap_url':None,
    'ldap_base_dn':None,
    'ldap_search':None,
    'ldap_unique_id_field':'cn',
    'ldap_username':None,
    'ldap_credential':None,
    'ldap_ca_certfile':None
}


def error_page_404(status, message, traceback, version):
        tmpl=Template()
        tmpl.load('404.html')
        return tmpl.generate(config_app['title'], "index")


class Root(WebInterfaceAbstract):
    def __init__(self, config):
        WebInterfaceAbstract.__init__(self, config)
        self.title=self.config['title']
        # ping
        self.ping.start()
        # database
        self.dbrq.start()
        # scheduler to check VM lifetime
        self.scheduleLifeTime=Schedule()
        self.scheduleLifeTime.timer(93, self.checkVMLifeTime)
        self.scheduleLifeTime.start()

    @cherrypy.expose
    def index(self, login="", password="", **values):
        tmpl=Template()

        if 'disconnect' in values:
            if 'login' in cherrypy.session:
                if self.getCurrentUsername()!=None:
                    self.logInfo("disconnected - {}".format(self.getCurrentUsername()))
                    tmpl.info("Vous êtes maintenant déconnecté")
            cherrypy.session.clear()

        if 'authenticated' not in cherrypy.session:
            self.setIsAdmin(False)
            self.setAuthenticated(False)
            self.setCurrentUsername(None)
            self.setCurrentTeamId(None)

        # POST SECTION
        if cherrypy.request.method == 'POST':
            if login!="" and password!="":
                # authentication
                if self.posixAuthenticate(login, password) or self.ldapAuthenticate(login, password):
                    tmpl.info("Welcome {} !".format(login))
                    self.logInfo("auth - {}".format(login))
                    # if redirect
                    if 'next' in values:
                        raise cherrypy.HTTPRedirect(self.urldecode(values['next']))
                else:
                    self.logWarning("bad login/password")
                    tmpl.error("Mauvais identifiant/mot de passe !")

        # GET and POST
        if self.isAuthenticated():
            # authenticated
            tmpl.load('authenticated.html')
            tmpl.replace('__VERSION__', str(__version__))
        else:
            tmpl.load('index.html')

            if 'next' in values:
                tmpl.replace('__NEXT_URL__','?next={}'.format(self.urlencode(values['next'])))

        return tmpl.generate(self.title, "index")

    def generateStartedTemplate(self, vm_id, challenge_id, name, uuid, to_address, url, with_iso):
        tmpl=Template()
        tmpl.load('started.html')
        tmpl.replace("__VM_ID__", vm_id)
        tmpl.replace("__CHALLENGE_NAME__", name)
        tmpl.replace("__UUID__", uuid)
        tmpl.replace("__ADDRESS_IP__", to_address)
        tmpl.replace("__VNC_URL__", url)
        tmpl.replace("__REBOOT_VM__", '/api/system_reset/{}'.format(vm_id))
        tmpl.replace("__DESTROY_VM__", '/api/quit/{}'.format(vm_id))
        tmpl.replace("__VM_ISALIVE__", '/api/isalive/{}'.format(vm_id))
        if with_iso:
            iso_path=self.dbrq.getIsoPathOfChallengeId(challenge_id)
            if iso_path:
                list_iso_files=self.coreVMS.getListIsoFiles(iso_path)
                tplwgt=Widget()
                tplwgt.load('started/iso.html')
                content_select_iso=""
                for iso_file in list_iso_files:
                    tplwgt_iso=Widget()
                    tplwgt_iso.load('started/iso_file_item.html')
                    tplwgt_iso.replace('__ISO_FILENAME__', iso_file)
                    content_select_iso+=tplwgt_iso.generate()
                tplwgt.replace('__ISO_FILES_SELECT__', content_select_iso)
                tmpl.replace("__ISO_FILES__", tplwgt.generate())
        return tmpl.generate(self.title, "currentuser")

    @cherrypy.expose
    @authenticated_only
    def started(self, id=None, **values):
        if id!=None:
            try:
                id=int(id)
            except:
                raise cherrypy.HTTPRedirect('/start')

        res=self.getVmInfoFromId(id)

        if not (isinstance(res, dict) and "success" in res and "alive" in res and res["success"] and res["alive"]):
            raise cherrypy.HTTPRedirect('/start')

        vm_id=res["vm_id"]
        name=res["name"]
        uuid=res["uuid"]
        to_address=res["address"]
        challenge_id=res["challenge"]
        url=""
        if "vnc_password" in res:
            vnc_password=res["vnc_password"]
            url=self.getUrlForConsole(challenge_id, vm_id, vnc_password)
        with_iso=False
        if "mount_iso" in res: with_iso=res["mount_iso"]
        return self.generateStartedTemplate(vm_id, challenge_id, name, uuid, to_address, url, with_iso)

    @cherrypy.expose
    def start(self, id=None, tr=None, **values):
        if id!=None:
            try:
                id=int(id)
            except:
                raise cherrypy.HTTPRedirect('/start')

        if not self.isAuthenticated():
            next="/start/?"
            if id!=None:
                next="/start/{}?".format(id)
            if tr:
                next+="tr={}".format(tr)
            if 'autostart' in values:
                next+="&autostart"
            next=self.urlencode(next)
            if "ta" in values:
                raise cherrypy.HTTPRedirect('/authenticate/{}?next={}'.format(values["ta"], next))
            else:
                raise cherrypy.HTTPRedirect('/?next={}'.format(next))

        if cherrypy.request.method == 'POST' or ('autostart' in values):
            # POST SECTION or autostart is set
            # check token (replay protect)
            if not tr:
                raise cherrypy.HTTPRedirect('/start/{}'.format(id))
            resutoken, _=self.tokens.checkToken(tr, True)
            if not resutoken:
                cherrypy.session['last_warning']='Sorry, token expired. Please retry ...'
                raise cherrypy.HTTPRedirect('/start/{}'.format(id))

            state, res=self.startVM(id)
            if not state:
                cherrypy.session['last_warning']=res
                raise cherrypy.HTTPRedirect('/start/{}'.format(id))
            else:
                vm_id=res["vm_id"]
                name=res["name"]
                uuid=res["uuid"]
                to_address=res["to_address"]
                vnc_password=res["vnc_password"]
                url=self.getUrlForConsole(id, vm_id, vnc_password)
                with_iso=bool(res["with_iso"])
                return self.generateStartedTemplate(vm_id, id, name, uuid, to_address, url, with_iso)

        else:
            # GET SECTION
            tmpl=Template()
            tmpl.load('start.html')
            if 'last_error' in cherrypy.session and cherrypy.session['last_error']:
                tmpl.error(cherrypy.session['last_error'])
                cherrypy.session['last_error']=None
            if 'last_warning' in cherrypy.session and cherrypy.session['last_warning']:
                tmpl.warning(cherrypy.session['last_warning'])
                cherrypy.session['last_warning']=None

            engine_available_list=(str(i) for i in self.coreVMS.getAllAvailableEngineFeatures().keys())
            tmp_lst=", ".join(engine_available_list)
            challenges=self.dbrq.getTupleFromTable('id,name', 'challenges', f'`enable` = 1 AND `guest_type` IN ({tmp_lst})', order='name')
            tmp_html=""
            if self.isAdmin():
                tmp_html+='<optgroup label="Activée">\n'

            tmp_html+=self.makeSelectList(id, challenges)

            if self.isAdmin():
                tmp_html+='</optgroup>\n<optgroup label="Désactivée (non publié)">\n'
                challenges=self.dbrq.getTupleFromTable('id,name', 'challenges', f'`enable` = 0 AND `guest_type` IN ({tmp_lst})', order='name')
                tmp_html+=self.makeSelectList(id, challenges)
                tmp_html+='</optgroup>\n<optgroup label="Désactivée (moteur manquant)">\n'
                challenges=self.dbrq.getTupleFromTable('id,name', 'challenges', f"`guest_type` NOT IN ({tmp_lst})", order='name')
                tmp_html+=self.makeSelectList(id, challenges, disable=True)
                tmp_html+='</optgroup>\n'

            tmpl.replace("__TITLE_MSG__", "Démarrer un environnement virtuel :")
            tmpl.replace("__FORM_ACTION__", "/start")
            tmpl.replace("__LISTCHALLENGES__", tmp_html)

            token_noreplay=self.tokens.getToken(self.tokens.encodeBase64(self.tokens.getRandomNumber(8)),True)
            tmpl.replace("__TOKEN__", token_noreplay)
            tmpl.replace("__HEAD_JS__", "<script src='/assets/vendor/bootstrap-select/js/bootstrap-select.min.js'></script>\n\t\t<script src='/assets/vendor/bootstrap-select/js/i18n/defaults-fr_FR.min.js'></script>")
            tmpl.replace("__HEAD_CSS__", "<!-- Bootstrap Select -->\n\t\t<link href='/assets/vendor/bootstrap-select/css/bootstrap-select.min.css' rel='stylesheet'>")
            return tmpl.generate(self.title, "currentuser")

 
    @cherrypy.expose
    @authenticated_only
    def console(self, ch_id=None, vm_id=None, password=None, **values):
        if ch_id==None or vm_id==None or password==None:
            return ""
        try:
            ch_id=int(ch_id)
            vm_id=int(vm_id)
        except:
            return ""
        url=self.getUrlForConsole(ch_id, vm_id, password)
        raise cherrypy.HTTPRedirect(url)


    @cherrypy.expose
    @authenticated_only
    def vms(self, **values):
        tmpl=Template()
        tmpl.load('vms.html')
        url="https://{}:".format(self.config['vnc_public_address'])
        tmpl.replace("__HEADER_TITLE__", "Vos environnements virtuels :")
        tmpl.replace("__URL_API_RQ__", '/api/alive')
        tmpl.replace("__VNC_URL__", url)
        return tmpl.generate(self.title, "currentuser")


    ###### Authenticate with token
    #
    # Example
    # /authenticate/rP1NzItlkws4lfIo9ImfdRRsgiW2ENYbL8YxMjA1LzE1NzY5Mzg4MDkuNDI0MzQ3
    #
    # Generate an auth token (example):
    #     team_id = 1
    #     team_name="myTeam"
    #     data="{};{}".format(team_id,base64.b64encode(team_name.encode()).decode())
    #     tokens=Tokens(secret)
    #     token_auth=tokens.getToken(data, True, True)
    @cherrypy.expose
    def authenticate(self, ta=None, **values):
        if not ta:
            return
        # check token
        res, team=self.tokens.checkToken(ta, timebased=True, encrypted=True)
        if not res:
            self.logWarning("Token authentication error")
            raise cherrypy.HTTPRedirect('/')
        else:
            try:
                team_id, team_name=team.split(";", 1)
                team_name=base64.b64decode(team_name.encode()).decode()
                team_name=re.sub(r'(\'|"|\?|`|\\|\n|\r|\s|\t|\;|\(|\))', '_', team_name)
                team_id=int(team_id)
            except:
                self.logWarning("Token authentication error (bad format)")
                raise cherrypy.HTTPRedirect('/')
            if team_id==0: # team_id 0 is reserved for admin
                self.logWarning("Token authentication error (using team_id=0 is forbidden)")
                raise cherrypy.HTTPRedirect('/')
            self.setIsAdmin(False)
            self.setAuthenticated(True)
            self.setCurrentUsername(team_name)
            self.setCurrentTeamId(team_id)
            self.dbrq.updateTeamNameFromId(team_id, team_name)
            self.logInfo("team {} ({}) authenticated with token".format(team_id,  team_name))

        if 'next' in values:
            raise cherrypy.HTTPRedirect(self.urldecode(values['next']))
        raise cherrypy.HTTPRedirect('/')

    def checkVMLifeTime(self):
        self.debug("Root:checkVMLifeTime")
        current_timestamp=self.dbrq.getCurrentTimestamp()
        for uuid, started, lifetime in self.dbrq.getListForVmRunningWithLifetime():
            if current_timestamp > started + lifetime:
                self.logInfo("end of life of the vm with uuid={}".format(uuid))
                self.stopVmByUuid(uuid)


    def stop(self):
        self.logInfo('Stopping service')
        self.debug('Stopping service')
        self.ping.stop()
        self.scheduleLifeTime.stop()
        self.coreVMS.stop()
        self.dbrq.stop()
        self.ping.join()
        self.scheduleLifeTime.join()
        self.dbrq.join()
        self.debug('Service stopped')

    def get_adresses(self):
        running=self.dbrq.getVmIdRunning()
        addresses=[]
        for vm_id in running:
            address=self.getAddressByOffset(self.config['start_address_vm'], vm_id)
            addresses.append(address)
        return addresses

if __name__ == '__main__':
    filename='/etc/portailvms/main.conf'
    if not os.path.isfile(filename):
        sys.stderr.write("Error - Configuration file doesn't exist : {}\n".format(filename))
        sys.exit(1)

    # read configuration file
    config = configparser.ConfigParser()
    try:
        config.read(filename)
        # DEFAULT section
        config_app['port']=getConfigValue(config,"DEFAULT", "port", config_app['port'], int)
        config_app['host']=getConfigValue(config,"DEFAULT", "host", config_app['host'], str)
        config_app['db_file']=getConfigValue(config,"DEFAULT", "db_file", config_app['db_file'], str)
        config_app['admin_grp']=getConfigValue(config,"DEFAULT", "admin_grp", config_app['admin_grp'], str)
        config_app['user_grp']=getConfigValue(config,"DEFAULT", "user_grp", config_app['user_grp'], str)
        secret=getConfigValue(config,"DEFAULT", "secret", config_app['secret'], str)
        config_app['secret']=base64.b64decode(secret)
        # LDAP section
        config_app['ldap_url']=getConfigValue(config,"LDAP", "ldap_url", config_app['ldap_url'], str)
        config_app['ldap_base_dn']=getConfigValue(config,"LDAP", "ldap_base_dn", config_app['ldap_base_dn'], str)
        config_app['ldap_search']=getConfigValue(config,"LDAP", "ldap_search", config_app['ldap_search'], str)
        config_app['ldap_unique_id_field']=getConfigValue(config,"LDAP", "ldap_unique_id_field", config_app['ldap_unique_id_field'], str)
        config_app['ldap_username']=getConfigValue(config,"LDAP", "ldap_username", config_app['ldap_username'], str)
        config_app['ldap_credential']=getConfigValue(config,"LDAP", "ldap_credential", config_app['ldap_credential'], str)
        config_app['ldap_ca_certfile']=getConfigValue(config,"LDAP", "ldap_ca_certfile", config_app['ldap_ca_certfile'], str)
        # PROXY section
        config_app['proxy_on']=getConfigValue(config,"PROXY", "proxy_on", config_app['proxy_on'], bool)
        config_app['base_url']=getConfigValue(config,"PROXY", "base_url", config_app['base_url'], str)
        # VM section
        config_app['origin_dir']=getConfigValue(config,"VM", "origin_dir", config_app['origin_dir'], str)
        config_app['tmp_target_dir']=getConfigValue(config,"VM", "tmp_target_dir", config_app['tmp_target_dir'], str)
        config_app['iso_dir']=getConfigValue(config,"VM", "iso_dir", config_app['iso_dir'], str)
        config_app['start_address_vm']=getConfigValue(config,"VM", "start_address_vm", config_app['start_address_vm'], str)
        config_app['max_vm']=getConfigValue(config,"VM", "max_vm", config_app['max_vm'], int)
        config_app['max_vm_per_user']=getConfigValue(config,"VM", "max_vm_per_user", config_app['max_vm_per_user'], int)
        config_app['keyboard']=getConfigValue(config,"VM", "keyboard", config_app['keyboard'], str)
        # LXC section
        config_app['lxc_dir']=getConfigValue(config,"LXC", "lxc_dir", config_app['lxc_dir'], str)
        # Archi section
        config_app['archi_templates_dir']=getConfigValue(config,"ARCHI", "templates_dir", config_app['archi_templates_dir'], str)
        # VNC section
        config_app['vnc_public_address']=getConfigValue(config,"VNC", "public_address", config_app['vnc_public_address'], str)
        # UPLOAD section
        config_app['upload_dir']=getConfigValue(config,"UPLOAD", "upload_dir", config_app['upload_dir'], str)
        config_app['max_size']=getConfigValue(config,"UPLOAD", "max_size", config_app['max_size'], int)
        # LOGGING section
        config_app['log_web_filename']=getConfigValue(config,"LOGGING", "log_web_filename",config_app['log_web_filename'], str)
        config_app['http_requests_filename']=getConfigValue(config,"LOGGING", "http_requests_filename",config_app['http_requests_filename'], str)
        config_app['http_service_filename']=getConfigValue(config,"LOGGING", "http_service_filename",config_app['http_service_filename'], str)
        config_app['logsql_filename']=getConfigValue(config,"LOGGING", "logsql_filename",config_app['logsql_filename'], str)
        config_app['maxbytes']=getConfigValue(config,"LOGGING", "maxbytes",config_app['maxbytes'], int)
        config_app['backupcount']=getConfigValue(config,"LOGGING", "backupcount",config_app['backupcount'], int)
    except:
        sys.stderr.write("Error - Impossible to parse configuration file : {}\n".format(filename))
        sys.exit(1)
    createDirIfNotExist(config_app['tmp_session_dir'])
    createDirIfNotExist(config_app['tmp_socketvms_dir'])

    # Parse Args
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--version', action='version', \
        help="show version", 
        version='%(prog)s version : {version}'.format(version=__version__))
    parser.add_argument("-d", "--database", metavar="FILENAME",dest="database",
            help="Database filename")
    parser.add_argument("--debug", help="Debug", action="store_true")
    args=parser.parse_args()
    config_app['debug']=args.debug
    if args.database:
        config_app['db_file']=args.database

    config_app['current_dir']=os.path.abspath(os.getcwd())
    cherrypy_conf = {
        'global': {
            'server.socket_port': config_app['port'],
            'server.socket_host': config_app['host'],
            'server.max_request_body_size' : config_app['max_size']*1024*1000,
            'error_page.404': error_page_404,
            'engine.autoreload.on' : False,
            'response.headers.server': "Marshack Dev",
            'request.show_tracebacks': config_app['debug']
        },
        '/': {
            'tools.sessions.on': True,
            'tools.sessions.storage_type': "file",
            'tools.sessions.storage_path': config_app['tmp_session_dir'],
            'tools.staticdir.root' : config_app['current_dir'],
            'tools.sessions.timeout': 720
        },
        '/assets':{
            'tools.sessions.on': False,
            'tools.staticdir.on' : True,
            'tools.staticdir.dir' : 'assets'
        }
    }

    if config_app['proxy_on']:
        cherrypy_conf['global']['tools.proxy.on']=True
        cherrypy_conf['global']['tools.proxy.base']=config_app['base_url']

    if config_app['http_requests_filename']:
        logscope = cherrypy.log
        # Make a new RotatingFileHandler for the access log.
        h = logging.handlers.RotatingFileHandler(config_app['http_requests_filename'], \
                maxBytes=config_app['maxbytes'], backupCount=config_app['backupcount'])
        h.setLevel(logging.DEBUG)
        logscope.access_file = ""
        logscope.access_log.addHandler(h)

    if config_app['http_service_filename']:
        logscope = cherrypy.log
        # Make a new RotatingFileHandler for the error log.
        h = logging.handlers.RotatingFileHandler(config_app['http_service_filename'], \
                maxBytes=config_app['maxbytes'], backupCount=config_app['backupcount'])
        h.setLevel(logging.DEBUG)
        logscope.error_file = ""
        logscope.error_log.addHandler(h)

    if not config_app['debug']:
        cherrypy_conf['global']['environment']='production'

    cherrypy.log.screen = False

    cherrypy.root=Root(config_app)
    cherrypy.root.api = Api(config_app)
    cherrypy.root.admin = Administrate(config_app)
    cherrypy.quickstart(cherrypy.root, '/', cherrypy_conf)


