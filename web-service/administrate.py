#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)


import cherrypy, re

from include.template import Template
from include.widget import Widget
from include.decorators import authenticated_only, admin_only
from webinterfaceabstract import WebInterfaceAbstract

class Administrate(WebInterfaceAbstract):
    def __init__(self, config):
        WebInterfaceAbstract.__init__(self, config)
        self.currentMenuItem="admin"
        self.title=self.config['title']

    def stop(self):
        pass

    @cherrypy.expose
    @authenticated_only
    @admin_only
    def index(self, **values):
        return ''

    @cherrypy.expose
    @authenticated_only
    @admin_only
    def allvms(self, **values):
        tmpl=Template()
        tmpl.load('vms.html')
        url="https://{}:".format(self.config['vnc_public_address'])
        tmpl.replace("__HEADER_TITLE__", "Environnements virtuels :")
        tmpl.replace("__URL_API_RQ__", '/api/aliveall')
        tmpl.replace("__VNC_URL__", url)

        return tmpl.generate(self.title, self.currentMenuItem)

    @cherrypy.expose
    @authenticated_only
    @admin_only
    def monitoring(self, **values):
        tmpl=Template()
        tmpl.load('monitoring.html')
        tmpl.replace("__HEAD_JS__", "<script src='/assets/vendor/chartJS/js/Chart.min.js'></script>\n\t\t<script src='/assets/vendor/chartJS/js/chartobject.js'></script>")
        return tmpl.generate(self.title, self.currentMenuItem)
 
    @cherrypy.expose
    @authenticated_only
    @admin_only
    def startmaster(self, id=None, tr=None, **values):
        if id!=None:
            try:
                id=int(id)
            except:
                raise cherrypy.HTTPRedirect('/admin/startmaster')

        if cherrypy.request.method == 'POST':
            # POST SECTION
            # check token (replay protect)
            if not tr:
                raise cherrypy.HTTPRedirect('/admin/startmaster/{}'.format(id))
            resutoken, _=self.tokens.checkToken(tr, True)
            if not resutoken:
                cherrypy.session['last_warning']='Sorry, token expired. Please retry ...'
                raise cherrypy.HTTPRedirect('/admin/startmaster/{}'.format(id))

            team_id=self.getCurrentTeamId()
            if id!=None and team_id==0:
                # check if is already started
                if self.dbrq.isVmMasterRunning(id):
                    cherrypy.session['last_warning']='Sorry, VM Master {} is already started'.format(id)
                    raise cherrypy.HTTPRedirect('/admin/startmaster/{}'.format(id))

                challenge=self.dbrq.getTupleFromTable('id,name,file,ram,vol_size,net_virtio,scsi_virtio,vnc,mount_iso,iso_path,guest_type,cpu', 'challenges', 'id={}'.format(id))
                if len(challenge)>0:
                    challenge=challenge[0]
                    chall_id=challenge[0]
                    name=challenge[1]
                    src_file=challenge[2]
                    ram=challenge[3]
                    disk_size=challenge[4]
                    net_virtio=challenge[5]
                    scsi_virtio=challenge[6]
                    guest_type=challenge[10]
                    cpu=challenge[11]
                    uuid=self.coreVMS.generateUUID()
                    vnc_password=self.coreVMS.generatePassword()
                    
                    vm_id=self.dbrq.getIdAvailable(max=self.config['max_vm'])
                    if vm_id==None:
                        self.logWarning("Maximum number of VM reached (max : {})".format(self.config['max_vm']))
                        cherrypy.session['last_error']='Sorry, maximum number of VM reached (max : {}). Please, try later ...'.format(self.config['max_vm'])
                        raise cherrypy.HTTPRedirect('/admin/startmaster/{}'.format(id))

                    # check ressources
                    mem_reserved=self.dbrq.getMemoryReserved()
                    if not self.coreVMS.isRessourcesAvailable(disk_size, ram, mem_reserved):
                        cherrypy.session['last_error']='Sorry, ressources unavailables. Please, try later ...'
                        raise cherrypy.HTTPRedirect('/admin/startmaster/{}'.format(id))

                    current_vm_name=re.sub(r'\_+', '_', ''.join((e if e.isalnum() else '_' for e in name.lower())))
                    # update DB
                    self.dbrq.addNewVmDisposable(uuid, chall_id, team_id, vm_id, vnc_password, master=1)

                    from_address=self.getRemoteIpAddress()
                    # store IP address that started this VM
                    self.collection_guest_address[uuid]=from_address
                    to_address=self.getAddressByOffset(self.config['start_address_vm'], vm_id)

                    self.logInfo("start VM MASTER by {} (team {}) - name={}, chall_id={}, uuid={}".format(self.getCurrentUsername(), team_id, current_vm_name, chall_id, uuid))
                    res, msg=self.coreVMS.startMasterVm(current_vm_name, vm_id, disk_size, ram, guest_type, \
                        src_file, uuid, \
                        from_address, to_address, net_virtio=net_virtio, scsi_virtio=scsi_virtio, \
                        vnc_password=vnc_password, cpu=cpu)

                    if not res: # an error occured
                        self.logWarning(msg)
                        self.setVmStoppedByUuid(uuid)
                        cherrypy.session['last_error']='An error occured. Please contact an Administrator : {}'.format(msg)
                        raise cherrypy.HTTPRedirect('/admin/startmaster/{}'.format(id))

                    tmpl=Template()
                    tmpl.load('started.html')
                    tmpl.replace("__VM_ID__", vm_id)
                    tmpl.replace("__CHALLENGE_NAME__", name)
                    tmpl.replace("__UUID__", uuid)
                    tmpl.replace("__ADDRESS_IP__", to_address)
                    url=self.getUrlForConsole(id, vm_id, vnc_password)
                    tmpl.replace("__VNC_URL__", url)
                    tmpl.replace("__REBOOT_VM__", '/api/system_reset/{}'.format(vm_id))
                    tmpl.replace("__DESTROY_VM__", '/api/quit/{}'.format(vm_id))
                    tmpl.replace("__VM_ISALIVE__", '/api/isalive/{}'.format(vm_id))

                    if self.coreVMS.isIsoAvailable(uuid):
                        list_iso_files=self.coreVMS.getListIsoFiles(self.config['iso_dir'])
                        tplwgt=Widget()
                        tplwgt.load('started/iso.html')
                        content_select_iso=""
                        for iso_file in list_iso_files:
                            tplwgt_iso=Widget()
                            tplwgt_iso.load('started/iso_file_item.html')
                            tplwgt_iso.replace('__ISO_FILENAME__', iso_file)
                            content_select_iso+=tplwgt_iso.generate()
                        tplwgt.replace('__ISO_FILES_SELECT__', content_select_iso)
                        tmpl.replace("__ISO_FILES__", tplwgt.generate())
                    return tmpl.generate(self.title, self.currentMenuItem)
        else:
            # GET SECTION
            tmpl=Template()
            tmpl.load('start.html')
            if 'last_error' in cherrypy.session and cherrypy.session['last_error']:
                tmpl.error(cherrypy.session['last_error'])
                cherrypy.session['last_error']=None
            if 'last_warning' in cherrypy.session and cherrypy.session['last_warning']:
                tmpl.warning(cherrypy.session['last_warning'])
                cherrypy.session['last_warning']=None

            engine_available_list=(str(i) for i in self.coreVMS.getAllAvailableEngineFeatures().keys())
            tmp_lst=", ".join(engine_available_list)

            # guest_type < 32 or guest_type=64: master for KVM and LXC only
            challenges=self.dbrq.getTupleFromTable('id,name', 'challenges', f'(`guest_type`<32 OR `guest_type`=64) AND `guest_type` IN ({tmp_lst})', order='name')
            tmp_html=self.makeSelectList(id, challenges)
            tmp_html+='</optgroup>\n<optgroup label="Désactivée (moteur manquant)">\n'
            challenges=self.dbrq.getTupleFromTable('id,name', 'challenges', f"(`guest_type`<32 OR `guest_type`=64) AND `guest_type` NOT IN ({tmp_lst})", order='name')
            tmp_html+=self.makeSelectList(id, challenges, disable=True)
            tmp_html+='</optgroup>\n'

            tmpl.replace("__TITLE_MSG__", "Démarrer la machine virtuelle Maître :")
            tmpl.replace("__FORM_ACTION__", "/admin/startmaster")
            tmpl.replace("__LISTCHALLENGES__", tmp_html)

            token_noreplay=self.tokens.getToken(self.tokens.encodeBase64(self.tokens.getRandomNumber(8)),True)
            tmpl.replace("__TOKEN__", token_noreplay)
            tmpl.replace("__HEAD_JS__", "<script src='/assets/vendor/bootstrap-select/js/bootstrap-select.min.js'></script>\n\t\t<script src='/assets/vendor/bootstrap-select/js/i18n/defaults-fr_FR.min.js'></script>")
            tmpl.replace("__HEAD_CSS__", "<!-- Bootstrap Select -->\n\t\t<link href='/assets/vendor/bootstrap-select/css/bootstrap-select.min.css' rel='stylesheet'>")
            return tmpl.generate(self.title, self.currentMenuItem)

    @cherrypy.expose
    @authenticated_only
    @admin_only
    def tools(self, **values):
        tmpl=Template()
        tmpl.load('tools.html')
        return tmpl.generate(self.title, self.currentMenuItem)

    def checkYaml(self, filename, key_required):
        self.debug(f"Administrate:checkYaml {filename} {key_required}")
        res=None
        try:
            with open(filename) as f:
                # ignore error if is a template
                data=re.sub(r'\{\{.+\}\}', '__TEMPLATE_ITEM__', f.read())
                res=yaml.safe_load(data)
        except OSError as e:
            return False, "{} (filename : {})".format(e.strerror, filename)
        except Exception as e:
            return False, "Impossible to parse yaml file {} : {}".format(filename, str(e))
        if res and key_required in res:
            return True, ""
        return False, "key '{}' missing in {}".format(key_required, filename)

    @cherrypy.expose
    @authenticated_only
    @admin_only
    def upload(self, **values):
        tmpl=Template()
        tmpl.load('upload.html')
        if 'last_error' in cherrypy.session and cherrypy.session['last_error']:
            tmpl.error(cherrypy.session['last_error'])
            cherrypy.session['last_error']=None
        if 'last_warning' in cherrypy.session and cherrypy.session['last_warning']:
            tmpl.warning(cherrypy.session['last_warning'])
            cherrypy.session['last_warning']=None
        token_noreplay=self.tokens.getToken(self.tokens.encodeBase64(self.tokens.getRandomNumber(8)),True)
        tmpl.replace("__TOKEN__", token_noreplay)
        max_size_upload=int(cherrypy.config['server.max_request_body_size']/(1024*1000))
        tmpl.replace("__MAXSIZE__", max_size_upload)
        return tmpl.generate(self.title, self.currentMenuItem)

    @cherrypy.expose
    @authenticated_only
    @admin_only
    def createqcow2(self, **values):
        tmpl=Template()
        tmpl.load('createqcow2.html')
        return tmpl.generate(self.title, self.currentMenuItem)

    def makeEditForm(self, id=None, name="", guest_type="0", _file="", ram="512", \
            vol_size="1500", net_virtio="0", scsi_virtio="0", vnc="0", \
            mount_iso="0", iso_path="", lifetime="0", enable="0", cpu="1"):
        tmpl=Template()
        tmpl.load('edit.html')
        if 'last_error' in cherrypy.session and cherrypy.session['last_error']:
            tmpl.error(cherrypy.session['last_error'])
            cherrypy.session['last_error']=None
        if 'last_warning' in cherrypy.session and cherrypy.session['last_warning']:
            tmpl.warning(cherrypy.session['last_warning'])
            cherrypy.session['last_warning']=None
        tmpl.replace('__NAME__', name)
        tmpl.replace('__GUEST_TYPE__', guest_type)
        tmpl.replace('__FILE__', _file)
        tmpl.replace('__RAM__', ram)
        tmpl.replace('__VOL_SIZE__', vol_size)
        tmpl.replace('__CPU__', cpu)
        tmpl.replace('__NET_VIRTIO__', net_virtio)
        tmpl.replace('__SCSI_VIRTIO__', scsi_virtio)
        tmpl.replace('__VNC__', vnc)
        tmpl.replace('__MOUNT_ISO__', mount_iso)
        tmpl.replace('__ISO_PATH__', iso_path)
        tmpl.replace('__LIFETIME__', int(int(lifetime)/60)) # convert to minutes
        tmpl.replace('__ENABLE__', enable)
        token_noreplay=self.tokens.getToken(self.tokens.encodeBase64(self.tokens.getRandomNumber(8)))
        tmpl.replace("__TOKEN__", token_noreplay)
        content_list_engine=""
        for value, name in self.coreVMS.getAllAvailableEngineFeatures().items():
            tplwgt=Widget()
            tplwgt.load('edit/engine_item.html')
            tplwgt.replace('__ENGINE_VALUE__', value)
            tplwgt.replace('__ENGINE_NAME__', name)
            content_list_engine+=tplwgt.generate()
        tmpl.replace("__LIST_ENGINES__", content_list_engine)
        return tmpl.generate(self.title, self.currentMenuItem)

    def sanitize(self, _input):
        return re.sub(r'(\'|"|\?|`|\\|\n|\r)', '', _input)

    @cherrypy.expose
    @authenticated_only
    @admin_only
    def challenges(self, id=None, tr=None, delete=None, **values):
        if id!=None:
            try:
                id=int(id)
            except:
                raise cherrypy.HTTPRedirect('/admin/challenges')

        if delete != None and id!=None:
            # Delete record
            # check token (replay protect)
            if not tr:
                raise cherrypy.HTTPRedirect('/admin/challenges/{}'.format(id))
            resutoken, _=self.tokens.checkToken(tr)
            if not resutoken:
                cherrypy.session['last_warning']='Sorry, token expired. Please retry ...'
                raise cherrypy.HTTPRedirect('/admin/challenges/{}'.format(id))

            res=self.dbrq.deleteChallenge(id)
            if res:
                cherrypy.session['last_info']="Challenge removed"
                raise cherrypy.HTTPRedirect('/admin/challenges')
            else:
                cherrypy.session['last_error']='Sorry, impossible to remove this'
                raise cherrypy.HTTPRedirect('/admin/challenges/{}'.format(id))

        else:
            tmpl=Template()
            tmpl.load('challenges.html')
            if 'last_error' in cherrypy.session and cherrypy.session['last_error']:
                tmpl.error(cherrypy.session['last_error'])
                cherrypy.session['last_error']=None
            if 'last_warning' in cherrypy.session and cherrypy.session['last_warning']:
                tmpl.warning(cherrypy.session['last_warning'])
                cherrypy.session['last_warning']=None
            if 'last_info' in cherrypy.session and cherrypy.session['last_info']:
                tmpl.info(cherrypy.session['last_info'])
                cherrypy.session['last_info']=None

            challenges=self.dbrq.getTupleFromTable('id,name', 'challenges', order='name')
            tmp_html=self.makeSelectList(id, challenges)
            tmpl.replace("__LISTCHALLENGES__", tmp_html)

            token_noreplay=self.tokens.getToken(self.tokens.encodeBase64(self.tokens.getRandomNumber(8)),True)
            tmpl.replace("__TOKEN__", token_noreplay)
            tmpl.replace("__HEAD_JS__", "<script src='/assets/vendor/bootstrap-select/js/bootstrap-select.min.js'></script>\n\t\t<script src='/assets/vendor/bootstrap-select/js/i18n/defaults-fr_FR.min.js'></script>")
            tmpl.replace("__HEAD_CSS__", "<!-- Bootstrap Select -->\n\t\t<link href='/assets/vendor/bootstrap-select/css/bootstrap-select.min.css' rel='stylesheet'>")
            return tmpl.generate(self.title, self.currentMenuItem)

    @cherrypy.expose
    @authenticated_only
    @admin_only
    def edit(self, id=None, tr=None, **values):
        if id!=None:
            try:
                id=int(id)
            except:
                raise cherrypy.HTTPRedirect('/admin/challenges')

        if cherrypy.request.method == 'POST':
            # POST SECTION
            # check token (replay protect)
            if not tr:
                raise cherrypy.HTTPRedirect('/admin/challenges/{}'.format(id))

            resutoken, _=self.tokens.checkToken(tr)
            try:
                name=values['name']
                guest_type=int(values['guest_type'])
                _file=values['file']
                ram=int(values['ram'])
                cpu=int(values['cpu'])
                vol_size=int(values['vol_size'])
                lifetime=int(values['lifetime'])*60 # convert to seconds
                net_virtio=int(values['net_virtio'])
                scsi_virtio=int(values['scsi_virtio'])
                vnc=int(values['vnc'])
                mount_iso=int(values['mount_iso'])
                if 'iso_path' in values:
                    iso_path=values['iso_path']
                else:
                    iso_path=""
                enable=int(values['enable'])
            except Exception as e:
                cherrypy.session['last_error']=e
                try:
                    return self.makeEditForm(id, name, guest_type, _file, ram, \
                        vol_size, net_virtio, scsi_virtio, vnc, \
                        mount_iso, iso_path, lifetime, enable, cpu)
                except:
                    # bad form (forged ?)
                    cherrypy.session['last_error']="Incorrect entries, check your form"
                    if id==None:
                        raise cherrypy.HTTPRedirect('/admin/edit')
                    elif isinstance(id, int):
                        raise cherrypy.HTTPRedirect('/admin/edit/{}'.format(id))

            if not resutoken:
                cherrypy.session['last_warning']='Sorry, token expired. Please retry ...'
                return self.makeEditForm(id, name, guest_type, _file, ram, \
                    vol_size, net_virtio, scsi_virtio, vnc, \
                    mount_iso, iso_path, lifetime, enable, cpu)

            if isinstance(id, int):
                # UPDATE Record
                res=self.dbrq.updateChallenge(id, self.sanitize(name), guest_type, self.sanitize(_file), ram, \
                    vol_size, net_virtio, scsi_virtio, vnc, \
                    mount_iso, self.sanitize(iso_path), lifetime, enable, cpu)
                if res:
                    cherrypy.session['last_info']='Challenge saved'
                    raise cherrypy.HTTPRedirect('/admin/challenges/{}'.format(id))
                else:
                    cherrypy.session['last_error']='Sorry, impossible to save change'
                    return self.makeEditForm(id, name, guest_type, _file, ram, \
                        vol_size, net_virtio, scsi_virtio, vnc, \
                        mount_iso, iso_path, lifetime, enable, cpu)
            elif id==None:
                # APPEND Record
                res=self.dbrq.addNewChallenge(self.sanitize(name), guest_type, self.sanitize(_file), ram, \
                    vol_size, net_virtio, scsi_virtio, vnc, \
                    mount_iso, self.sanitize(iso_path), lifetime, enable, cpu)
                if res:
                    cherrypy.session['last_info']="Challenge '{}' created".format(name)
                    raise cherrypy.HTTPRedirect('/admin/challenges/{}'.format(id))
                else:
                    cherrypy.session['last_error']='Sorry, impossible to add this'
                    return self.makeEditForm(id, name, guest_type, _file, ram, \
                        vol_size, net_virtio, scsi_virtio, vnc, \
                        mount_iso, iso_path, lifetime, enable, cpu)
        else:
            # GET SECTION
            if id==None:
                # new record
                return self.makeEditForm()
            else:
                if not isinstance(id, int): raise cherrypy.HTTPRedirect('/admin/challenges')

                # search a record
                challenge=self.dbrq.getTupleFromTable('name,guest_type,file,ram,vol_size,net_virtio,scsi_virtio,vnc,mount_iso,iso_path,lifetime,enable,cpu', 'challenges', 'id={}'.format(id))
                if len(challenge)>0:
                    challenge=challenge[0]
                    return self.makeEditForm(id, challenge[0],
                        challenge[1], challenge[2], challenge[3],
                        challenge[4], challenge[5], challenge[6],
                        challenge[7], challenge[8], challenge[9],
                        challenge[10], challenge[11], challenge[12])
                else:
                    # not found
                    cherrypy.session['last_error']='Sorry, record not found'
                    raise cherrypy.HTTPRedirect('/admin/challenges/{}'.format(id))
