clean:
	find . -name \*.pyc -type f -delete
	find . -name __pycache__ -type d -delete

build_python:
	find ./common ./launcher-service ./web-service -type f -name "*.py" -exec dirname {} \; 2>/dev/null | sort -u | xargs -r -I {} sh -c "python3 -m py_compile {}/*.py"

install: install_configuration install_program install_services install_log

uninstall:
	if [ -f /etc/systemd/system/portailvmsd-web.service ]; then \
		systemctl stop portailvmsd-web.service; \
		systemctl disable portailvmsd-web.service; \
		rm /etc/systemd/system/portailvmsd-web.service; \
	fi
	if [ -f /etc/systemd/system/portailvmsd-launcher.service ]; then \
		systemctl stop portailvmsd-launcher.service; \
		systemctl disable portailvmsd-launcher.service; \
		rm /etc/systemd/system/portailvmsd-launcher.service; \
	fi
	test -d /opt/portailvms && rm -rf /opt/portailvms || /bin/true
	test -d /tmp/portailvms && rm -rf /tmp/portailvms || /bin/true

install_program: build_python
	mkdir -p /opt/portailvms
	cp -Pr * /opt/portailvms/
	chown root:portailvmsd /opt/portailvms -R
	chmod u=rwx,g=rx,o-rwx /opt/portailvms/launcher-service/scripts/*.sh

install_configuration:
	mkdir -p /etc/portailvms/db/
	test -f /etc/portailvms/main.conf && echo "[Warning] main.conf file is already exist -> skip" || cp main.conf /etc/portailvms/main.conf
	test -f /etc/portailvms/db/database.sqlite3 && echo "[Warning] database.sqlite3 file is already exist -> skip" || cp web-service/database.sqlite3 /etc/portailvms/db/
	chown root:portailvmsd /etc/portailvms -R
	chmod u=rwx,g=rx,o-rwx /etc/portailvms
	chmod u=rw,g=r,o-rwx /etc/portailvms/*.conf
	chown portailvmsd:portailvmsd /etc/portailvms/db -R
	chmod u=rw,g=r,o-rwx /etc/portailvms/db/* -R
	chmod o-rwx /etc/portailvms -R

install_log:
	mkdir -p /var/log/portailvmsd/
	chmod u=rwx,g=rx,o= /var/log/portailvmsd/
	chown portailvmsd:portailvmsd /var/log/portailvmsd/

install_services:
	cp web-service/portailvmsd-web.service /etc/systemd/system/
	cp launcher-service/portailvmsd-launcher.service /etc/systemd/system/
	systemctl daemon-reload
	systemctl enable portailvmsd-launcher.service
	systemctl start portailvmsd-launcher.service
	systemctl enable portailvmsd-web.service
	systemctl start portailvmsd-web.service

enable_apparmor:
	test -d /etc/apparmor.d/ && cp apparmor/opt.portailvms.web-service.main.py /etc/apparmor.d/
	test -d /etc/apparmor.d/ && cp apparmor/opt.portailvms.launcher-service.main.py /etc/apparmor.d/
	systemctl reload apparmor

disable_apparmor:
	if [ -f /etc/apparmor.d/opt.portailvms.web-service.main.py ]; then \
		rm -f /etc/apparmor.d/opt.portailvms.web-service.main.py; \
	fi
	if [ -f /etc/apparmor.d/opt.portailvms.launcher-service.main.py ]; then \
		rm -f /etc/apparmor.d/opt.portailvms.launcher-service.main.py; \
	fi
	systemctl reload apparmor

fix_right_serve_path:
	chown root:portailvmsd /serve/origin/ -R
	chmod u=rwx,g=rx,o= /serve/origin/
	find /serve/origin/ -name "*.qcow2" -type f -exec chmod u=rw,g=r,o= {} \;
	chown root:kvm /serve/tmp/
	chmod u=rwx,g=x,o= /serve/tmp/
	chown root:portailvmsd /serve/ISO/ -R
	chown portailvmsd:portailvmsd /serve/upload/
	chmod u=rwx,g=rx,o= /serve/upload/
	find /serve/ISO/ -type d -exec chmod u=rwx,g=rx,o=rx {} \;
	find /serve/ISO/ -name "*iso" -type f -exec chmod u=rw,g=r,o=r {} \;

