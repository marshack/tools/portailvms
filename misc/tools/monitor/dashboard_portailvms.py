#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais Ministere de la Defense
# Portions created by the Initial Developer are Copyright (C)

import sys, os, re
import subprocess, psutil
import docker
from pylxd import Client as ClientLxd
import lxc
import netifaces

__version__="0.4.0"


class DashBoard():
    def __init__(self, lxc_containers_path, debug_enabled=False):
        self.lxc_containers_path=lxc_containers_path
        try:
            self.client_docker=docker.from_env()
            self.client_docker.version()
        except:
            self.client_docker=None
        try:
            self.client_lxd=ClientLxd()
        except:
            self.client_lxd=None
        # output color
        self.BLUE='\033[36m'
        self.RED='\033[1;31m'
        self.YELLOW='\033[0;33m'
        self.NC='\033[0m' # No Color
        self.debug_enabled=debug_enabled

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def printError(self, msg):
        print("{0}{2}{1}".format(self.RED, self.NC, msg))

    def printSection(self, section):
        print("\n{0}{2}{1} :\n".format(self.YELLOW, self.NC, section))

    def printValue(self, key, value=""):
        print("{0}{2}{1} : {3}".format(self.BLUE, self.NC, key, value))

    def execCommand(self, cmd, wait=True):
        self.debug('DashBoard:execCommand - {}'.format(" ".join(cmd)))
        p=subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if not wait:
            return 0, [], []
        else:
            returncode=p.wait()
            stderr=p.stderr.read().decode().split('\n')
            stdout=p.stdout.read().decode().split('\n')
            return returncode, stdout, stderr

    def getKvmRunning(self, pattern=r'(kvm|qemu-system-x86_64).*\s+-name\s+(\S+)\s+.*sock_(\S+)\,server\,nowait'):
        res={}
        for p in psutil.process_iter():
            if p.name() in ('qemu-system-x86_64', 'kvm'):
                c=' '.join(p.cmdline())
                m=re.search(pattern, c)
                if m:
                    res[m[3]]=m[2]
        return res

    def getLxcRunning(self, pattern=r'(host-\S+)'):
        res={}
        for name in lxc.list_containers(config_path=self.lxc_containers_path):
            c=lxc.Container(name, config_path=self.lxc_containers_path)
            state=c.state
            m=re.search(pattern, name)
            if m:
                res[name]=state.upper()
        return res

    def getLxdRunning(self, pattern=r'(host-\S+)'):
        res={}
        if not self.client_lxd:
            return res
        for container in self.client_lxd.containers.all():
            state=container.status
            name=container.name
            m=re.search(pattern, name)
            if m:
                res[name]=state.upper()
        return res

    def getDockerRunning(self, pattern=r'(host-\S+)'):
        res={}
        if not self.client_docker:
            return res
        for container in self.client_docker.containers.list():
            state=container.status
            name=container.name
            m=re.search(pattern, name)
            if m:
                res[name]=state.upper()
        return res

    def getArchiRunning(self):
        res={}
        for p in psutil.process_iter():
            if p.name() == 'python3':
                c=' '.join(p.cmdline())
                if "archimulator.py" in c:
                    m=re.search(r'archimulator\.py.*\s+-f\s+(\S+)\s+.*sock_archi_(\S+)\:\S+\:\S+\:\d+', c)
                    if m:
                        res[m[2]]=m[1]
        return res

    def getNetNS(self):
        code, stdout,_ = self.execCommand(('ip','netns'))
        res=[]
        if code==0:
            for c in stdout:
                c=c.split(' ')
                if len(c)>2:
                    res.append(c[0])
        return res

    def getInterfaces(self):
        return [ x for x in netifaces.interfaces() if "-A-" in x or "-B-" in x]


if __name__ == '__main__':
    lxc_not_running=[]
    lxd_not_running=[]
    docker_not_running=[]

    dash=DashBoard("/serve/portailvmsd/lxc/containers/", debug_enabled=False)
    dash.printSection("Machines KVM en cours de fonctionnement")
    for key, value in dash.getKvmRunning().items():
        dash.printValue(value, key)

    # archi with kvm
    for key, value in dash.getKvmRunning(r'(kvm|qemu-system-x86_64).*\s+-name\s+(\S+)\s+.*\/archimulator\/.*kvm_(\S+).sock\,server\,nowait').items():
        dash.printValue(value, key)

    dash.printSection("Containers en cours de fonctionnement")
    # LXC
    for key, value in dash.getLxcRunning().items():
        dash.printValue(f"LXC:{key}", value)
        if 'RUNNING' not in value:
            lxc_not_running.append(key)

    # archi with lxc
    lxc_archi=[]
    for key, value in dash.getLxcRunning(r'(lc-\S+)').items():
        dash.printValue(f"LXC:{key}", value)
        lxc_archi.append(key)
        if 'RUNNING' not in value:
            lxc_not_running.append(key)

    # LXD
    for key, value in dash.getLxdRunning().items():
        dash.printValue(f"LXD:{key}", value)
        if 'RUNNING' not in value:
            lxd_not_running.append(key)

    # archi with lxd
    lxd_archi=[]
    for key, value in dash.getLxdRunning(r'(ld-\S+)').items():
        dash.printValue(f"LXD:{key}", value)
        lxd_archi.append(key)
        if 'RUNNING' not in value:
            lxd_not_running.append(key)

    for key, value in dash.getDockerRunning().items():
        dash.printValue(f"DCK:{key}", value)
        if 'RUNNING' not in value:
            docker_not_running.append(key)

    # archi with docker
    docker_archi=[]
    for key, value in dash.getDockerRunning(r'(dr-\S+)').items():
        dash.printValue(f"DCK:{key}", value)
        docker_archi.append(key)
        if 'RUNNING' not in value:
            docker_not_running.append(key)

    dash.printSection("Architectures en cours de fonctionnement")
    for key, value in dash.getArchiRunning().items():
        dash.printValue(value.replace('//','/'), key)

    dash.printSection("Anomalies")
    # cherche zombies

    for l in lxc_not_running:
        dash.printError('Container LXC non démarré : {}'.format(l))

    for l in lxd_not_running:
        dash.printError('Container LXD non démarré : {}'.format(l))

    for l in docker_not_running:
        dash.printError('Container Docker non démarré : {}'.format(l))

    tmp_archi_lock='/tmp/archimulator/lock/'
    tmp_portail_lock='/tmp/portailvms/lock/'
    if os.path.exists(tmp_archi_lock) and os.path.exists(tmp_portail_lock):
        locks_archi=os.listdir(tmp_archi_lock)
        locks_portailvms=os.listdir(tmp_portail_lock)
        netns=dash.getNetNS()
        archi=[]
        portail=[]

        for k in locks_archi:
            m=re.search(r'archi_(\S+)\.lock', k)
            if m:
                archi.append(m[1])

        for k in locks_portailvms:

            m=re.search(r'(\S+)\.lock', k)
            if m:
                portail.append(m[1])

        for n in netns:
            found=False
            for a in archi:
                if a in n:
                    found=True
                    break
            if not found:
                dash.printError('Network NS zombie : {}'.format(n))

        for a in archi:
            found=False
            for n in netns:
                if a in n:
                    found=True
                    break
            if not found:
                dash.printError('Network NS manquant : {}'.format(a))

        for l in lxc_archi:
            found=False
            for a in archi:
                if a in l:
                    found=True
                    break
            if not found:
                dash.printError('LXC zombie : {}'.format(l))

        for l in lxd_archi:
            found=False
            for a in archi:
                if a in l:
                    found=True
                    break
            if not found:
                dash.printError('LXD zombie : {}'.format(l))

        for l in docker_archi:
            found=False
            for a in archi:
                if a in l:
                    found=True
                    break
            if not found:
                dash.printError('Docker zombie : {}'.format(l))

        interfaces=dash.getInterfaces()
        for i in interfaces:
            found=False
            # search in arch
            for a in archi:
                if a in i:
                    found=True
                    break
            # search in portailvms
            for a in portail:
                if a in i:
                    found=True
                    break
            
            if not found:
                dash.printError('Interface zombie : {}'.format(i))

        # cherche interfaces non rattachées
        r=[x.replace('-B-','-A-') for x in interfaces]
        dupl=[ x for x in r if r.count(x)==2]
        seen=[]
        for x in dupl:
            if x not in seen:
                seen.append(x)
                dash.printError(f"Interface not attached : '{x}' (and associated : -B-)")

        netns_files=os.listdir('/var/run/netns/')
        for f in netns_files:
            is_symlink=False
            f=f'/var/run/netns/{f}'
            try:
                is_symlink=os.path.islink(f)
            except:
                pass
            if is_symlink:
                netns_exist=False
                try:
                    netns_exist=os.path.exists(os.readlink(f))
                except:
                    pass
                if not netns_exist:
                    dash.printError(f'Symlink is dead : {f}')
