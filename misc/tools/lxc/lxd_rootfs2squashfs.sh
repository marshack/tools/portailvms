#!/bin/bash
#
# Written by erable <erable@marshack.fr>
# Portions created by the Initial Developer are Copyright (C)

# Conversion d'un export d'image LXD de rootfs vers squashfs (lxd 3.x -> 4.x)
#
# ce script requiert squashfs-tools :
#    sudo apt install squashfs-tools-ng

error () {
    printf "\nError : $1\n\n"
    exit 1
}

## verifie l'existence d'une commande
# argument 1 : nom d'une commande
cmd_exist () {
    type $1 > /dev/null 2>&1
    return $?
}

checkIsRoot () {
    if [ "$(id -u)" -ne 0 ]; then return 1; fi
}


#########################################################################################
#### Main

usage="Usage: $0 ARCHIVE_NAME
Conversion d'un export d'image LXD de rootfs vers squashfs (lxd 3.x -> 4.x)

Options:
  -h : display this help and exit
"

while getopts "h" opt; do
  case $opt in
    h)
      exec echo "$usage"
      exit 0
      ;;
    \?)
      exec echo "$usage"
      exit 1
      ;;
  esac
done

shift $((OPTIND-1))

if [ "$#" -ne 1 ]; then
      exec echo "$usage"
      exit 1
fi

archive_name="$*"
base_name="$(basename ${archive_name})"
image="${base_name%%.*}"

cmd_exist mksquashfs || error "La commande mksquashfs est requise"
checkIsRoot || error "Ce script doit être executé en tant que root (uid 0)"

current_user=${SUDO_USER}
test -z ${current_user} && error "Impossible de récupérer l'utilisateur 'SUDO_USER'"

tar xf ${archive_name} || error "Impossible d'extraire l'archive"
test -f meta-${image}.tar.xz && error "Un fichier portant le même nom existe déjà : meta-${image}.tar.xz"
test -f ${image}.squashfs && error "Un fichier portant le même nom existe déjà : ${image}.squashfs"

mksquashfs rootfs/ ${image}.squashfs -noappend -comp xz -b 1M || error "Impossible de créer le système de fichiers squashfs"
tar Jcf meta-${image}.tar.xz metadata.yaml templates/  || error "Impossible de créer le fichier de meta-données"
chown ${current_user}:${current_user} ${image}.squashfs meta-${image}.tar.xz -R || error "Impossible de modifier les droits"
# suppression des fichiers temporaires
rm -rf ./rootfs ./templates/ metadata.yaml 2>/dev/null

echo -e "\nOpération terminée sans erreur"
