#!/bin/bash
#
# Written by erable <erable@marshack.fr>
# Portions created by the Initial Developer are Copyright (C)

# Conversion d'un export d'image LXD de squashfs vers rootfs
#
# ce script requiert squashfs-tools :
#    sudo apt install squashfs-tools-ng

error () {
    printf "\nError : $1\n\n"
    exit 1
}

## verifie l'existence d'une commande
# argument 1 : nom d'une commande
cmd_exist () {
    type $1 > /dev/null 2>&1
    return $?
}

checkIsRoot () {
    if [ "$(id -u)" -ne 0 ]; then return 1; fi
}


#########################################################################################
#### Main

usage="Usage: $0 IMAGE_NAME

Conversion d'un export d'image LXD de squashfs vers rootfs

Options:
  -h : display this help and exit
"

while getopts "h" opt; do
  case $opt in
    h)
      exec echo "$usage"
      exit 0
      ;;
    \?)
      exec echo "$usage"
      exit 1
      ;;
  esac
done

shift $((OPTIND-1))

if [ "$#" -ne 1 ]; then
      exec echo "$usage"
      exit 1
fi

image="$*"

cmd_exist sqfs2tar || error "La commande sqfs2tar est requise"
checkIsRoot || error "Ce script doit être executé en tant que root (uid 0)"

current_user=${SUDO_USER}
test -z ${current_user} && error "Impossible de récupérer l'utilisateur 'SUDO_USER'"

test -f ${image}.squashfs || error "Le fichier ${image}.squashfs est manquant"
test -f meta-${image}.tar.xz || error "Le fichier meta-${image}.tar.xz est manquant"
test -f ${image}.tar.gz && error "Un fichier portant le même nom existe déjà : ${image}.tar.gz"
test -d templates && error "Un dossier 'templates' existe déjà dans le répertoire courant"
test -f metadata.yaml && error "Un fichier 'metadata.yaml' existe déjà dans le répertoire courant"

tar Jxf meta-${image}.tar.xz || error "Impossible d'extraire le fichier de meta-données"
sqfs2tar ${image}.squashfs -r rootfs > ${image}.tar || error "Impossible de créer le système de fichiers rootfs"
tar rf ${image}.tar templates/ metadata.yaml || error "Impossible d'ajouter les meta-données dans l'archive ${image}.tar"
gzip ${image}.tar || error "Impossible de compresser l'archive ${image}.tar"
chown ${current_user}:${current_user} ${image}.tar.gz || error "Impossible de modifier les droits"
# suppression des fichiers temporaires
rm -rf templates/ metadata.yaml 2>/dev/null

echo -e "\nOpération terminée sans erreur"
