#!/bin/bash
#
# Written by erable <erable@marshack.fr>
# Portions created by the Initial Developer are Copyright (C)
#
# Convert docker image to lxc image archive

#########################################################################################
#### Properties (private)

_version="0.2.0"
unset -v image
unset -v target
tmp_path="/tmp/docker2lxc/"
_force=false
_verbose=false
start_remap=100000

#########################################################################################
#### Methods

function error {
    echo -e "\nError : $1\n\n" >&2
    exit 1
}

function warning {
    printf "\nWarning : $1\n" >&2
}

function debug {
    if [ $_verbose = true ]; then
        echo "$*"
    fi
}

function isNumeric {
    if [[ $1 =~ ^[0-9]+$ ]]; then
        return 0
    fi
    return 1
}

function checkIsRoot {
    if [[ $EUID -ne 0 ]]; then return 1; fi
}

#########################################################################################
#### Main
usage="Usage: $0 [OPTION]
Convert docker image to lxc image archive

Options:
  -i : image ID 
  -t : target archive
  -r : start remap (default = 100000)
  -f : force
  -d : debug
  -h : display this help and exit
  -v : output version information and exit

Example : ./image_docker2lxc.sh -i 81bd353a18d4 -t newimage_lxc.tar.gz 
"

while getopts "vhi:t:r:fd" opt; do
  case $opt in
    i) image=$OPTARG ;;
    t) target=$OPTARG ;;
    f) _force=true ;;
    d) _verbose=true ;;
    r) start_remap=$OPTARG
        if ! isNumeric ${start_remap} ; then
            echo -e 'remap must be an integer\n' >&2
            exec echo "$usage"
            exit 1
        fi
    ;;
    h)
      exec echo "$usage"
      exit 0
      ;;
    v)
      printf "Version $_version\n"
      exit 0
      ;;
    \?)
      exec echo "$usage"
      exit 1
      ;;
  esac
done

if [ -z "$image" ] || [ -z "$target" ]; then
        echo -e 'image and target is required (-i and -t)\n' >&2
        exec echo "$usage"
        exit 1
fi

docker image inspect ${image} > /dev/null 2>&1 || error "image file ${image} doesn't exist"

if [ -e ${target} ]; then
    if [ $_force = true ]; then
        rm -f ${target} || error "Impossible to remove target file ${target}"
    else
        error "destination file ${target} already exist"
    fi
fi

checkIsRoot || error "root is required (uid 0)"

test -d ${tmp_path} || mkdir -p ${tmp_path} || error "Impossible to create tmp directory ${tmp_path}"

base_name="$(basename ${target})"
name="${base_name%%.*}"
target_tmp_path="$(readlink -m ${tmp_path}/${name})"

if [ -d ${target_tmp_path} ]; then
    if [ $_force = true ]; then
        rm -rf ${target_tmp_path} || error "Impossible to destroy ${target_tmp_path}"
    else
        error "tmp directory ${target_tmp_path} already exist"
    fi
fi

mkdir -p ${target_tmp_path} || error "Impossible to create tmp directory ${target_tmp_path}"

tmp_container_name="${name}_tmp_convert"
tmp_container_archive="${tmp_path}/${tmp_container_name}.tar"

echo " - Create container from image ${image}"
docker inspect ${tmp_container_name} > /dev/null 2>&1 && 
    if [ $_force = true ]; then docker rm -f ${tmp_container_name} ; else error "${tmp_container_name} already exist. Remove it first" ; fi

docker create --name ${tmp_container_name} ${image}
retval=$?
if [ $retval -ne 0 ] ; then
    docker rm ${tmp_container_name} > /dev/null 2>&1
    error "Impossible to create temporary container ${tmp_container_name}"
fi

# retreive config
tmp_config_file="${tmp_path}/${name}.config"
# entrypoint and cmd
docker_cmd="$(docker inspect ${tmp_container_name} -f '{{.Config.Cmd}}' 2>/dev/null | sed 's/[][]//g')"
docker_entrypoint="$(docker inspect ${tmp_container_name} -f '{{.Config.Entrypoint}}' 2>/dev/null | sed 's/[][]//g')"
if [ -n "${docker_cmd}" ] || [ -n "${docker_entrypoint}" ]; then
    echo -e "# Entrypoint/cmd\nlxc.init.cmd = ${docker_entrypoint} ${docker_cmd}" > ${tmp_config_file}
fi
# workingdir
docker_workingdir="$(docker inspect ${tmp_container_name} -f '{{.Config.WorkingDir}}' 2>/dev/null)"
if [ -n "${docker_workingdir}" ] ; then
    echo -e "\n# Working dir\nlxc.init.cwd = ${docker_workingdir}" >> ${tmp_config_file}
fi
# environment variables
if [ -n "$(docker inspect ${tmp_container_name} -f '{{.Config.Env}}' 2>/dev/null)" ] ; then
    echo -e "\n# Environment variables" >> ${tmp_config_file}
fi
docker inspect ${tmp_container_name} -f '{{range $index, $value := .Config.Env}}{{println $value}}{{end}}' 2>/dev/null | while read line ; do
    if [[ $line =~ "=" ]]; then
        echo -e "lxc.environment = $line" >> ${tmp_config_file}
    fi
done

docker export ${tmp_container_name} --output="${tmp_container_archive}"
retval=$?

docker rm ${tmp_container_name} > /dev/null 2>&1
if [ $retval -ne 0 ] ; then
    error "Impossible to create archive ${tmp_container_archive}"
fi

rootfs="${target_tmp_path}/rootfs"
config_file="${target_tmp_path}/config"

mkdir -p ${rootfs} || error "Impossible to create rootfs directory ${rootfs}"
chmod 755 ${rootfs} || error "Impossible to chmod ${rootfs}"

echo " - Extract archive ${tmp_container_name}.tar"
tar -xf ${tmp_container_archive} -C ${rootfs} || error "Impossible to extract ${tmp_container_archive} to ${rootfs}"

echo " - Create configuration file"

echo "# Distribution configuration
lxc.include = /usr/share/lxc/config/common.conf
lxc.include = /usr/share/lxc/config/userns.conf
lxc.arch = linux64

# Container specific configuration
lxc.idmap = u 0 ${start_remap} 65536
lxc.idmap = g 0 ${start_remap} 65536
lxc.rootfs.path = btrfs:/serve/${name}/rootfs
lxc.uts.name = ${name}

# Network configuration
lxc.net.0.type = veth
lxc.net.0.link = br0
lxc.net.0.flags = up
lxc.net.0.hwaddr = 00:16:3e:70:5d:d8
" > ${config_file}

test -f ${tmp_config_file} && cat ${tmp_config_file} >> ${config_file}

echo " - Remap uid:gid -> ${start_remap}"

find -P ${rootfs} -print | while IFS= read -r f; do
    f="$(echo ${f} | sed s#//*#/#g)"
    debug "current file : ${f}"
    current_uid=$(stat -c '%u' "${f}")
    current_gid=$(stat -c '%g' "${f}")
    if [ ! -L "${f}" ]; then
        current_perm=$(stat -Lc '%a' "${f}")
        isNumeric ${current_perm} || error "Impossible to retrieve permissions for ${f}"
        debug "current_perm : ${current_perm}"
    fi
    debug "current_uid : ${current_uid}"
    debug "current_gid : ${current_gid}"
    # check before remap
    isNumeric ${current_uid} || error "Impossible to retrieve UID"
    isNumeric ${current_gid} || error "Impossible to retrieve GID"
    if [ "${current_uid}" -ge "${start_remap}" ]; then
        warning "${f} Already mapped"
    else
        new_uid=$(( ${current_uid} + ${start_remap} ))
        new_gid=$(( ${current_gid} + ${start_remap} ))
        debug "new_uid : ${new_uid}"
        debug "new_gid : ${new_gid}"
        chown -h ${new_uid}:${new_gid} "${f}" || error "Impossible to change owner of ${f}"
        if [ ! -L "${f}" ]; then
            new_perm=$(stat -Lc '%a' "${f}")
            isNumeric ${new_perm} || error "Impossible to retrieve permissions for ${f}"
            if [ "${current_perm}" -ne "${new_perm}"  ]; then
                warning "Restore permissions for ${f} ($new_perm -> $current_perm)"
                chmod ${current_perm} "${f}" || error "Impossible to chmod ${f}"
            fi
        fi
    fi
done

if [ -f ${rootfs}/etc/hosts ]; then
    echo " - fix etc/hosts"
    echo "127.0.1.1	${name}
127.0.0.1	localhost
::1		localhost ip6-localhost ip6-loopback
ff02::1		ip6-allnodes
ff02::2		ip6-allrouters" > ${rootfs}/etc/hosts || warning "Impossible to fix ${rootfs}/etc/hosts"
fi

if [ -f ${rootfs}/etc/hostname ]; then
    echo " - fix etc/hostname"
    echo -n "${name}" > ${rootfs}/etc/hostname || warning "Impossible to fix ${rootfs}/etc/hostname"
fi

echo " - Create archive ${target}"
tar -czf ${target} -C ${tmp_path} ${name} || error "Impossible to create ${target} from ${target_tmp_path}"

echo " - Remove temporary files"
rm -rf ${target_tmp_path} || error "Impossible to destroy ${target_tmp_path}"
rm -f  ${tmp_container_archive} || error "Impossible to destroy ${tmp_container_archive}"
test -f ${tmp_config_file} && rm -f ${tmp_config_file}
echo " - Done"
