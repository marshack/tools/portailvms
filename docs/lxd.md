### LXD/LXC


> **Important : LXD est déprécié dans Portailvms et sa prise en charge sera supprimée dans une prochaine version**
>
> La société Canonical a modifié le modèle de développement de LXD pour en faire un projet d'entreprise plutôt qu'un projet communautaire. De plus, LXD n'était plus disponible qu'au format de paquets Snap.
>
> Toutefois, vous pouvez facilement transformer vos images LXD en images LXC à l'aide du [script disponible](https://gitlab.com/marshack/tools/portailvms/-/blob/main/misc/tools/lxc/image_lxd2lxc.sh).

#### Installation

##### Ubuntu >= 20.04 et Debian 11
LXD **requiert Snap** :
```bash
sudo apt install snapd
sudo snap install lxd --channel=latest/stable
```

##### LXD et Debian
L'intégration de *LXD* avec *Snap* est incomplète sous Debian.

Modifier le fichier */etc/sudoers*, et ajouter le chemin vers les exécutables *Snap* (`/snap/bin`) :
```
Defaults    secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin"
```

##### Modification des droits et redémarrage
```bash
sudo usermod -a -G lxd $(whoami)
sudo usermod -a -G lxd portailvmsd
sudo reboot
```

##### Vérification
```bash
lxd --version
```

#### Initialisation

Création du sous-volume qui servira de stockage au profit des images et containers :
```bash
sudo btrfs subvolume create /serve/lxd
```

Création du stockage par défaut :
```bash
lxc storage create default btrfs source=/serve/lxd/
lxc profile device add default root disk path=/ pool=default
```

Vérifications :
```bash
lxc storage list
  +---------+-------------+--------+-------------+-------------+
  |   NOM   | DESCRIPTION | PILOTE |   SOURCE    | UTILISÉ PAR |
  +---------+-------------+--------+-------------+-------------+
  | default |             | btrfs  | /serve/lxd/ | 0           |
  +---------+-------------+--------+-------------+-------------+

sudo btrfs subvolume list /serve
  ID 257 gen 16743 top level 5 path @
  ID 258 gen 16745 top level 5 path @home
  ID 276 gen 15533 top level 257 path serve/origin
  ID 277 gen 16596 top level 257 path serve/tmp
  ID 278 gen 16417 top level 257 path serve/ISO
  ID 290 gen 16742 top level 257 path serve/lxd
  ID 291 gen 16739 top level 290 path serve/lxd/containers
  ID 292 gen 16740 top level 290 path serve/lxd/snapshots
  ID 293 gen 16741 top level 290 path serve/lxd/images
  ID 294 gen 16742 top level 290 path serve/lxd/custom
```

Initialisation de lxd :
```bash
lxd init
  Would you like to use LXD clustering? (yes/no) [default=no]: 
  Do you want to configure a new storage pool? (yes/no) [default=yes]: no
  Would you like to connect to a MAAS server? (yes/no) [default=no]: 
  Would you like to create a new local network bridge? (yes/no) [default=yes]: no
  Would you like to configure LXD to use an existing bridge or host interface? (yes/no) [default=no]: yes
  Name of the existing bridge or host interface: br0
  Would you like LXD to be available over the network? (yes/no) [default=no]: 
  Would you like stale cached images to be updated automatically? (yes/no) [default=yes] no
  Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]: no
```

Vérifications (exemple) :
```bash
lxc network list
  +---------+----------+------+-------------+-------------+
  |   NOM   |   TYPE   | GÉRÉ | DESCRIPTION | UTILISÉ PAR |
  +---------+----------+------+-------------+-------------+
  | br0     | bridge   | NON  |             | 0           |
  +---------+----------+------+-------------+-------------+
  | enp0s3  | physical | NON  |             | 0           |
  +---------+----------+------+-------------+-------------+
  | enp0s8  | physical | NON  |             | 0           |
  +---------+----------+------+-------------+-------------+
```

Désactivation de la création de l'interface réseau par défaut, lorsqu'un container est créé :
```bash
lxc profile device remove default eth0
```

Si un réseau `lxdbr0` est présent, le supprimer :
```bash
lxc network delete lxdbr0
```

Vérifications :
```bash
lxc profile show default
config: {}
description: Default LXD profile
devices:
  root:
    path: /
    pool: default
    type: disk
name: default
used_by: []
```

Si l'on souhaite re-activer l'interface réseau et le bridge par défaut :
```bash
lxc network create lxdbr0
lxc profile device add default eth0 nic name=eth0 nictype=bridged parent=lxdbr0
```

> Si l'on souhaite offrir un accès à un terminal dans des containers jetables, il est possible d'installer [ttyd](ttyd.md)

#### Création d'une image à partir d'une autre

Exemple de création d'une image apache2, à partir d'une image "alpine" disponible sur le depôt distant "images" :
```
$ lxc launch images:alpine/3.11 alpine
  Création de alpine
  Démarrage de alpine
$ lxc exec alpine sh
$ apk add apache2
$ rc-service apache2 start
$ rc-update add apache2
$ halt
$ lxc publish alpine --alias apache2
$ lxc delete alpine
```
Créer ensuite une entrée dans la table *challenges*, à l'aide de l'interface web du PortailVMS (profil administrateur).

#### Export/Import

Les images peuvent être exportées et importées d'une machine à une autre :
```bash
# machine A
lxc image export apache2 .
# machine B
lxc image import bf9c161e80734b2fff2db574a727be019ec5afd7652e8c4a29abfc8d267d0693.tar.gz --alias apache2
```

L'import d'une image LXD/LXC peut être effectué via l'interface web, menu *Administrer/Téléverser*.

