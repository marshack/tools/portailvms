### LXC

#### Prérequis

Un système de fichiers **btrFS**, **gérant la copie légère** (CoW: *Copy on Write*).

#### Installation

```bash
sudo apt install lxc
```

#### Configuration

Modifier le fichier `/etc/default/lxc-net` en remplaçant :
```ini
USE_LXC_BRIDGE="true"
```
par :
```ini
USE_LXC_BRIDGE="false"
```

Création des sous-volumes :
```bash
sudo btrfs subvolume create /serve/portailvmsd
sudo btrfs subvolume create /serve/portailvmsd/lxc
sudo btrfs subvolume create /serve/portailvmsd/lxc/images/
sudo btrfs subvolume create /serve/portailvmsd/lxc/containers/
```

Modification des droits :
```bash
sudo chown portailvmsd:portailvmsd /serve/portailvmsd -R
```

Activation des quotas sur le volume :
```bash
sudo btrfs quota enable /serve
```

**Note :** Pour les quotas (cpu, ...), éditer à l'aide de la commande :
```bash
sudo systemctl edit user@$(id -u portailvmsd).service
```
et insérer :
```ini
[Service]
Delegate=cpu cpuset io
```

Créer le fichier `lxc.conf` :
```bash
sudo -u portailvmsd mkdir -p /serve/portailvmsd/.config/lxc
echo "lxc.lxcpath = /serve/portailvmsd/lxc/containers/" | sudo -u portailvmsd tee /serve/portailvmsd/.config/lxc/lxc.conf
```

Créer le fichier `default.conf` :
```bash
echo "lxc.net.0.type = veth
lxc.net.0.link = br0
lxc.net.0.flags = up
lxc.net.0.hwaddr = 00:16:3e:xx:xx:xx
lxc.idmap = u 0 100000 65536
lxc.idmap = g 0 100000 65536" | sudo -u portailvmsd tee /serve/portailvmsd/.config/lxc/default.conf
```

Ajouter dans les fichiers `/etc/subuid` et `/etc/subgid` :
```
portailvmsd:100000:65536
root:100000:65536
```

Autoriser l'utilisateur *portailvmsd* à raccorder des interfaces sur le bridge (250 conn.) :
```bash
echo "portailvmsd veth br0 250" | sudo tee -a /etc/lxc/lxc-usernet
```

Afin de pouvoir démarrer des containers non privilégiés avec le compte système *portailvmsd* :
```bash
sudo loginctl enable-linger portailvmsd
```

#### Redémarrage
```bash
sudo reboot
```

#### Vérifications
```bash
lxc-start --version
4.0.6

sudo -u portailvmsd lxc-config lxc.lxcpath
/serve/portailvmsd/.local/share/lxc

sudo -u portailvmsd lxc-config lxc.default_config
/serve/portailvmsd/.config/lxc/default.conf

cat "/sys/fs/cgroup/user.slice/user-$(id -u portailvmsd).slice/user@$(id -u portailvmsd).service/cgroup.controllers"
cpuset cpu io memory pids

lxc-checkconfig
...
```

> Si l'on souhaite offrir un accès à un terminal dans des containers jetables, il est possible d'installer [ttyd](ttyd.md)

Chemin des images LXC : `/serve/portailvmsd/lxc/images/`

L'import d'une image LXC peut être effectué via l'interface web, menu *Administrer/Téléverser*.
