### Architecture (Archimulator)

Il est possible de fournir une architecture complète dans *PortailVMS*. Pour cela, il est nécessaire d'installer l'outil **Archimulator** : https://gitlab.com/marshack/tools/archimulator

Installer l'outil, et créer volumes et répertoires :
```bash
sudo btrfs subvolume create /serve/archimulator
sudo mkdir --mode=750 /serve/archimulator/kvm
sudo mkdir --mode=750 /serve/archimulator/templates
sudo chown root:portailvmsd /serve/archimulator/*
```

Modifier le fichier de configuration `/etc/archimulator/lxc_plugin.conf` :
```ini
[PATH]
source_dir=/serve/portailvmsd/lxc/images
tmp_dir=/serve/portailvmsd/lxc/containers
```

Modifier le fichier de configuration `/etc/archimulator/kvm_plugin.conf` :
```ini
[PATH]
configurations=/serve/archimulator/kvm/
target_disk=/serve/tmp
tmp_dir=/tmp/archimulator/kvm/sock/
```

Une fois l'outil installé et configuré, créer un *template* décrivant l'architecture, au format `yaml` (exemple) :
```yaml
archi:
    name : example
    version : 1.0
switch:
    name : switch-example
    interfaces :
        1 :
            link : br0
            intname : eth0
            type : external
            vlan : 1
        2 :
            link : tap2
            vlan : 2
vlans :
    1 :
        intname: vlan1
        addresses : [ {{ADDRESS}}/24 ]
        mac : "{{MAC}}"
    2 :
        intname: vlan2
        addresses : [ 192.168.5.1/24 ]
routes :
        forward: yes
        nat:
            source: [ 192.168.5.0/24 ]
            out: vlan1
        dnat:
          - in: vlan1
            protocole : tcp
            dports: [ 21, 22, 23, 80, 443, 990 ]
            to : 192.168.5.2
machines :
    machine-1 :
        honeypot :
            interfaces:
                link : tap2
                addresses : [ 192.168.5.2/24 ]
                gw : 192.168.5.1
            services : [ssh, meta-ftp, meta-web, telnet]
```

Contraintes à respecter :
 - `{{ADDRESS}}` et/ou `{{MAC}}` doit être présent dans le fichier de configuration `yaml` (l'un ou l'autre, ou les deux). *PortailVMS* remplacera  `{{ADDRESS}}` par l'adresse IP allouée à l'utilisateur qui a démarré l'architecture (gestion dynamique). Idem pour `{{MAC}}` (adresse MAC allouée à l'utilisateur). Si seulement `{{MAC}}` est fournie, l'adresse IP devra être récupérée à l'aide d'un client DHCP (utilisation de la clé `dhcp4: yes`).
 - Une interface du *switch* devra être de type `external` afin de pouvoir être raccordée sur le *bridge* géré par *portailVMS* (raccordé sur `br0`)
 - Si une machine de type KVM est présente dans l'architecture, et que l'on souhaite utiliser VNC avec *portailVMS*, le *template* devra être de la forme (routage à inclure, plus mot-clé `{{PASSWORD}}`) :

 ```yaml
machines :
    machine-1:
         kvm :
            image: alpine
            link : tap1
            vnc:
                listen : 127.0.0.1
                port: 1
                password : "{{PASSWORD}}"
routes :
        forward: yes
        nat:
            source: [ 192.168.5.0/24 ]
            out: vlan1
        dnat:
          - in: vlan1
            source: [ 172.24.8.1 ]
            protocole : tcp
            dports: [ 5901 ]
            to : 127.0.0.1
 ```

> Voir la documentation de l'outil [Archimulator](https://gitlab.com/marshack/tools/archimulator) pour plus d'informations
