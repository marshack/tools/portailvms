### KVM

#### Prérequis

**KVM requiert une virtualisation matérielle** (capacité CPU).

```bash
egrep -c '(vmx|svm)' /proc/cpuinfo
```
Doit retourner un nombre différent de zéro. De plus, le fichier spécial "kvm" doit être présent :

```bash
ls -l /dev/kvm
```

Le système de fichiers **btrFS**, **gérant la copie légère** (CoW: *Copy on Write*) **est également requis**.

#### Installation et configuration
```bash
sudo apt install qemu-kvm ovmf
```

#### Créer un groupe au profit des machines virtuelles

Si le groupe n'existe pas :
```bash
sudo groupadd -r kvm
```

Le périphérique *kvm* doit appartenir au groupe *kvm*. Vérifier les droits :
```bash
ls -l /dev/kvm
  crw-rw---- 1 root kvm 10, 232 janv. 19 10:18 /dev/kvm
```

Pour corriger sans redémarrer :
```bash
chown root:kvm /dev/kvm
chmod g+rw,o= /dev/kvm
```

Pour rendre persistant pour les prochains démarrages, créer le fichier */etc/udev/rules.d/80-kvm.rules* :
```
KERNEL=="kvm", GROUP="kvm", MODE="0660"
```

#### Créer un compte non privilégié, au profit des machines virtuelles
```bash
sudo groupadd -r vmunpriv
sudo useradd -rNM vmunpriv -G kvm -g vmunpriv -d /dev/null -s /sbin/nologin -c "Unprivileged virtual machine account"
sudo passwd -l vmunpriv
```

#### NoVNC

Si l'on souhaite offrir un déport d'affichage de nos machines virtuelles, il est nécessaire d'installer [novnc](novnc.md)

### qemu monitor et socket unix

Il est possible d'interagir avec la machine virtuelle KVM en cours de fonctionnement, via l'interface '*monitor*' de qemu. Pour y accéder, il faut réaliser une connexion sur le socket unix associé à la VM, à l'aide de l'utilitaire socat (Remplacer UUID par celui donné par l'interface web) :

```bash
socat - UNIX-CONNECT:/tmp/portailvms/sock/sock_UUID
```

#### Quelques commandes qemu monitor

    # voir l'aide
    (qemu) help
    # affichage des périphériques type bloc (disques dur, cdrom, ...)
    (qemu) info block
    # monter un iso
    (qemu) change ide1-cd0 /serve/ISO/xubuntu-16.04.2-desktop-amd64.iso
    # changer le démarrage (c=disque dur, d=cdrom)
    (qemu) boot_set d
    # ejecter le cdrom (-f pour forcer)
    (qemu) eject ide1-cd0
    # arrêt de la machine
    (qemu) quit
    # redémarrage de la machine
    (qemu) system_reset
    # modifier le mot de passe vnc
    (qemu) set_password vnc my_new_password


## Préparation d'un environnement virtuel

#### Installation d'un nouveau système d'exploitation

 - déposer une image ISO du système d'exploitation que vous souhaitez installer (menu *Administrer/Téléverser*)
 - créer un nouvel environnement (menu *Administrer/Configurer*), renseigner les champs et cliquer sur *Nouveau fichier qcow2*
 - dans *Administrer/Images Maîtres*, choisir l'environnement précédemment créé et démarrer
 - une fois que l'environnement démarre, sélectionner l'image ISO de l'OS à installer et valider
 - cliquer sur redémarrer
 - suivre la procédure d'installation ...
 - à l'issu, n'oubliez pas d'installer les pilotes *Virtio* (voir chapitre *Pilotes Virtio* ci-dessous)

#### Récupération d'une machine virtuelle existante

> Exemple pour une image disque de type VirtualBox (.vdi)

 - convertir l'image, du format *vdi* au format *qcow2* : `qemu-img convert -f vdi -O qcow2 image.vdi image.qcow2`
 - déposer le fichier qcow2 sur PortailVMS, dans le répertoire */serve/origin* :
     - soit via l'interface web menu *Administrer/Téléverser* (attention à la limite de taille de fichier)
     - soit via un transfert SSH, penser à fixer les droits (voir chapitre *Fixer les droits* ci-dessous)
 - créer un nouvel environnement (menu *Administrer/Configurer*) et renseigner les champs
 - dans *Administrer/Images Maîtres*, choisir l'environnement précédemment créé et démarrer
 - une fois le système démarré, désinstaller les *guests additions*/*VMWare Tools* (s'ils étaient installés), et installer les pilotes *Virtio* (voir chapitre *Pilotes Virtio* ci-dessous)

#### Pilotes Virtio

Sources : https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/stable-virtio/

Les pilotes *virtio* permettent d'accélérer les accès disques, réseaux, ... entre la machine virtuelle et la machine hôte. Normalement, ces pilotes sont déjà installés sur les distributions Linux récentes.

à minima, sous Windows installer les pilotes :

 - **Virtio balloon** (libérer des ressources en mémoire vive, si non utilisées sur la machine virtuelle)
 - **Virtio network** (pour accélérer les accès réseaux)

 Si vous utilisez un autre système de fichier que *btrFS*, vous pouvez également installer le pilote **Virtio SCSI**.

**Virtio network** et **Virtio SCSI** doivent être activés individuellement, pour chaque entrée utilisant le moteur KVM (*balloon* est activé par défaut).

### Fixer les droits

Après création d'images disques, il peut être nécessaire de fixer les droits :

```bash
make fix_right_serve_path
```

> Notes : Les fichiers disques des machines virtuelles jetables seront présents dans le dossier */serve/tmp*. Les fichiers ISO (cdrom), doivent être déposés dans le dossier */serve/ISO*.
