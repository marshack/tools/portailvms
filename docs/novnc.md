### noVNC

**noVNC** est un portail web permettant d'agir comme proxy entre un navigateur et VNC démarré par *Qemu*.

Installer noVNC et websockify :

```bash
sudo apt install python3-websockify python3-numpy
git clone https://github.com/novnc/noVNC
cd noVNC/utils/
git clone https://github.com/novnc/websockify
cd websockify/websockify/
py3compile *.py
cd ../../../../
sudo mkdir -p /opt/noVNC/pem
sudo mv noVNC /opt/noVNC/proxyVNC
cat <<EOF >index.html
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="refresh" content="0;URL=vnc.html">
    </head>
    <body></body>
</html>
EOF
sudo mv index.html /opt/noVNC/proxyVNC/
```


#### Créer un certificat et clé privée, puis installer :

```bash
openssl req -x509 -nodes -newkey rsa:4096 -keyout novnc.pem -out novnc.pem -days 1500
sudo mv novnc.pem /opt/noVNC/pem/
```


#### Créer un utilisateur dédié, et modification des droits :

```bash
sudo groupadd -r novnc
sudo useradd -rNM novnc -g novnc -d /dev/null -s /sbin/nologin -c "noVNC account"
sudo passwd -l novnc
sudo chown root:novnc /opt/noVNC/ -R
sudo chmod u=r,g=r,o-rwx /opt/noVNC/pem/novnc.pem
```

#### En tant que root, tester :

```bash
sudo bash
su novnc -s /bin/sh -c  "/opt/noVNC/proxyVNC/utils/websockify/websockify.py --web /opt/noVNC/proxyVNC/ --ssl-only --cert /opt/noVNC/pem/novnc.pem 6901 127.0.0.1:5901"
```

Et tester dans un navigateur (accepter le certificat) :

<https://127.0.0.1:6901/vnc.html>

`Ctrl+C` pour tuer l'instance de test.

