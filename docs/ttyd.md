### ttyd

**ttyd** expose un terminal TTY, via une websocket, vers *xterm.js* (comme le fait *noVNC* pour les KVM).

À installer uniquement si l'on souhaite offrir un accès à un terminal dans des containers jetables (LXC, LXD et Docker).

> **Avertissement :** Le terminal exposé est généralement un shell avec des **privilèges élévés** dans le container (*root*). À utiliser pour débogage (au profit des administrateurs). Pour les utilisateurs, il est préférable d'offrir un accès SSH dans le container.

*ttyd* utilisera le même certificat et la même clé que *novnc* (voir [novnc](novnc.md)).

#### Installation

##### Ubuntu
```bash
sudo apt install ttyd
```

##### Debian

lire : https://github.com/tsl0922/ttyd#install-on-linux

#### Test :

```bash
sudo -u novnc ttyd -m 1 -o -W -p 5901 -S -C /opt/noVNC/pem/novnc.pem -K /opt/noVNC/pem/novnc.pem sh
```

Et dans un navigateur (accepter le certificat) :

<https://127.0.0.1:5901>

`Ctrl+C` pour tuer l'instance de test.
