### Docker

Il est possible d'intégrer des containers *Docker* dans PortailVMS.

Installation :
```bash
sudo apt install docker.io udhcpc
sudo usermod -a -G docker $(whoami)
sudo usermod -a -G docker portailvmsd
```

> Il est recommandé d'utiliser un pool de stockage de type **btrfs**, ce qui permet d’accélérer la copie du système de fichiers lors de la création du container, et préserver l'espace de stockage.

> Il est également recommandé d'utiliser les **espaces de noms utilisateur** avec Docker

Lire :
 - https://docs.docker.com/storage/storagedriver/btrfs-driver/
 - https://docs.docker.com/config/containers/resource_constraints/
 - https://docs.docker.com/engine/security/userns-remap/

Le fichier de configuration conseillé, pour utiliser *PortailVMS* avec *Docker* (*/etc/docker/daemon.json*) :
```json
{
  "storage-driver": "btrfs",
  "userns-remap": "default",
  "bridge": "none",
  "iptables": false
}
```

Création du sous-volume qui servira de stockage au profit des images et containers (**Toutes les images et containers existants seront perdus**) :
```bash
sudo btrfs subvolume create /serve/docker
sudo rm -rf /var/lib/docker
sudo ln -s /serve/docker /var/lib/docker
sudo systemctl restart docker.socket docker.service
```

> Si l'on souhaite offrir un accès à un terminal dans des containers jetables, il est possible d'installer [ttyd](ttyd.md)
