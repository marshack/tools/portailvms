# Last Modified: Sat Sep  3 14:48:25 2022
abi <abi/3.0>,

include <tunables/global>

/opt/portailvms/launcher-service/main.py {
  include <abstractions/base>
  include <abstractions/lxc/container-base>
  include <abstractions/lxc/start-container>
  include <abstractions/nameservice>
  include <abstractions/openssl>
  include <abstractions/python>

  ptrace read peer=unconfined,
  ptrace trace peer=unconfined,

  capability chown,
  capability dac_override,
  capability dac_read_search,
  capability fsetid,

  signal send set=kill peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startarchimulator.sh,
  signal send set=kill peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startdisposablevm.sh,
  signal send set=kill peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startephemerallxc.sh,
  signal send set=kill peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startephemerallxd.sh,
  signal send set=kill peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startmaster.sh,
  signal send set=kill peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startmasterlxc.sh,
  signal send set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startarchimulator.sh,
  signal send set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startdisposablevm.sh,
  signal send set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startephemeraldocker.sh,
  signal send set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startephemerallxc.sh,
  signal send set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startephemerallxd.sh,
  signal send set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startmaster.sh,
  signal send set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startmasterlxc.sh,

  /etc/portailvms/main.conf r,
  /lib/x86_64-linux-gnu/ld-*.so mr,
  /opt/portailvms/common/** r,
  /opt/portailvms/common/__pycache__/* rw,
  /opt/portailvms/launcher-service/ r,
  /opt/portailvms/launcher-service/** r,
  /opt/portailvms/launcher-service/scripts/cleanall.sh Cx,
  /opt/portailvms/launcher-service/scripts/createqcow2file.sh Cx,
  /opt/portailvms/launcher-service/scripts/initialise.sh Cx,
  /opt/portailvms/launcher-service/scripts/prune.sh Cx,
  /opt/portailvms/launcher-service/scripts/startarchimulator.sh Cx,
  /opt/portailvms/launcher-service/scripts/startdisposablevm.sh Cx,
  /opt/portailvms/launcher-service/scripts/startephemeraldocker.sh Cx,
  /opt/portailvms/launcher-service/scripts/startephemerallxc.sh Cx,
  /opt/portailvms/launcher-service/scripts/startephemerallxd.sh Cx,
  /opt/portailvms/launcher-service/scripts/startmaster.sh Cx,
  /opt/portailvms/launcher-service/scripts/startmasterlxc.sh Cx,
  /serve/ISO/*.iso w,
  /serve/archimulator/kvm/*.yaml w,
  /serve/archimulator/kvm/*.yml w,
  /serve/archimulator/templates/*.yaml w,
  /serve/archimulator/templates/*.yml w,
  /serve/origin/*.qcow2 w,
  /serve/upload/* rw,
  /usr/bin/btrfs mrix,
  /usr/bin/chmod mrix,
  /usr/bin/chown mrix,
  /usr/bin/{b,d}ash mrix,
  /usr/bin/python{2.7,3.[0-9]*} ix,
  /usr/bin/rsync mrix,
  /usr/bin/uname mrix,
  /usr/sbin/ldconfig mrix,
  /usr/sbin/ldconfig r,
  /usr/sbin/ldconfig.real mrix,
  owner /etc/magic r,
  owner /opt/portailvms/common/ r,
  owner /opt/portailvms/common/* r,
  owner /opt/portailvms/launcher-service/include/__pycache__/* rw,
  owner /proc/*/fd/ r,
  owner /proc/*/mounts r,
  owner /tmp/portailvms/ w,
  owner /tmp/portailvms/sock/ w,
  owner /tmp/portailvms/sock/launcher.sock rw,
  owner /usr/bin/{b,d}ash mr,
  owner /var/log/portailvmsd/* rw,


  profile /opt/portailvms/launcher-service/scripts/cleanall.sh {
    include <abstractions/base>
    include <abstractions/bash>
    include <abstractions/consoles>
    include <abstractions/lxc/container-base>
    include <abstractions/lxc/start-container>
    include <abstractions/nameservice>

    capability chown,
    capability dac_read_search,
    capability fowner,
    capability fsetid,
    capability net_admin,
    capability net_raw,
    capability sys_module,
    capability syslog,

    network inet raw,

    /lib/x86_64-linux-gnu/ld-*.so mr,
    /opt/portailvms/launcher-service/scripts/cleanall.sh r,
    /serve/portailvmsd/lxc/containers/* rw,
    /serve/tmp/* rw,
    /serve/upload/* rw,
    /tmp/portailvms/ w,
    /tmp/portailvms/lock/ rw,
    /tmp/portailvms/lock/* rw,
    /tmp/portailvms/sessions/ rw,
    /tmp/portailvms/sessions/* w,
    /tmp/portailvms/sock/ rw,
    /tmp/portailvms/sock/* rw,
    /usr/bin/btrfs mrix,
    /usr/bin/chmod mrix,
    /usr/bin/chown mrix,
    /usr/bin/{b,d}ash mrix,
    /usr/bin/dirname mrix,
    /usr/bin/docker mrix,
    /usr/bin/{,e}grep mrix,
    /usr/bin/[gm]awk mrix,
    /usr/bin/ip mrix,
    /usr/bin/kill mrix,
    /usr/bin/killall PUx,
    /usr/bin/kmod mrix,
    /usr/bin/lxc PUx,
    /usr/bin/lxc-{destroy,ls,wait,stop} mrix,
    /usr/bin/mkdir mrix,
    /usr/bin/ps mrix,
    /usr/bin/readlink mrix,
    /usr/bin/rm mrix,
    /usr/bin/sed mrix,
    /usr/bin/sleep mrix,
    /usr/bin/xargs mrix,
    /usr/sbin/ipset mrix,
    /usr/sbin/sysctl mrix,
    /usr/sbin/xtables-nft-multi mrix,
    owner /etc/iproute2/group r,
    owner /etc/modprobe.d/ r,
    owner /etc/modprobe.d/* r,
    owner /opt/portailvms/launcher-service/scripts/sources/*.sh r,
    owner /proc/ r,
    owner /proc/cmdline r,
    owner /proc/modules r,
    owner /proc/sys/net/bridge/bridge-nf-call-ip6tables rw,
    owner /proc/sys/net/bridge/bridge-nf-call-iptables rw,
    owner /run/docker.sock rw,
    owner /run/xtables.lock rwk,
    owner /serve/portailvmsd/lxc/containers/ rw,
    owner /serve/tmp/ r,
    owner /serve/upload/ r,
    owner /sys/module/** r,
    owner /tmp/portailvms/lock/ rw,
    owner /tmp/portailvms/sock/ rw,
    owner /usr/bin/{b,d}ash mr,

  }

  profile /opt/portailvms/launcher-service/scripts/createqcow2file.sh {
    include <abstractions/base>
    include <abstractions/bash>
    include <abstractions/consoles>
    include <abstractions/nameservice>

    capability chown,
    capability fsetid,

    /lib/x86_64-linux-gnu/ld-*.so mr,
    /opt/portailvms/launcher-service/scripts/createqcow2file.sh r,
    /usr/bin/{b,d}ash ix,
    /usr/bin/chmod mrix,
    /usr/bin/chown mrix,
    /usr/bin/dirname mrix,
    /usr/bin/qemu-img mrix,
    /usr/bin/readlink mrix,
    owner /opt/portailvms/launcher-service/scripts/sources/*.sh r,
    owner /serve/origin/*.qcow2 rwk,
    owner /usr/bin/{b,d}ash r,

  }

  profile /opt/portailvms/launcher-service/scripts/initialise.sh {
    include <abstractions/base>
    include <abstractions/bash>
    include <abstractions/consoles>
    include <abstractions/lxc/start-container>
    include <abstractions/nameservice>

    capability chown,
    capability dac_read_search,
    capability fowner,
    capability fsetid,
    capability net_admin,
    capability net_raw,
    capability sys_module,
    capability syslog,

    network inet raw,

    /lib/x86_64-linux-gnu/ld-*.so mr,
    /opt/portailvms/launcher-service/scripts/initialise.sh r,
    /tmp/portailvms/ w,
    /tmp/portailvms/lock/ rw,
    /tmp/portailvms/sessions/ rw,
    /tmp/portailvms/sessions/* w,
    /tmp/portailvms/sock/ rw,
    /usr/bin/{b,d}ash ix,
    /usr/bin/btrfs mrix,
    /usr/bin/chmod mrix,
    /usr/bin/chown mrix,
    /usr/bin/dirname mrix,
    /usr/bin/{,e}grep mrix,
    /usr/bin/kmod mrix,
    /usr/bin/mkdir mrix,
    /usr/bin/readlink mrix,
    /usr/bin/sleep mrix,
    /usr/sbin/ipset mrix,
    /usr/sbin/sysctl mrix,
    /usr/sbin/xtables-nft-multi mrix,
    owner /etc/modprobe.d/ r,
    owner /etc/modprobe.d/* r,
    owner /opt/portailvms/launcher-service/scripts/sources/*.sh r,
    owner /proc/cmdline r,
    owner /proc/modules r,
    owner /proc/sys/net/bridge/bridge-nf-call-ip6tables rw,
    owner /proc/sys/net/bridge/bridge-nf-call-iptables rw,
    owner /run/xtables.lock rwk,
    owner /sys/module/** r,
    owner /usr/bin/{b,d}ash mr,

  }

  profile /opt/portailvms/launcher-service/scripts/prune.sh {
    include <abstractions/base>
    include <abstractions/bash>
    include <abstractions/consoles>
    include <abstractions/lxc/container-base>
    include <abstractions/lxc/start-container>
    include <abstractions/nameservice>

    /opt/portailvms/launcher-service/scripts/cleanall.sh r,
    /opt/portailvms/launcher-service/scripts/prune.sh r,
    /serve/portailvmsd/lxc/containers/* rw,
    /serve/tmp/* rw,
    /serve/upload/* rw,
    /tmp/portailvms/lock/* rw,
    /tmp/portailvms/sock/* rw,
    /usr/bin/btrfs mrix,
    /usr/bin/{b,d}ash mrix,
    /usr/bin/docker mrix,
    /usr/bin/{,e}grep mrix,
    /usr/bin/[gm]awk mrix,
    /usr/bin/ip mrix,
    /usr/bin/kill mrix,
    /usr/bin/killall PUx,
    /usr/bin/lxc PUx,
    /usr/bin/lxc-{destroy,ls,wait,stop} mrix,
    /usr/bin/ps mrix,
    /usr/bin/readlink mrix,
    /usr/bin/rm mrix,
    /usr/bin/sed mrix,
    /usr/bin/sleep mrix,
    /usr/bin/xargs mrix,
    /usr/sbin/ipset mrix,
    /usr/sbin/xtables-nft-multi mrix,
    owner /etc/iproute2/group r,
    owner /opt/portailvms/launcher-service/scripts/sources/*.sh r,
    owner /proc/ r,
    owner /run/docker.sock rw,
    owner /run/xtables.lock rwk,
    owner /serve/portailvmsd/lxc/containers/ rw,
    owner /serve/tmp/ r,
    owner /serve/upload/ r,
    owner /tmp/portailvms/lock/ rw,
    owner /tmp/portailvms/sock/ rw,

  }

  profile /opt/portailvms/launcher-service/scripts/startarchimulator.sh {
    include <abstractions/authentication>
    include <abstractions/base>
    include <abstractions/bash>
    include <abstractions/consoles>
    include <abstractions/lxc/start-container>
    include <abstractions/nameservice>
    include <abstractions/openssl>
    include <abstractions/python>
    include <abstractions/wutmp>

    signal receive set=kill peer=/opt/portailvms/launcher-service/main.py,
    signal receive set=kill peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/prune.sh,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/prune.sh,
    signal send set=term peer=unconfined,

    ptrace trace peer=unconfined,

    /opt/archimulator/archimulator.py PUx,
    /opt/noVNC/proxyVNC/utils/websockify/run mrix,
    /opt/portailvms/launcher-service/scripts/startarchimulator.sh r,
    /usr/bin/{b,d}ash mrix,
    /usr/bin/dirname mrix,
    /usr/bin/[gm]awk mrix,
    /usr/bin/{,e}grep mrix,
    /usr/bin/kill mrix,
    /usr/bin/printf mrix,
    /usr/bin/ps mrix,
    /usr/bin/python{2.7,3.[0-9]*} mrix,
    /usr/bin/readlink mrix,
    /usr/bin/rm mrix,
    /usr/bin/sleep mrix,
    /usr/bin/su mrix,
    /usr/{,s}bin/runuser mrix,
    /usr/bin/xargs mrix,
    /usr/sbin/ipset mrix,
    /usr/sbin/xtables-nft-multi mrix,

  }

  profile /opt/portailvms/launcher-service/scripts/startdisposablevm.sh {
    include <abstractions/authentication>
    include <abstractions/base>
    include <abstractions/bash>
    include <abstractions/consoles>
    include <abstractions/nameservice>
    include <abstractions/openssl>
    include <abstractions/python>
    include <abstractions/wutmp>

    capability audit_write,
    capability chown,
    capability dac_override,
    capability dac_read_search,
    capability fowner,
    capability fsetid,
    capability kill,
    capability net_admin,
    capability net_raw,
    capability setgid,
    capability setuid,
    capability sys_module,
    capability sys_ptrace,
    capability sys_resource,

    network inet raw,

    signal receive set=kill peer=/opt/portailvms/launcher-service/main.py,
    signal receive set=kill peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/prune.sh,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/cleanall.sh,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/prune.sh,
    signal send set=term peer=unconfined,

    ptrace read peer=/opt/portailvms/launcher-service/main.py,
    ptrace read peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/cleanall.sh,
    ptrace read peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startarchimulator.sh,
    ptrace read peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startephemerallxc.sh,
    ptrace read peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startephemerallxd.sh,
    ptrace read peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startmaster.sh,
    ptrace read peer=/opt/portailvms/web-service/main.py,
    ptrace read peer=/sbin/dhclient,
    ptrace read peer=/usr/sbin/dhcpd,
    ptrace read peer=unconfined,
    ptrace trace peer=/opt/portailvms/launcher-service/main.py,
    ptrace trace peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/cleanall.sh,
    ptrace trace peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startdisposablevm.sh,
    ptrace trace peer=/sbin/dhclient,
    ptrace trace peer=unconfined,

    /etc/mime.types r,
    /opt/noVNC/pem/*.pem r,
    /opt/noVNC/proxyVNC/ r,
    /opt/noVNC/proxyVNC/** r,
    /opt/noVNC/proxyVNC/utils/websockify/run mrix,
    /opt/portailvms/launcher-service/scripts/startdisposablevm.sh r,
    /proc/*/cmdline r,
    /proc/*/stat r,
    /serve/origin/* r,
    /serve/tmp/* rw,
    /sys/fs/cgroup/freezer/user/novnc/*/ r,
    /sys/fs/cgroup/memory/user/novnc/*/ r,
    /tmp/portailvms/sock/* rw,
    /usr/bin/chgrp mrix,
    /usr/bin/chmod mrix,
    /usr/bin/chown mrix,
    /usr/bin/cp mrix,
    /usr/bin/{b,d}ash mrix,
    /usr/bin/dirname mrix,
    /usr/bin/env ix,
    /usr/bin/[gm]awk mrix,
    /usr/bin/{,e}grep mrix,
    /usr/bin/ip mrix,
    /usr/bin/kill mrix,
    /usr/bin/kvm PUx,
    /usr/bin/printf mrix,
    /usr/bin/ps mrix,
    /usr/bin/python{2.7,3.[0-9]*} mrix,
    /usr/bin/qemu-system-{i386,x86_64} PUx,
    /usr/bin/readlink mrix,
    /usr/bin/rm mrix,
    /usr/bin/sleep mrix,
    /usr/bin/socat mrix,
    /usr/bin/su mrix,
    /usr/{,s}bin/runuser mrix,
    /usr/bin/xargs mrix,
    /usr/sbin/bridge mrix,
    /usr/sbin/ipset mrix,
    /usr/sbin/xtables-nft-multi mrix,
    owner /dev/net/tun rw,
    owner /etc/default/locale r,
    owner /etc/environment r,
    owner /etc/security/limits.d/ r,
    owner /opt/noVNC/proxyVNC/utils///*.pyc mrwk,
    owner /opt/noVNC/proxyVNC/utils///__pycache__/ rw,
    owner /opt/noVNC/proxyVNC/utils///__pycache__/* rw,
    owner /opt/portailvms/launcher-service/scripts/sources/*.sh r,
    owner /proc/ r,
    owner /proc/*/* r,
    owner /proc/*/fd/ r,
    owner /proc/sys/kernel/* r,
    owner /proc/uptime r,
    owner /run/xtables.lock rwk,
    owner /sys/fs/cgroup/cpuset/cgroup.clone_children r,
    owner /sys/fs/cgroup/freezer/user/novnc/ r,
    owner /sys/fs/cgroup/freezer/user/novnc/ w,
    owner /sys/fs/cgroup/memory/cgroup.procs w,
    owner /sys/fs/cgroup/memory/user/novnc/ r,
    owner /sys/fs/cgroup/memory/user/novnc/ w,
    owner /sys/fs/cgroup/systemd/cgroup.procs w,
    owner /sys/fs/cgroup/systemd/user/novnc/ r,
    owner /sys/fs/cgroup/systemd/user/novnc/ w,
    owner /sys/fs/cgroup/systemd/user/novnc/*/ w,
    owner /sys/fs/cgroup/systemd/user/novnc/*/cgroup.procs w,
    owner /usr/bin/{b,d}ash r,
    owner /usr/bin/[gm]awk r,

  }

  profile /opt/portailvms/launcher-service/scripts/startephemeraldocker.sh {
    include <abstractions/base>
    include <abstractions/bash>
    include <abstractions/consoles>
    include <abstractions/lxc/start-container>
    include <abstractions/nameservice>
    include <abstractions/openssl>

    capability net_admin,
    capability net_raw,
    capability sys_module,
    capability sys_ptrace,

    network inet raw,

    signal receive set=kill peer=/opt/portailvms/launcher-service/main.py,
    signal receive set=kill peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/prune.sh,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/prune.sh,
    signal send set=term peer=unconfined,

    ptrace trace peer=unconfined,

    /lib/x86_64-linux-gnu/ld-*.so mr,
    /opt/portailvms/launcher-service/scripts/startephemeraldocker.sh r,
    /tmp/portailvms/lock/* rw,
    /usr/bin/{b,d}ash ix,
    /usr/bin/dirname mrix,
    /usr/bin/docker mrix,
    /usr/bin/head mrix,
    /usr/bin/ip mrix,
    /usr/bin/nsenter PUx,
    /usr/bin/[gm]awk mrix,
    /usr/bin/{,e}grep mrix,
    /usr/bin/date mrix,
    /usr/bin/printf mrix,
    /usr/bin/readlink mrix,
    /usr/bin/sleep mrix,
    /usr/bin/touch mrix,
    /usr/bin/xxd mrix,
    /usr/sbin/dhclient mrix,
    /usr/sbin/ipset mrix,
    /usr/sbin/udhcpc mrix,
    /usr/sbin/xtables-nft-multi mrix,
    owner /opt/portailvms/launcher-service/scripts/sources/*.sh r,
    owner /opt/portailvms/launcher-service/scripts/udhcpc/default.script r,
    owner /run/docker.sock rw,
    owner /run/xtables.lock rwk,
    owner /sys/kernel/** r,
    owner /tmp/portailvms/lock/** rw,
    owner /usr/bin/bash mr,
    owner /usr/bin/bash r,

  }

  profile /opt/portailvms/launcher-service/scripts/startephemerallxc.sh {
    include <abstractions/base>
    include <abstractions/bash>
    include <abstractions/consoles>
    include <abstractions/lxc/container-base>
    include <abstractions/lxc/start-container>
    include <abstractions/nameservice>
    include <abstractions/openssl>
    include <abstractions/xdg-open>

    signal receive set=kill peer=/opt/portailvms/launcher-service/main.py,
    signal receive set=kill peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/prune.sh,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/prune.sh,
    signal send set=term peer=unconfined,

    ptrace trace peer=unconfined,

    /lib/x86_64-linux-gnu/ld-*.so mr,
    /opt/portailvms/launcher-service/scripts/startephemerallxc.sh r,
    /serve/portailvmsd/lxc/containers/* rw,
    /serve/portailvmsd/lxc/images/* r,
    /usr/bin/{b,d}ash ix,
    /usr/bin/btrfs mrix,
    /usr/bin/dirname mrix,
    /usr/bin/lxc-{copy,destroy,ls,stop,wait,info,freeze,unfreeze} mrix,
    /usr/bin/lxc-attach PUx,
    /usr/bin/lxc-start PUx,
    /usr/bin/nsenter PUx,
    /usr/bin/ip mrix,
    /usr/bin/[gm]awk mrix,
    /usr/bin/{,e}grep mrix,
    /usr/bin/date mrix,
    /usr/bin/printf mrix,
    /usr/{,local/}bin/ttyd mrix,
    /usr/bin/readlink mrix,
    /usr/bin/sleep mrix,
    /usr/bin/su mrix,
    /usr/{,s}bin/runuser mrix,
    /usr/bin/systemd-run mrix,
    /usr/sbin/ipset mrix,
    /usr/sbin/xtables-nft-multi mrix,
    owner /opt/portailvms/launcher-service/scripts/sources/*.sh r,
    owner /run/xtables.lock rwk,
    owner /serve/portailvmsd/lxc/containers/ rw,
    owner /serve/portailvmsd/lxc/images/ r,
    owner /usr/bin/{b,d}ash mr,

  }

  profile /opt/portailvms/launcher-service/scripts/startephemerallxd.sh {
    include <abstractions/base>
    include <abstractions/bash>
    include <abstractions/consoles>
    include <abstractions/lxc/container-base>
    include <abstractions/lxc/start-container>
    include <abstractions/nameservice>
    include <abstractions/openssl>

    signal receive set=kill peer=/opt/portailvms/launcher-service/main.py,
    signal receive set=kill peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/prune.sh,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/prune.sh,
    signal send set=term peer=unconfined,

    ptrace trace peer=unconfined,

    /lib/x86_64-linux-gnu/ld-*.so mr,
    /opt/portailvms/launcher-service/scripts/startephemerallxd.sh r,
    /usr/bin/{b,d}ash ix,
    /usr/bin/dirname mrix,
    /usr/bin/lxc PUx,
    /usr/bin/snap PUx,
    /usr/bin/printf mrix,
    /usr/bin/readlink mrix,
    /usr/bin/sleep mrix,
    /usr/bin/date mrix,
    /usr/sbin/ipset mrix,
    /usr/sbin/xtables-nft-multi mrix,
    owner /opt/portailvms/launcher-service/scripts/sources/*.sh r,
    owner /run/xtables.lock rwk,
    owner /usr/bin/{b,d}ash mr,

  }

  profile /opt/portailvms/launcher-service/scripts/startmaster.sh {
    include <abstractions/authentication>
    include <abstractions/base>
    include <abstractions/bash>
    include <abstractions/consoles>
    include <abstractions/lxc/container-base>
    include <abstractions/lxc/start-container>
    include <abstractions/nameservice>
    include <abstractions/openssl>
    include <abstractions/python>
    include <abstractions/wutmp>

    capability audit_write,
    capability chown,
    capability dac_override,
    capability dac_read_search,
    capability kill,
    capability net_admin,
    capability net_raw,
    capability setgid,
    capability setuid,
    capability sys_module,
    capability sys_ptrace,
    capability sys_resource,

    network inet raw,

    signal receive set=kill peer=/opt/portailvms/launcher-service/main.py,
    signal receive set=kill peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/prune.sh,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/prune.sh,
    signal send set=term peer=unconfined,

    ptrace read peer=/opt/portailvms/launcher-service/main.py,
    ptrace read peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startdisposablevm.sh,
    ptrace read peer=/opt/portailvms/web-service/main.py,
    ptrace read peer=/sbin/dhclient,
    ptrace read peer=/usr/sbin/dhcpd,
    ptrace read peer=unconfined,
    ptrace trace peer=/opt/portailvms/launcher-service/main.py,
    ptrace trace peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startdisposablevm.sh,
    ptrace trace peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/startmaster.sh,
    ptrace trace peer=/opt/portailvms/web-service/main.py,
    ptrace trace peer=/sbin/dhclient,
    ptrace trace peer=/usr/sbin/dhcpd,
    ptrace trace peer=unconfined,

    /etc/mime.types r,
    /opt/noVNC/pem/*.pem r,
    /opt/noVNC/proxyVNC/ r,
    /opt/noVNC/proxyVNC/** r,
    /opt/noVNC/proxyVNC/utils/websockify/run mrix,
    /opt/portailvms/launcher-service/scripts/startmaster.sh r,
    /proc/*/cmdline r,
    /proc/*/stat r,
    /serve/origin/* r,
    /serve/tmp/* rw,
    /tmp/portailvms/sock/* rw,
    /usr/bin/{b,d}ash ix,
    /usr/bin/chgrp mrix,
    /usr/bin/chmod mrix,
    /usr/bin/chown mrix,
    /usr/bin/cp mrix,
    /usr/bin/{b,d}ash mrix,
    /usr/bin/dirname mrix,
    /usr/bin/env ix,
    /usr/bin/[gm]awk mrix,
    /usr/bin/{,e}grep mrix,
    /usr/bin/ip mrix,
    /usr/bin/kill mrix,
    /usr/bin/kvm PUx,
    /usr/bin/printf mrix,
    /usr/bin/ps mrix,
    /usr/bin/python{2.7,3.[0-9]*} mrix,
    /usr/bin/qemu-system-{i386,x86_64} PUx,
    /usr/bin/readlink mrix,
    /usr/bin/rm mrix,
    /usr/bin/sleep mrix,
    /usr/bin/socat mrix,
    /usr/bin/su mrix,
    /usr/{,s}bin/runuser mrix,
    /usr/bin/xargs mrix,
    /usr/sbin/bridge mrix,
    /usr/sbin/ipset mrix,
    /usr/sbin/xtables-nft-multi mrix,
    owner /dev/net/tun rw,
    owner /etc/default/locale r,
    owner /etc/environment r,
    owner /etc/iproute2/group r,
    owner /etc/security/limits.d/ r,
    owner /opt/noVNC/proxyVNC/utils///*.pyc mrwk,
    owner /opt/noVNC/proxyVNC/utils///__pycache__/ rw,
    owner /opt/noVNC/proxyVNC/utils///__pycache__/* rw,
    owner /opt/portailvms/launcher-service/scripts/sources/*.sh r,
    owner /proc/ r,
    owner /proc/*/cgroup r,
    owner /proc/*/fd/ r,
    owner /proc/*/limits r,
    owner /proc/*/loginuid r,
    owner /proc/*/mountinfo r,
    owner /proc/sys/kernel/osrelease r,
    owner /proc/sys/kernel/pid_max r,
    owner /proc/tty/drivers r,
    owner /proc/uptime r,
    owner /run/xtables.lock rwk,
    owner /usr/bin/{b,d}ash r,
    owner /usr/bin/[gm]awk r,

  }

  profile /opt/portailvms/launcher-service/scripts/startmasterlxc.sh {
    include <abstractions/base>
    include <abstractions/bash>
    include <abstractions/consoles>
    include <abstractions/lxc/container-base>
    include <abstractions/lxc/start-container>
    include <abstractions/nameservice>
    include <abstractions/openssl>
    include <abstractions/xdg-open>

    signal receive set=kill peer=/opt/portailvms/launcher-service/main.py,
    signal receive set=kill peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/prune.sh,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py,
    signal receive set=term peer=/opt/portailvms/launcher-service/main.py///opt/portailvms/launcher-service/scripts/prune.sh,
    signal send set=term peer=unconfined,

    ptrace trace peer=unconfined,

    /lib/x86_64-linux-gnu/ld-*.so mr,
    /opt/portailvms/launcher-service/scripts/startmasterlxc.sh r,
    /serve/portailvmsd/lxc/containers/* rw,
    /serve/portailvmsd/lxc/images/* r,
    /usr/bin/{b,d}ash ix,
    /usr/bin/btrfs mrix,
    /usr/bin/dirname mrix,
    /usr/bin/lxc-{copy,destroy,ls,stop,wait,info,freeze,unfreeze} mrix,
    /usr/bin/lxc-attach PUx,
    /usr/bin/lxc-start PUx,
    /usr/bin/nsenter PUx,
    /usr/bin/ip mrix,
    /usr/bin/[gm]awk mrix,
    /usr/bin/{,e}grep mrix,
    /usr/bin/date mrix,
    /usr/bin/printf mrix,
    /usr/{,local/}bin/ttyd mrix,
    /usr/bin/readlink mrix,
    /usr/bin/sleep mrix,
    /usr/bin/su mrix,
    /usr/{,s}bin/runuser mrix,
    /usr/bin/systemd-run mrix,
    /usr/sbin/ipset mrix,
    /usr/sbin/xtables-nft-multi mrix,
    owner /opt/portailvms/launcher-service/scripts/sources/*.sh r,
    owner /run/xtables.lock rwk,
    owner /serve/portailvmsd/lxc/containers/ rw,
    owner /serve/portailvmsd/lxc/images/ rw,
    owner /usr/bin/{b,d}ash mr,

  }
}
