# Last Modified: Thu Jun 24 14:20:40 2021
abi <abi/3.0>,

include <tunables/global>

/opt/portailvms/web-service/main.py {
  include <abstractions/authentication>
  include <abstractions/base>
  include <abstractions/lxc/container-base>
  include <abstractions/nameservice>
  include <abstractions/openssl>
  include <abstractions/python>
  include <abstractions/user-tmp>

  signal send set=int peer=lxc-container-default-cgns,

  ptrace trace peer=unconfined,

  /usr/bin/ping Cx,
  /etc/portailvms/main.conf r,
  /etc/mime.types r,
  /lib/x86_64-linux-gnu/ld-*.so mr,
  /usr/bin/x86_64-linux-gnu-* mrix,
  /usr/lib/gcc/x86_64-linux-gnu/*/collect2 mrix,
  /usr/sbin/ldconfig mrix,
  /usr/sbin/ldconfig.real mrix,
  /opt/portailvms/web-service/ r,
  /opt/portailvms/web-service/** r,
  /opt/portailvms/common/** r,
  /opt/portailvms/web-service/__pycache__/* rw,
  /opt/portailvms/web-service/external/__pycache__/* rw,
  /opt/portailvms/web-service/include/__pycache__/* rw,
  /opt/portailvms/common/__pycache__/* rw,

  /serve/ISO/ r,
  /serve/upload/* rw,
  /tmp/portailvms/sessions/* rw,
  /tmp/portailvms/sock/* rw,
  /usr/bin/lxc mrix,
  /{usr/,}lib{,32,64}/** mr,
  owner /etc/portailvms/db/ r,
  owner /etc/portailvms/db/** rwk,
  owner /opt/portailvms/web-service/__pycache__/ rw,
  owner /opt/portailvms/web-service/external/__pycache__/ rw,
  owner /opt/portailvms/web-service/include/__pycache__/ rw,
  owner /opt/portailvms/common/__pycache__/* rw,
  owner /var/log/portailvmsd/* rw,
  owner /serve/upload/ r,


  profile /usr/bin/ping {
    include <abstractions/base>

    capability net_raw,

    network inet dgram,
    network inet raw,
    network inet6 dgram,
    network inet6 raw,

    /usr/bin/ping mr,
    /lib/x86_64-linux-gnu/ld-*.so mr,

  }

  ^/usr/bin/lxc {
    include <abstractions/lxc/container-base>

  }
}
