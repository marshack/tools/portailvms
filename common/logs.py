#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)


import logging
import logging.handlers
import threading, sys


class Logs(threading.Thread):
    def __init__(self, service, logfilename, maxbytes, backupcount):
        threading.Thread.__init__(self)
        self.logger = logging.getLogger(service)
        self.logger.setLevel(logging.DEBUG)
        self.lock=threading.Lock()
        self.logfilename=logfilename
        self.last_loginfo=None
        self.last_logwarning=None

        # custom file log
        if self.logfilename:
            formattercustom = logging.Formatter('%(asctime)s %(name)s: [%(levelname)s] %(message)s')
            try:
                handlerrotate = logging.handlers.RotatingFileHandler(self.logfilename, \
                                     maxBytes=maxbytes, backupCount=backupcount)
                handlerrotate.setFormatter(formattercustom)
                self.logger.addHandler(handlerrotate)
            except:
                print('Impossible to write to {} !'.format(logfilename))

    def logInfo(self, msg):
        self.lock.acquire()
        try:
            if self.logfilename:
                msg=msg[0:200] # cut ...
                if self.last_loginfo!=msg: # 'remove' duplicate
                    self.logger.info(msg)
                    self.last_loginfo=msg
        except:
            pass
        self.lock.release()

    def logWarning(self, msg):
        self.lock.acquire()
        try:
            if self.logfilename:
                msg=msg[0:200] # cut ...
                if self.last_logwarning!=msg: # 'remove' duplicate
                    self.logger.warning(msg)
                    self.last_logwarning=msg
            else: sys.stderr.write("{}\n".format(msg))
        except:
            pass
        self.lock.release()


if __name__ == "__main__":
    filename="test.log"
    log=Logs("test", filename, 10000, 4)
    log.logInfo('An information')
    log.logWarning('A warning')
    with open(filename, 'r') as fd:
        print(fd.read())
    import os
    os.remove(filename)
