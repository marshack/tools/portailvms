#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import threading, time

class Timer():
    single = 0
    timer = 1

class Schedule(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.eventTerminated = threading.Event()
        self.queue_trigger=[]

    def singleShot(self, sec, callback):
        self.queue_trigger.append((Timer.single, time.time() + sec, callback, None))

    def timer(self, interval, callback):
        self.queue_trigger.append((Timer.timer, time.time() + interval, callback, interval))

    def stop(self):
            self.eventTerminated.set()

    def run(self):
        while not self.eventTerminated.is_set():
            todo_remove=[]
            for q in self.queue_trigger:
                typ, time_trig, callback, interval=q
                if time_trig < time.time():
                    if callable(callback):
                        callback()
                    todo_remove.append(q)
                    if typ==Timer.timer:
                        # timer
                        self.queue_trigger.append((Timer.timer, time.time() + interval, \
                           callback, interval))
            # purge
            for c in todo_remove:
                self.queue_trigger.remove(c)

            time.sleep(0.2)

if __name__ == "__main__":
    import signal
    def signal_handler(signal, frame):
        schedule.stop()

    def callback_singleshot():
        print("callback_singleshot")
    def callback_timer():
        print("callback_timer")
        
    schedule=Schedule()
    
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    schedule.singleShot(11, callback_singleshot)
    schedule.timer(3, callback_timer)
    schedule.start()

