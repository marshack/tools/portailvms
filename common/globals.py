#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, os

__version__="1.3.4"

def getConfigValue(mConfig, section, key, default="", typ=str):
    error=None
    if mConfig.has_option(section, key):
        if typ==str:
            return mConfig.get(section, key)
        elif typ==int:
            try:
                return int(mConfig.get(section, key))
            except:
                error="{}.{} (int required)" % (section, key)
        elif typ==bool:
            try:
                return mConfig.getboolean(section, key)
            except:
                error="{}.{} (boolean required)" % (section, key)
        elif typ==list:
            string=config.get(section, key)
            if not ',' in string:
                string +=','
            tmplist=string.strip().split(',')
            newlist=[]
            for arg in tmplist:
                if arg:
                    newlist.append(arg.strip())
            return newlist
    if error:
        sys.stderr.write("Error - Unable to retrieve configuration data : {}\n".format(error))
        sys.exit(1)
    return default

def createDirIfNotExist(path):
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except Exception as e:
            sys.stderr.write("Warning - {}\n".format(e))

