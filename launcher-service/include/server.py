#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, os, re
import signal, select
import hmac, hashlib, pickle
import pwd, grp

from multiprocessing.connection import Listener
from multiprocessing import ProcessError



class Server:
    def __init__(self, bind, secret, debug_enabled=False):
        self.bind=bind
        self.secret=secret
        self.listener=None
        self.running=False
        self.currentConn=None
        self.callbackloginfo=None
        self.callbacklogwarning=None
        self.callbackconnected=None
        self.callbackdisconnected=None
        self.callbackrcvdata=None
        self.debug_enabled=debug_enabled
        self.verbose=True if os.getenv('PORTAILVMS_DEBUG_VERBOSE') else False

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def info(self, msg):
        self.debug("Info - {}".format(msg))
        self.logInfo(msg)

    def warning(self, msg):
        sys.stderr.write("Warning - {}\n".format(msg))
        self.logWarning(msg)

    def logInfo(self, msg):
        if callable(self.callbackloginfo): self.callbackloginfo(msg)

    def logWarning(self, msg):
        if callable(self.callbacklogwarning): self.callbacklogwarning(msg)

    def setCallBackRcvData(self, callbackrcvdata):
        self.callbackrcvdata=callbackrcvdata

    def setCallBackLogInfo(self, callbackloginfo):
        self.callbackloginfo=callbackloginfo

    def setCallBackLogWarning(self, callbacklogwarning):
        self.callbacklogwarning=callbacklogwarning

    def setCallBackConnected(self, callbackconnected):
        self.callbackconnected=callbackconnected

    def setCallBackDisconnected(self, callbackdisconnected):
        self.callbackdisconnected=callbackdisconnected

    def getSignature(self, data):
        return hmac.new(self.secret, data, hashlib.sha1).digest()


    def fix_right(self, _struct):
        filename=_struct[0]
        user=_struct[1]
        try:
            uid = pwd.getpwnam(user).pw_uid
        except:
            self.warning("User {} doesn't exist".format(user))
            return
        if len(_struct)>2:
            group=_struct[2]
        else:
            self.warning("Group missing")
            return
        try:
            gid = grp.getgrnam(group).gr_gid
        except:
            self.warning("Group {} doesn't exist".format(group))
            return

        try:
            os.chown(filename, uid, gid)
        except:
            self.warning("Impossible to change owner")
        if len(_struct)>3:
            mod=_struct[3]
            try:
                os.chmod(filename, int(mod, base=8))
            except:
                self.warning("Impossible to chmod : {}".format(mod))


    def sendData(self, data):
        if self.verbose:
            self.debug("Server::sendData '{}'".format(data))
        if not self.running or not self.currentConn:
            return False
        try:
            stream=pickle.dumps(data)
            sig=hmac.new(self.secret, stream, hashlib.sha1).digest()
            self.currentConn.send((stream, sig))
        except Exception as e:
            self.warning("{}".format(e))
            return False
        return True

    def unpackData(self, data):
        if self.verbose:
            self.debug("Server::unpackData")
        if isinstance(data, tuple) and len(data)==2:
            stream, rcv_hmac=data
            comp_hash=self.getSignature(stream)
            if comp_hash!=rcv_hmac:
                raise Exception('bad signature')
            return pickle.loads(stream)

    def stop(self):
        if not self.running:
            return
        self.info("Stop listener, please wait ...")
        self.running=False
        # stop server
        if self.listener:
            try:
                self.listener.close()
            except:
                pass
            self.listener=None
        self.info("Listener stopped")

    def run(self):
        family='AF_UNIX' # default
        tmp_struct=self.bind.split(':')
        if re.match(r'^\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}\:\d+$', self.bind):
            family='AF_INET'
            port=6542 if len(tmp_struct)==1 else int(tmp_struct[1])
            self.bind=(tmp_struct[0], port)
        else:
            self.bind=tmp_struct[0]

        try:
            self.listener = Listener(self.bind, family=family, authkey=self.secret)
            self.running=True
            self.info("Listener started")
            if family=='AF_UNIX' and len(tmp_struct)>1:
                self.fix_right(tmp_struct)
        except Exception as e:
            self.warning("An error occured : {}".format(e))
            return

        while self.running:
            try:
                self.currentConn = self.listener.accept()
                self.info('connection accepted ({} -> {})'.format(self.listener.last_accepted, self.listener.address))
                if callable(self.callbackconnected): self.callbackconnected()
            except ProcessError as e:
                self.warning("Listener error: {}".format(e))
            except:
                pass

            r = [self.currentConn]; w = []; x = []
            while self.running and self.currentConn:
                r, w, x = select.select(r, w, x, 0.8)
                if self.currentConn in r:
                    try:
                        msg = self.currentConn.recv()
                        data=self.unpackData(msg)
                        if self.verbose:
                            self.debug("rcv data : {}".format(data))
                        if callable(self.callbackrcvdata): self.callbackrcvdata(data)
                    except EOFError:
                        self.info("Closed connection")
                        self.currentConn=None
                        break
                    except Exception as e:
                        self.warning("An error occured : {}".format(e))
                r = [self.currentConn]

            if callable(self.callbackdisconnected): self.callbackdisconnected()

        self.stop()

if __name__ == "__main__":
    def callback_data_rcv(data):
        print(f"callback_data_rcv :\n{data}")
        s.sendData({"OK": True})

    def signal_handler(signal, frame):
        s.stop()

    # Examples :
    # bind = "/tmp/tmpsocket.sock:username:group:660"
    # bind = "127.0.0.1:6542"
    # bind = "0.0.0.0:6542"
    bind = "/tmp/tmpsocket.sock"
    secret=b'\x96\x8d\r\xbd\xbc.d\xaaYblDK\xee\xd9\xc8\tD)\x083l\xacf\x87\x0b\xcb#\xf9e__'
    s=Server(bind, secret, debug_enabled=True)
    s.setCallBackRcvData(callback_data_rcv)
    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGQUIT, signal_handler)

    s.run()
