#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import threading, subprocess
import time

class ProcessInstance(threading.Thread):
    def __init__(self, target, uuid, cmd, callback_finished=None, debug_enabled=False):
        threading.Thread.__init__(self)
        self.target=target
        self.uuid=uuid
        self.cmd=cmd
        self.debug_enabled=debug_enabled
        self.proc=None
        self.pid=None
        self.callback_finished=callback_finished

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def signalTerminated(self, returncode, stdout, stderr):
        self.debug("ProcessInstance:signalTerminated")
        if callable(self.callback_finished):
            self.callback_finished(self.target, self.uuid, returncode, stdout, stderr)

    def execCommand(self, cmd):
        self.debug('ProcessInstance:execCommand - {}'.format(" ".join(cmd)))
        try:
            self.proc=subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except Exception as e:
            return 1, (), (str(e))
        self.pid=self.proc.pid
        returncode=self.proc.wait()
        stderr=self.proc.stderr.read().decode().split('\n')
        stdout=self.proc.stdout.read().decode().split('\n')
        return returncode, stdout, stderr

    def getPid(self):
        return self.pid

    def terminate(self):
        if self.proc:
            self.debug('ProcessInstance:terminate')
            if self.proc:
                self.debug("try to terminate pid {} ({})".format(self.proc.pid, " ".join(self.proc.args)))
                try:
                    self.proc.terminate()
                    self.proc.wait(0.4)
                except:
                    pass

    def kill(self):
        if self.proc:
            self.debug('ProcessInstance:kill')
            if self.proc.returncode==None:
                self.debug("is alive, kill pid {} ({})".format(self.proc.pid, " ".join(self.proc.args)))
                try:
                    self.proc.kill()
                    self.proc.wait(0.2)
                except:
                    pass

    def stop(self):
        if self.proc:
            if self.proc.returncode==None:
                self.terminate()
            for _ in range(100):
                time.sleep(0.1)
                if self.proc.returncode!=None:
                    break
            if self.proc.returncode==None:
                self.kill()

    def run(self):
        returncode, stdout, stderr=self.execCommand(self.cmd)
        self.signalTerminated(returncode, stdout, stderr)

if __name__ == '__main__':
    def callback_finished(target, uuid, returncode, stdout, stderr):
        print("callback_finished", (target, uuid, returncode, stdout, stderr))

    pi=ProcessInstance("my_target", "my_uuid", ('sleep','2'), callback_finished=callback_finished, debug_enabled=True)
    pi.start()
    pi.join()
