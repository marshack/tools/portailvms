#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import threading, os, re
import subprocess, shutil
import magic, hashlib
try:
    from pylxd import Client as pyClient
except:
    pyClient=None
try:
    import docker
except:
    docker=None

try: 
    from include.process_instance import ProcessInstance
except:
    from process_instance import ProcessInstance

class Route():
    def __init__(self, config, callback_proc_finished=None, debug_enabled=False):
        self.origin_dir=config['origin_dir']
        self.tmp_target_dir=config['tmp_target_dir']
        self.iso_dir=config['iso_dir']
        self.archi_templates_dir=config['archi_templates_dir']
        self.archi_kvm_dir=config['archi_kvm_dir']
        self.upload_dir=config['upload_dir']
        self.lxc_dir=config['lxc_dir']
        self.callback_proc_finished=callback_proc_finished
        self.debug_enabled=debug_enabled
        self.lock_collection=threading.Lock()
        self.maintenance=False
        self.collection={}
        # key : command name
        # value :
        #    - function (callable), or shell command (execute)
        #    - boolean : this command requires (or not) that the maintenance mode be activated during its execution
        #    - argument list (optional)
        self.commands_dic={
            "sleep":        ("sleep", False, ("wait", )), # FOR TEST ONLY
            "cleanall":     (self.targetCleanall, True, None),
            "killfromuuid": (self.targetKillUuid, False, None),
            "startdisposablevm": ("scripts/startdisposablevm.sh", False, ("name", "id", "tmp_target_dir", "ram", "cpu", "source", "uuid", "from_address", "to_address", "net_virtio", "scsi_virtio", "with_vnc", "vnc_password", "guest_type", "keyboard")),
            "startmaster":       ("scripts/startmaster.sh", False, ("name", "id", "ram", "cpu", "source", "uuid", "from_address", "to_address", "net_virtio", "scsi_virtio", "vnc_password", "guest_type", "keyboard")),
            "startephemerallxc": ("scripts/startephemerallxc.sh", False, ("name", "id", "lxc_path", "ram", "cpu", "disk", "image", "uuid", "from_address", "to_address", "with_ttyd", "ttyd_password")),
            "startmasterlxc":    ("scripts/startmasterlxc.sh", False, ("name", "id", "lxc_path", "ram", "cpu", "disk", "image", "uuid", "from_address", "to_address", "ttyd_password")),
            "startephemerallxd": ("scripts/startephemerallxd.sh", False, ("name", "id", "ram", "cpu", "disk", "image", "uuid", "from_address", "to_address", "with_ttyd", "ttyd_password")),
            "startephemeraldocker": ("scripts/startephemeraldocker.sh", False, ("name", "id", "ram", "cpu", "disk", "image", "uuid", "from_address", "to_address", "with_ttyd", "ttyd_password")),
            "startarchimulator": ("scripts/startarchimulator.sh", False, ("name", "id", "template", "uuid", "from_address", "to_address", "with_vnc", "vnc_password")),
            "createqcow2":  (self.targetCreateQcow2File, False, ("name", "size")),
            "moveuploaded": (self.moveUploadedFile, False, ("type", "name", "force")),
        }

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def stop(self):
        self.debug("Route:stop")
        self.lock_collection.acquire()
        collection=self.collection.copy()
        self.lock_collection.release()
        for uuid, pi in collection.items():
            if pi.is_alive():
                pi.stop()
            pi.join() 

    def targetKillUuid(self, target, uuid, _=None):
        self.debug("Route:targetKillUuid - uuid {}".format(uuid))
        self.lock_collection.acquire()
        if uuid in self.collection:
            pi=self.collection[uuid]
        self.lock_collection.release()
        try:
            pi.stop()
        except:
            pass

    def targetCleanall(self, target, uuid, _=None):
        self.debug("Route:targetCleanall")
        self.startProcessInstance(target, uuid, ("scripts/cleanall.sh", self.tmp_target_dir, self.upload_dir, self.lxc_dir))

    def targetCreateQcow2File(self, target, uuid, values):
        self.debug("Route:targetCreateQcow2File")
        if isinstance(values, list) and len(values)==2:
            name="{}/{}".format(self.origin_dir, values[0])
            try:
                size=int(values[1])
            except:
                self.callback_finished(target, uuid, 1)
                return
            self.debug("Route:targetCreateQcow2File - name={}, size={}".format(name, size))
            self.startProcessInstance(target, uuid, ("scripts/createqcow2file.sh", name, str(size)))

    def moveUploadedFile(self, target, uuid, values):
        self.debug("Route:moveUploadedFile")
        force=False
        if isinstance(values, list) and len(values)==3:
            tpe=values[0]
            filename=values[1]
            source="{}/{}".format(self.upload_dir, filename)
            if not os.path.isfile(source):
                self.callback_finished(target, uuid, 1, stderr=[f"file {source} doesn't exist"])
                return
            else:
                try:
                    force=bool(int(values[2]))
                except:
                    pass

            validate= {
                'qcow2' :      { 'mime' : 'application/(octet-stream|x-qemu-disk)',
                                 'endswith' : 'qcow2',
                                 'target_dir' : self.origin_dir,
                                 'owner': "root:portailvmsd",
                                 'mod': 'u=rw,g=r,o='
                                },
                'iso' :        { 'mime' : 'application/(octet-stream|x-cd-image|x-iso9660-image)',
                                 'endswith' : 'iso',
                                 'target_dir' : self.iso_dir,
                                 'owner': "root:portailvmsd",
                                 'mod': 'u=rw,g=r,o=r'
                                },
                'yaml_archi' : { 'mime' : 'application/x-yaml|text/plain',
                                 'endswith' : 'yaml|yml',
                                 'target_dir' : self.archi_templates_dir,
                                 'owner': "root:root",
                                 'mod': 'u=rw,g=r,o='},
                'yaml_kvm' :   { 'mime' : 'application/x-yaml|text/plain',
                                 'endswith' : 'yaml|yml',
                                 'target_dir' : self.archi_kvm_dir,
                                 'owner': "root:root",
                                 'mod': 'u=rw,g=r,o='
                                },
                'lxc_image' :   { 'mime' : 'application/gzip',
                                 'endswith' : 'tar.gz',
                                },
                'lxd_image' :   { 'mime' : 'application/gzip',
                                 'endswith' : 'tar.gz',
                                },
                'docker_image': { 'mime' : 'application/x-tar',
                                 'endswith' : 'tar',
                                },
            }
            m=magic.open(magic.MAGIC_MIME)
            m.load()
            mime=m.file(source)
            if tpe in validate and re.match('^({})$'.format(validate[tpe]['mime']),mime , re.IGNORECASE) and re.match(r'.*\.({})$'.format(validate[tpe]['endswith']),source , re.IGNORECASE):
                if tpe=="lxc_image":
                    ret, msg=self.importLxcImage(filename, force)
                    if ret!=0:
                        self.callback_finished(target, uuid, 1, stderr=[msg])
                    else:
                        self.callback_finished(target, uuid, 0)
                    return
                if tpe=="lxd_image":
                    ret, msg=self.importLxdImage(filename, force)
                    if ret!=0:
                        self.callback_finished(target, uuid, 1, stderr=[msg])
                    else:
                        self.callback_finished(target, uuid, 0)
                    return
                elif tpe=="docker_image":
                    ret, msg=self.importDockerImage(filename, force)
                    if ret!=0:
                        self.callback_finished(target, uuid, 1, stderr=[msg])
                    else:
                        self.callback_finished(target, uuid, 0)
                    return
                # other (file to move to target)
                target_dir=validate[tpe]['target_dir']
                destination="{}/{}".format(target_dir, filename)
                owner=validate[tpe]['owner']
                mod=validate[tpe]['mod']
                if not force and os.path.isfile(destination):
                    self.callback_finished(target, uuid, 1, stderr=[f"file {filename} already exist in {target_dir}"])
                    return
                try:
                    if os.path.isfile(destination):
                        os.remove(destination)
                    shutil.move(source, target_dir)
                    r1=subprocess.call(('chown', owner, destination))
                    r2=subprocess.call(('chmod', mod, destination))
                    if r1 or r2:
                        raise Exception("Impossible to change owner or file mode bits for this file. Please contact an Administrator")
                except Exception as e:
                    self.callback_finished(target, uuid, 1, stderr=[str(e)])
                    return
                # all is ok
                self.callback_finished(target, uuid, 0)
                return
        self.callback_finished(target, uuid, 1, stderr=["An error occured"])

    def importLxcImage(self, filename, force=False):
        def sanitize(_input):
            return re.sub(r'[^a-zA-Z0-9\_\-\.]', '_', _input)

        source=f"{self.upload_dir}/{filename}"
        target=f"{self.lxc_dir}/images"
        # check archive format and if already exists
        try:
            cmd=("tar", "-tzf", source, "-C", target, "--wildcards", "*/config")
            proc=subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            self.debug(proc.args)
            exitcode=proc.wait()
            if exitcode!=0:
                raise Exception(f"Impossible to read {source}")
            found_config=None
            image_name=None
            for line in proc.stdout.readlines():
                m=re.match(r'^(.+)/config$', line.strip().decode())
                if m:
                    res=m.group(1)
                    if '/' not in res:
                        found_config=True
                        image_name=res
                        break
            if not found_config:
                raise Exception("Bad format archive (missing config file)")
            image_name_origin=image_name
            image_name=sanitize(image_name)
            path_image=os.path.abspath(f"{target}/{image_name}")
            rootfs=f"{path_image}/rootfs"
            conf_file=f"{path_image}/config"
            if os.path.isdir(path_image):
                if force:
                    # if force enable and image already exists, remove first
                    shutil.rmtree(path_image)
                else:
                    raise Exception("Image already exists")
            # extract
            cmd=("tar", "-xzf", source, "-C", target)
            proc=subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            self.debug(proc.args)
            exitcode=proc.wait()
            if exitcode!=0:
                raise Exception(f"Impossible to extract {source} -> {target}")
            os.remove(source)
            if image_name_origin!=image_name:
                subprocess.run(['mv', f"{target}/{image_name_origin}", f"{target}/{image_name}"])
            # update configuration
            exitcode=subprocess.run(['sed', '-r', '-i', f's#lxc.rootfs.path\\s*=.*#lxc.rootfs.path = btrfs:{rootfs}#g', conf_file]).returncode
            if exitcode!=0:
                raise Exception("Impossible to update configuration (lxc.rootfs.path)")
            exitcode=subprocess.run(['sed', '-r', '-i', 's#lxc.net.0.link\\s*=.*#lxc.net.0.link = br0#g', conf_file]).returncode
            if exitcode!=0:
                raise Exception("Impossible to update configuration (lxc.net.0.link)")
            # convert directory to btrfs subvolume
            try:
                subprocess.run(['mv', rootfs, f"{rootfs}.old"], check=True)
                subprocess.run(['btrfs', 'sub', 'create', rootfs], check=True)
                subprocess.run(['rsync', '-a', '-S', '-H', '--remove-source-files', f"{rootfs}.old/", f"{rootfs}/"], check=True)
                shutil.rmtree(f"{rootfs}.old")
            except Exception as err:
                raise Exception("Impossible to create a rootfs subvolume : {}".format(err))
            # fix right
            os.chown(rootfs, 100000, 100000)
            os.chmod(path_image, 0o755)
            os.chmod(conf_file, 0o644)
            account="portailvmsd"
            shutil.chown(path_image, account, account)
            shutil.chown(conf_file, account, account)
            return 0, ""
        except Exception as err:
            return 1, str(err)

    def importLxdImage(self, filename, force=False):
        if not pyClient:
            return 1, "An error occured"
        m=re.match(r"^(.*)\.tar\.gz$", filename)
        if m:
            alias=m.group(1)
            source="{}/{}".format(self.upload_dir, filename)
            client = pyClient()
            try:
                image_data = open(source, 'rb').read()
                if force:
                    hash=hashlib.sha256(image_data)
                    image=None
                    try:
                        image=client.images.get(hash.hexdigest())
                    except:
                        pass
                    if image:
                        image.delete()
            except Exception as err:
                return 1, str(err)
            try:
                image = client.images.create(image_data, public=False, wait=True)
                image.add_alias(alias, "")
                os.remove(source)
                return 0, ""
            except Exception as err:
                return 1, str(err)
        return 1, "An error occured"

    def importDockerImage(self, filename, force=False):
        if not docker:
            return 1, "An error occured"
        source="{}/{}".format(self.upload_dir, filename)
        client=docker.from_env()
        try:
            image_data = open(source, 'rb').read()
            client.images.load(image_data)
            os.remove(source)
            return 0, ""
        except Exception as err:
            return 1, str(err)

    def isCommandRequireMaintenance(self, target):
        if target in self.commands_dic:
            return self.commands_dic[target][1]
        return False

    def getCmdbyTarget(self, target, meta):
        cmd=None
        if target in self.commands_dic:
            arg0=self.commands_dic[target][0]
            cmd=[arg0]
            args=self.commands_dic[target][2]
            if args:
                for arg in args:
                    if arg in meta:
                        cmd.append(meta[arg])
                    else:
                        self.debug("Arg Missing (target '{}'): {}".format(target, arg))
                        return None
        else:
            self.debug("Unknown target key : {}".format(target))
            return None
        return cmd

    def startProcessInstance(self, target, uuid, cmd):
        pi=ProcessInstance(target, uuid, cmd, callback_finished=self.callback_finished, debug_enabled=self.debug_enabled)
        self.lock_collection.acquire()
        self.collection[uuid]=pi
        self.lock_collection.release()
        pi.start()

    def launch(self, data):
        target=None
        uuid=None
        if "target" in data: target=data.pop("target")
        if "uuid" in data: uuid=data['uuid']
        if target and uuid:
            self.debug("Route:launch - target: {}, uuid: {}".format(target, uuid))
            if self.maintenance:
                self.debug("Reject : maitenance mode is On !")
                if callable(self.callback_proc_finished): self.callback_proc_finished({ 'success':False , 'uuid': uuid, 'origin': target, 'running':False, 'maintenance': True})
                return

            if self.isCommandRequireMaintenance(target):
                self.maintenance=True

            cmd=self.getCmdbyTarget(target, data)
            
            if cmd:
                if callable(cmd[0]):
                    # call method
                    cmd[0](target, uuid, cmd[1:])
                else:
                    # run process
                    self.startProcessInstance(target, uuid, cmd)


    def callback_finished(self, origin, uuid, returncode, stdout=[], stderr=[]):
        self.debug("Route:callback_finished - origin: {}, uuid: {}, returncode: {}".format(origin, uuid, returncode))
        if self.maintenance and self.isCommandRequireMaintenance(origin):
            self.maintenance=False

        if os.getenv('PORTAILVMS_DEBUG_VERBOSE'):
            self.debug("StdOut :\n{}".format("\n".join(stdout)))
            self.debug("StdError :\n{}".format("\n".join(stderr)))

        if callable(self.callback_proc_finished):
            ret={ 'success':True , 'uuid': uuid, 'origin': origin, 'running':False, 'maintenance': self.maintenance}
            if returncode!=0:
                ret['success']=False
                ret['msg']="\n".join([x[0:120] for x in stderr if x.strip()][-7:])
            self.callback_proc_finished(ret)

        self.lock_collection.acquire()
        if uuid in self.collection:
            self.collection.pop(uuid)
        self.lock_collection.release()

if __name__ == '__main__':
    def callback_proc_finished(data):
        print("callback_proc_finished : ", data)
    import time
    config={
        'origin_dir': '/serve/origin/',
        'tmp_target_dir':"/serve/tmp/",
        'iso_dir':"/serve/ISO/",
        'archi_templates_dir' :'/serve/archimulator/templates/',
        'archi_kvm_dir':'/serve/archimulator/kvm/',
        'upload_dir': '/serve/upload/',
    }
    route=Route(config, callback_proc_finished=callback_proc_finished, debug_enabled=True)
    route.launch({"target": "sleep", "uuid": "my_uuid_1", "wait" : "5"})
    route.launch({"target": "sleep", "uuid": "my_uuid_2", "wait" : "1"})
    # to test create qcow2 file
    # route.launch({"target": "createqcow2", "uuid": "my_uuid_3", "name" : "test.qcow2", 'size': 15 })
    # to test force terminate process
    # time.sleep(0.5)
    # route.stop()
