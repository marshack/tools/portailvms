#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

# Example :
# ./startephemeraldocker.sh alpine-chal2-team0 01 700 0.5 1000 alpine ed65436c-21db-11ea-a2cd-8b5be4829329 172.24.8.1 172.24.8.201 1  mysecret


br_interface="br0"
interface_name="eth0"
hostname_prefix='host-'
tmp_dir="/tmp/portailvms/"
tmp_sock_dir="/$tmp_dir/sock"
tmp_lock_dir="/$tmp_dir/lock"

current_script="$(readlink -f "$0")"
dir_source_script="$(dirname $current_script)/sources/"
dir_xterm_scripts="$(dirname $current_script)/xterm/"

source $dir_source_script/functions.sh

function isContainerAlive {
    debug "isContainerAlive $1"
    if [ -n "$(docker container inspect -f '{{.State.Status}}' $1 | grep -i 'running' 2>/dev/null)" ] ; then
        return 0
    fi
    return 1
}

function stopContainer {
    debug "stopContainer $hostname_container"
    docker kill $hostname_container >/dev/null 2>&1
}

function destroyContainer {
    debug "destroyContainer $hostname_container"
    docker rm --force $hostname_container >/dev/null 2>&1 # necessary if never started
}

function execInContainerNameSpace {
    local docker_pid=$1
    shift
    debug "execInContainerNameSpace (pid $docker_pid) $*"
    nsenter -t ${docker_pid} -n $* || warning "failed to run command in docker namespace : $*"
}

function createAndAttachVethInterface {
    local veth_name="$1"
    ip link add tap-B-${veth_name} type veth peer name tap-A-${veth_name} || warning "Impossible to create veth interface ${veth_name}"
    ip link set dev tap-A-${veth_name} up || warning "Impossible to bring up interface tap-A-${veth_name}"

    # attach interface to bridge
    ip link set dev tap-A-${veth_name} master ${br_interface}

    docker_pid="$(docker inspect -f '{{.State.Pid}}' $hostname_container 2>/dev/null)"
    debug "container pid : $docker_pid"
    if ! isNumeric $docker_pid; then
        warning "Impossible to get PID for $hostname_container"
        return
    fi

    ip link set netns ${docker_pid} dev tap-B-${veth_name}
    if [ $? -ne 0 ]; then
        warning "Failed to attach interface tap-B-${veth_name} to this namespace ($hostname_container)"
        return
    fi

    # remove existing interface
    execInContainerNameSpace ${docker_pid} ip link delete ${interface_name}
    # rename tap interface
    execInContainerNameSpace ${docker_pid} ip link set tap-B-${veth_name} name ${interface_name}
    # set mac address
    execInContainerNameSpace ${docker_pid} ip link set dev ${interface_name} address ${macaddr}
    # bring up interface
    execInContainerNameSpace ${docker_pid} ip link set dev ${interface_name} up

    # try to get address
    debug "Start dhclient ..."
    execInContainerNameSpace ${docker_pid} udhcpc -b -n -q -i ${interface_name} -s /opt/portailvms/launcher-service/scripts/udhcpc/default.script
}

if (( $# < 11 )); then
    error "argument(s) missing"
fi

name="${1}"
id="${2}"
ram="${3}"
cpu="${4}"
disk="${5}"
image="${6}"
uuid="${7}"
from_address="${8}"
to_address="${9}"
with_ttyd="${10}"
ttyd_password="${11}"

hostname_container="${hostname_prefix}${uuid}"

if ! isNumeric $id; then
    error "'id' must be a number"
fi

if ! isNumeric $ram; then
    error "'ram' must be a number"
fi

if ! isFloat $cpu; then
    error "'cpu' must be a floating-point numbers"
fi

if ! isNumeric $disk; then
    error "'disk' must be a number"
fi

check_uuid "$uuid" || error "'UUID' is not valid"
check_ip "$from_address" || error " IP address '$from_address' is not valid"
check_ip "$to_address" || error " IP address '$to_address' is not valid"
check_password "$ttyd_password" || error " Password is not valid"

if ! isNumeric $with_ttyd; then
    error "'with_ttyd' must be a number"
fi

if ! cmd_exist ttyd; then
    warning "ttyd missing"
    with_ttyd=0
fi

echo -e "\t name=$name
\t id=$id
\t ram=$ram
\t cpu=$cpu
\t disk=$disk
\t image=$image
\t uid=$uuid
\t hostname=$hostname_container
\t from_address=$from_address
\t to_address=$to_address
\t with_ttyd=$with_ttyd
\t ttyd_password=$ttyd_password
"


macaddr="$(getMacFromId ${id})"

# verbosity is required ?
if [[ "${PORTAILVMS_DEBUG_VERBOSE}" == "2" ]]; then
    echo "Verbose mode is set : 2"
    debug_enable=1
fi

# create container
debug "create container $hostname_container"
cmd="docker create --rm -i --storage-opt size=${disk}m --cpus=${cpu} --memory=${ram}m --name ${hostname_container} ${image}"
debug "$cmd"
$cmd || warning "Impossible to create container $hostname_container"

# start it ...
debug "start container $hostname_container"
docker start $hostname_container || warning "Impossible to start container $hostname_container"

# add iptables rules
source $dir_source_script/iptables_add_rules.sh

# create a veth random interface
veth_name="$(getUniqShortId)"

# intercept signal
trap 'echo "try to stop all jobs ..." ; kill $(jobs -p) ; stopContainer' SIGINT SIGTERM SIGQUIT

if (( $with_ttyd )); then
    novnc_port="$((5900 + ${id#0}))"
    # start ttyd
    xterm_script="xterm_docker.sh"
    source $dir_source_script/start_ttyd_service.sh
    # add iptables rules for ttyd
    source $dir_source_script/iptables_vnc_add_rules.sh
fi

# Loop, in order to manage docker restart
# Until is stopped by 'halt' command (in container) or docker stop/kill
# Blocking with docker wait command, if container is running
while isContainerAlive $hostname_container; do
    createAndAttachVethInterface "${veth_name}"
    docker wait $hostname_container &
    current_job=$!
    # waits for the container to stop
    wait $current_job
    checkIfInfiniteLoop || break
    sleep 0.5
done

# clean all
stopContainer
destroyContainer
ip link delete tap-A-${veth_name} 2>/dev/null
removeLockFileFromId ${veth_name}

if (( $with_ttyd )); then
    # stop ttyd
    source $dir_source_script/stop_ttyd_service.sh
    # remove iptables rules for ttyd
    source $dir_source_script/iptables_vnc_remove_rules.sh
fi

# remove iptables rules
source $dir_source_script/iptables_remove_rules.sh


