#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

# Example :
# ./createqcow2file.sh /serve/origin/ubuntu.qcow2 1000

account_portail="portailvmsd"

current_script="$(readlink -f "$0")"
dir_source_script="$(dirname $current_script)/sources/"

source $dir_source_script/functions.sh

if (( $# < 2 )); then
    error "argument(s) missing"
fi


name="${1}"
size="${2}"

function check_ext_qcow2 {
    if [[ $1 =~ \.qcow2$ ]]; then
        return 0
    fi
    return 1
}

if ! isNumeric $size; then
    error "'size' must be a number"
fi

if ! check_ext_qcow2 $name; then
    error "It's not a qcow2 file"
fi


test -d "$(dirname $name)" || error "Target dir doesn't exit"
test -f "${name}" && error "File '${name}' already exists"

echo -e "\t name=$name
\t size=${size}M
"

qemu-img create -f qcow2 ${name} ${size}M || error "Impossible to create '${name}' file"
chown root:${account_portail} ${name} || error "Impossible to change owner of ${name}"
chmod 640 ${name} || error "Impossible to chmod : ${name}"

