#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

iptables -D INPUT -s $from_address -p tcp -m tcp --dport "$novnc_port" -m comment --comment "generated for portailvms - allow VNC for $from_address" -j ACCEPT || warning "Impossible to remove iptables rules"
iptables -D OUTPUT -d $from_address -p tcp -m tcp --sport "$novnc_port" -m comment --comment "generated for portailvms - allow VNC for $from_address" -j ACCEPT || warning "Impossible to remove iptables rules"
