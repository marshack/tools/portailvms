#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

function error {
    echo -e "\nError : $1\n\n" >&2
    exit 1
}

function warning {
    echo -e "\nWarning : $1\n\n" >&2
}

debug_enable=0 # default value
function debug {
    if [ $debug_enable = 1 ]; then
        echo $*
    fi
}

function isNumeric {
    if [[ $1 =~ ^[0-9]+$ ]]; then
        return 0
    fi
    return 1
}

function isFloat {
    if [[ $1 =~ ^[+-]?[0-9]*\.?[0-9]+$ ]]; then
        return 0
    fi
    return 1
}

function check_ip {
    if [[ $1 =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        return 0
    fi
    return 1
}

function check_uuid {
    if [[ $1 =~ ^\{?[A-F0-9a-f]{8}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{12}\}?$ ]]; then
        return 0
    fi
    return 1
}

function check_password {
    if [[ $1 =~ ^[A-Za-z0-9]{1,25}$ ]]; then
        return 0
    fi
    return 1
}

function cmd_exist {
    type $1 > /dev/null 2>&1
    return $?
}

function create_chain_ifnotexist {
    iptables -L $1 >/dev/null 2>&1 || iptables -N $1 || error "Impossible to create iptables chain : $1"
}

function termprocname {
    killall --quiet $1 > /dev/null 2>&1
}

function killprocname {
    killall --quiet -s SIGKILL $1 > /dev/null 2>&1
}

function getMacFromId {
    id=$1
    echo "02:00:00:00:00:$(printf "%02x" "${id#0}")"
}

function getUniqShortId {
    for i in {1..6}; do
        local candidate=$(head -c 2 /dev/urandom | xxd -p 2>/dev/null)
        local lock_file="${tmp_lock_dir}/${candidate}.lock"
        if [ ! -f ${lock_file} ]; then
            touch "${lock_file}" &&  echo "${candidate}" ; return
        fi
    done
}

function removeLockFileFromId {
    local lock_file="${tmp_lock_dir}/$1.lock"
    if [ -f ${lock_file} ]; then
        rm -f "${lock_file}" || warning "Impossible to remove lock file : ${lock_file}"
    fi
}

# Infinite loop protection
_count_=0
_start_=0
function checkIfInfiniteLoop {
    local retval=0
    local _end_=$(date +%s)
    if [[ $((_end_-_start_)) -le 5 ]]; then
        warning "Container stopped very quickly!"
        _count_="$(( _count_ +1 ))"
        if [[ ${_count_} -ge 3 ]]; then
            warning "too many restarts -> stop container!"
            retval=1
        fi
    fi
    _start_=${_end_}
    return ${retval}
}
