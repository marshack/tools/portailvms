#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

ps -edf | grep -E "ttyd\s+.*\s+${hostname_container}" | awk '{print $2}' | xargs -r kill -s SIGTERM
