#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

# start vnc proxy web
runuser $account_vnc -s /bin/sh -c  "/opt/noVNC/proxyVNC/utils/websockify/websockify.py -D --web $path_novnc/proxyVNC/ --cert $pem_file $novnc_port $vnc_target 2>&1"

