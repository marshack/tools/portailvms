#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

current_job="$(jobs -p)"
echo "Current Job : $current_job"
sleep 1
# fix right
chmod 770 $sock_filename || warning "Impossible to change mode (file $sock_filename)"
chgrp portailvmsd $sock_filename || warning "Impossible to change group (file $sock_filename)"
if [ "$with_vnc" = "1" ] && [ -n "$vnc_password" ]; then
    sleep 1
    # fix VNC password
    echo -e "set_password vnc ${vnc_password}" | socat - unix-connect:$sock_filename
fi

