#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

addresses_vm_hash="addresses_vm_hash"
prefix_addr_hash="prf_addr_pvms_"
ipset_name="${prefix_addr_hash}${from_address}"

# remove iptables rules
iptables -D VMS-PORTAIL -m set --match-set $ipset_name src -d $to_address -m comment --comment "generated for portailvms - allow $from_address" -j ACCEPT || warning "Impossible to apply iptables rules"
iptables -D VMS-PORTAIL -s $to_address -m set --match-set $ipset_name dst -m comment --comment "generated for portailvms - allow $from_address" -j ACCEPT || warning "Impossible to apply iptables rules"

# remove ipset rules
ipset test $ipset_name $to_address 2>/dev/null && ipset del -! $ipset_name $to_address
ipset test $addresses_vm_hash $to_address 2>/dev/null && ipset del -! $addresses_vm_hash $to_address

