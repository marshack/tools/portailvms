#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

addresses_vm_hash="addresses_vm_hash"
prefix_addr_hash="prf_addr_pvms_"
ipset_name="${prefix_addr_hash}${from_address}"

# add ipset rules
ipset create -! $ipset_name iphash
ipset test $ipset_name $from_address 2>/dev/null || ipset add -! $ipset_name $from_address # owner (never removed)
ipset test $ipset_name $to_address 2>/dev/null || ipset add -! $ipset_name $to_address
ipset test $addresses_vm_hash $to_address 2>/dev/null || ipset add -! $addresses_vm_hash $to_address

# add iptables rules
iptables -A VMS-PORTAIL -m set --match-set $ipset_name src -d $to_address -m comment --comment "generated for portailvms - allow $from_address" -j ACCEPT || warning "Impossible to apply iptables rules"
iptables -A VMS-PORTAIL -s $to_address -m set --match-set $ipset_name dst -m comment --comment "generated for portailvms - allow $from_address" -j ACCEPT || warning "Impossible to apply iptables rules"

