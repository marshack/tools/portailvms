#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)


function makeKvmCommand {
    local _name="${1}"
    local _id="${2}"
    local _guest_type="${3}"
    local _with_vnc="${4}"
    local _ram="${5}"
    local _cpu="${6}"
    local _net_virtio="${7}"
    local _scsi_virtio="${8}"
    local _file="${9}"
    local _sock_filename="${10}"
    local _keyboard="${11}"
    local cmd="qemu-system-x86_64"

    if [[ -c "/dev/kvm" ]] ; then
        cmd="kvm"
    fi

    cmd="$cmd -k ${_keyboard} -name ${_name}"

    if (($_guest_type==0)); then
        # default
        cmd="$cmd -boot c"
    elif (($_guest_type==1)); then
        # Windows with HyperV
        cmd="$cmd -cpu SandyBridge,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time,+vmx -boot c"
    elif (($_guest_type==8)); then
        cmd="$cmd -boot menu=off,order=c -bios /usr/share/ovmf/OVMF.fd"
    elif (($_guest_type==9)); then
        cmd="$cmd -boot menu=on,order=c -bios /usr/share/ovmf/OVMF.fd"
    elif (($_guest_type==16)); then
        cmd="$cmd -cpu host -boot c"
    else
        error "Bad 'guest_type'"
    fi

    # CPU(s)
    cmd="$cmd -smp cores=$_cpu,threads=1,sockets=1"

    if (( $_with_vnc )); then
        cmd="$cmd -vnc 127.0.0.1:$_id,password=on"
    else
        cmd="$cmd -nographic"
    fi

    # RAM
    cmd="$cmd -m $_ram"

    # virtio balloon device allows KVM guests to reduce their memory size
    cmd="$cmd -device virtio-balloon"

    # network interface
    local device="e1000" # default
    if (( $_net_virtio )); then
        device="virtio-net-pci"
    fi

    local tapname="tap$_id"
    local macaddr="$(getMacFromId ${id})"

    cmd="$cmd -netdev tap,id=$tapname,ifname=$tapname,script=no,downscript=no"
    cmd="$cmd -device $device,romfile=,netdev=$tapname,mac=$macaddr"

    # hard disk
    cmd="$cmd -drive file=$_file"

    if (( $_scsi_virtio )); then
        cmd="$cmd,if=virtio,cache=off"
    fi

    # qemu/kvm process have a monitor accessible via a UNIX socket
    cmd="$cmd -monitor unix:$_sock_filename,server,nowait"

    # Use a USB tablet instead of the default PS/2 mouse. Recommend, because the tablet sends the mouse cursor's position to match the host mouse cursor
    cmd="$cmd -device ich9-usb-ehci1,id=usb,bus=pci.0,addr=0x5.0x7 -device ich9-usb-uhci1,masterbus=usb.0,firstport=0,bus=pci.0,multifunction=on,addr=0x5 -device usb-tablet,id=input0,bus=usb.0,port=1"

    echo "$cmd"
}
