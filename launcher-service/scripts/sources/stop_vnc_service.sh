#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

# stop vnc proxy web
ps -edf | grep -E "websockify .*--cert\s+$path_novnc\S+\.pem\s+$novnc_port\s+$vnc_target" | awk '{print $2}' | xargs -r kill -s SIGQUIT
