#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

cert_key="/opt/noVNC/pem/novnc.pem"
password=${ttyd_password} ttyd -W -m 1 -p ${novnc_port} -S -C ${cert_key} -K ${cert_key} -t titleFixed="${hostname_container}" --url-arg $dir_xterm_scripts/${xterm_script} ${hostname_container} 2>&1 &