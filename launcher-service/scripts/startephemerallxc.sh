#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

# Example :
# ./startephemerallxc.sh alpine-04-chal2-team0 01 /serve/portailvmsd/lxc/ 700 1 1000 alpine ed65436c-21db-11ea-a2cd-8b5be4829329 172.24.8.1 172.24.8.201 1  mysecret

br_interface="br0"
interface_name="eth0"
hostname_prefix='host-'
cert_key="/opt/noVNC/pem/novnc.pem"
su_cmd="runuser portailvmsd -s /bin/sh -c "
tmp_dir="/tmp/portailvms/"
tmp_sock_dir="/$tmp_dir/sock"
tmp_lock_dir="/$tmp_dir/lock"

current_script="$(readlink -f "$0")"
dir_source_script="$(dirname $current_script)/sources/"
dir_xterm_scripts="$(dirname $current_script)/xterm/"

source $dir_source_script/functions.sh

function isContainerAlive {
    if [ -n "$(lxc-ls -P ${containers_path} --line --filter="^${1}$")" ] ; then
        return 0
    fi
    return 1
}

function destroyContainer {
    debug "destroyContainer ${hostname_container}"
    $su_cmd "lxc-stop -t 1 -n ${hostname_container}" >/dev/null 2>&1
    sleep 0.1
    $su_cmd "lxc-destroy -n ${hostname_container}" >/dev/null 2>&1 # necessary if never started
    sleep 0.1
    if [ -f ${containers_path}/${hostname_container}/config ]; then
        # force remove files
        warning "Remove all files (container ${hostname_container})"
        rm -rf ${containers_path}/${hostname_container}
    fi
}

if (( $# < 12 )); then
    error "argument(s) missing"
fi


name="${1}"
id="${2}"
lxc_path="${3}"
images_path="$(readlink -m ${lxc_path}/images/)"
containers_path="$(readlink -m ${lxc_path}/containers/)"
ram="${4}"
cpu="${5}"
disk="${6}"
image="${7}"
uuid="${8}"
from_address="${9}"
to_address="${10}"
with_ttyd="${11}"
ttyd_password="${12}"

hostname_container="${hostname_prefix}${uuid}"

if ! isNumeric $id; then
    error "'id' must be a number"
fi

if [[ ! -d "$lxc_path"  ]] ; then
    error "Directory '$lxc_path' doesn't exist"
fi

if [[ ! -d "$images_path"  ]] ; then
    error "Directory '$images_path' doesn't exist"
fi

if [[ ! -d "$containers_path"  ]] ; then
    error "Directory '$containers_path' doesn't exist"
fi

if ! isNumeric $ram; then
    error "'ram' must be a number"
fi

if ! isNumeric $cpu; then
    error "'cpu' must be a number"
fi

if ! isNumeric $disk; then
    error "'disk' must be a number"
fi

check_uuid "$uuid" || error "'UUID' is not valid"
check_ip "$from_address" || error " IP address '$from_address' is not valid"
check_ip "$to_address" || error " IP address '$to_address' is not valid"
check_password "$ttyd_password" || error " Password is not valid"

if ! isNumeric $with_ttyd; then
    error "'with_ttyd' must be a number"
fi

if ! cmd_exist ttyd; then
    warning "ttyd missing"
    with_ttyd=0
fi

echo -e "\t name=$name
\t id=$id
\t images path=$images_path
\t containers path=$containers_path
\t ram=$ram
\t cpu=$cpu
\t disk=$disk
\t image=$image
\t uid=$uuid
\t hostname=$hostname_container
\t from_address=$from_address
\t to_address=$to_address
\t with_ttyd=$with_ttyd
\t ttyd_password=$ttyd_password
"


macaddr="$(getMacFromId ${id})"

# verbosity is required ?
if [[ "${PORTAILVMS_DEBUG_VERBOSE}" == "2" ]]; then
    echo "Verbose mode is set : 2"
    debug_enable=1
fi

# create container
debug "create container ${hostname_container}"
$su_cmd "lxc-copy -B btrfs -P ${images_path} -p ${containers_path} -n ${image} -N ${hostname_container}"  || warning "Impossible to create container ${hostname_container}"

# add iptables rules
source $dir_source_script/iptables_add_rules.sh


# configure container
echo "lxc.ephemeral = 1" >> ${containers_path}/${hostname_container}/config || warning "Impossible to configure (container ${hostname_container})"
sed -i -r "s/^(lxc\.net\.0\.hwaddr\s*=\s*).*/\1${macaddr}/" ${containers_path}/${hostname_container}/config || warning "Impossible to change MAC address (container ${hostname_container})"

# Quotas
btrfs qgroup limit ${disk}M ${containers_path}/${hostname_container}/rootfs || warning "Impossible to set disk quota (container ${hostname_container})"
echo "lxc.cgroup2.cpu.max = $(( ${cpu} * 100000 )) 100000
lxc.cgroup2.memory.max = ${ram}M
lxc.cgroup2.memory.swap.max = ${ram}M " >> ${containers_path}/${hostname_container}/config || warning "Impossible to set quotas (container ${hostname_container})"

if [ $debug_enable = 1 ]; then
    echo "Config :"
    cat ${containers_path}/${hostname_container}/config
fi

# required for systemd/dbus
export XDG_RUNTIME_DIR="/run/user/$(id -u portailvmsd)"
if [ -S ${XDG_RUNTIME_DIR}/bus ]; then
    export DBUS_SESSION_BUS_ADDRESS="unix:path=${XDG_RUNTIME_DIR}/bus"
    debug "start container ${hostname_container}"
    $su_cmd "/usr/bin/systemd-run --user --scope -p "Delegate=yes" /usr/bin/lxc-start ${hostname_container}" || (warning "Impossible to start container ${hostname_container}" ; destroyContainer)
    sleep 1
else
    warning "Impossible to start container ${hostname_container} (DBUS connection failed)"
    destroyContainer
fi

# intercept signal
trap 'echo "try to stop all jobs ..." ; kill $(jobs -p) ; destroyContainer' SIGINT SIGTERM SIGQUIT

if (( $with_ttyd )); then
    novnc_port="$((5900 + ${id#0}))"
    # start ttyd
    $su_cmd "containers_path=${containers_path} password=${ttyd_password} ttyd -W -m 1 -p ${novnc_port} -S -C ${cert_key} -K ${cert_key} -t titleFixed='${hostname_container}' --url-arg $dir_xterm_scripts/xterm_lxc.sh ${hostname_container}" &
    # add iptables rules for ttyd
    source $dir_source_script/iptables_vnc_add_rules.sh
fi

# Loop, in order to manage lxc restart
# Until is stopped by 'halt' command (in container) or lxc-stop
# Blocking with lxc-wait command, if container is running
while isContainerAlive ${hostname_container}; do
    $su_cmd "lxc-wait -s STOPPED -n ${hostname_container}" &
    current_job=$!
    # waits for the container to stop
    wait $current_job
    checkIfInfiniteLoop || break
    sleep 0.5
done

# clean all
destroyContainer

if (( $with_ttyd )); then
    # stop ttyd
    source $dir_source_script/stop_ttyd_service.sh
    # remove iptables rules for ttyd
    source $dir_source_script/iptables_vnc_remove_rules.sh
fi

# remove iptables rules
source $dir_source_script/iptables_remove_rules.sh


