#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

chain_portail="VMS-PORTAIL"
addresses_vm_hash="addresses_vm_hash"
prefix_addr_hash="prf_addr_pvms_"
tmp_dir="/tmp/portailvms/"
tmp_sock_ux="/$tmp_dir/sock"
tmp_lock_dir="/$tmp_dir/lock"
lxd_cmd="lxc --force-local"

current_script="$(readlink -f "$0")"
dir_source_script="$(dirname $current_script)/sources/"

source $dir_source_script/functions.sh

if (( $# < 3 )); then
    error "argument(s) missing"
fi


tmp_target_dir="${1}"
tmp_upload_dir="${2}"
lxc_ephemeral_dir="$(readlink -m ${3}/containers/)"

if [[ ! -d "$tmp_target_dir"  ]] ; then
    error "Directory '$tmp_target_dir' doesn't exist"
fi

if [[ ! -d "$tmp_upload_dir"  ]] ; then
    error "Directory '$tmp_upload_dir' doesn't exist"
fi

if [[ ! -d "$lxc_ephemeral_dir"  ]] ; then
    warning "Directory '$lxc_ephemeral_dir' doesn't exist"
fi

# terminate kvm/qemu
termprocname kvm
termprocname qemu-system-x86_64
termprocname qemu-system-i386

# terminate noVNC
ps -edf | grep -E "^novnc\s+.*\s+python.*\s+websockify\s+.*noVNC.*" | awk '{print $2}' | xargs -r kill > /dev/null 2>&1

# terminate ttyd
termprocname ttyd

# Architecture : terminate all instances
ps -edf | grep -E '[p]ython3\s+.*\/archimulator.py' | awk '{print $2}' | xargs -r kill  > /dev/null 2>&1
ps -edf | grep -E '[p]ython3\s+.*fake_services\/.*.py' | awk '{print $2}' | xargs -r kill > /dev/null 2>&1

# terminate all scripts
termprocname startdisposablevm.sh
termprocname startmaster.sh
termprocname startephemerallxc.sh
termprocname startephemerallxd.sh
termprocname startephemeraldocker.sh
termprocname startarchimulator.sh

sleep 2
# emergency kill (when kvm has crashed)
killprocname kvm
killprocname qemu-system-x86_64
killprocname qemu-system-i386

# remove tmp files
test -d $tmp_target_dir && rm -vf /$tmp_target_dir/*.qcow2
test -d $tmp_sock_ux && rm -vf /$tmp_sock_ux/sock_*

# remove lock files
test -d $tmp_lock_dir && rm -vf /$tmp_lock_dir/*.lock

# remove upload tmp files
test -d $tmp_upload_dir && rm -vf /$tmp_upload_dir/*.{qcow2,iso,yaml,yml,tar,gz}

# remove tap interfaces
ip addr show | grep -E "^[[:digit:]]{1,2}:[[:space:]]+tap[[:digit:]]{2,2}" | awk -F : '{print $2}' | sed 's/ //' | xargs -r -I '{}' sh -c "ip link set dev '{}' nomaster ; ip link delete '{}'"

# stop all LXC containers
cmd_exist lxc-ls && lxc-ls -P ${lxc_ephemeral_dir} -1 | xargs -r lxc-stop -P ${lxc_ephemeral_dir} -k
# destroy (step 1)
cmd_exist lxc-ls && lxc-ls -P ${lxc_ephemeral_dir} -1 | xargs -r lxc-destroy -P ${lxc_ephemeral_dir}
# destroy (step 2)
find  ${lxc_ephemeral_dir} -mindepth 1 -maxdepth 1 -type d | xargs -r -I '{}' sh -c "test -d {}/rootfs && rm -rf {}"

# stop all LXD containers
cmd_exist lxc && $lxd_cmd list --format csv | awk -F , '{print $1}' | xargs -r -I '{}' sh -c "$lxd_cmd stop --force --timeout=0 '{}' ; $lxd_cmd delete --force '{}'"

# stop all Docker containers
cmd_exist docker && docker ps -a -q | xargs -r -I '{}' sh -c "docker kill '{}' ; docker rm --force '{}'"

# clear iptables rules
iptables -D VMS-PORTAIL -m set --match-set $addresses_vm_hash dst -m comment --comment "generated for portailvms - user rules" -j VMS-USER-INPUT
iptables -D VMS-PORTAIL -m set --match-set $addresses_vm_hash src -m comment --comment "generated for portailvms - user rules" -j VMS-USER-OUTPUT
iptables -L $chain_portail >/dev/null 2>&1 && iptables -F $chain_portail

# destroy ipset iphash table
ipset destroy $addresses_vm_hash  >/dev/null 2>&1;echo -n
ipset list -n | grep -E "^prf_addr_pvms_" | xargs -r -I '{}' ipset destroy '{}'

# remove all network namespaces
ip netns | awk '{print $1}' | xargs -r -I '{}' ip netns delete {}

