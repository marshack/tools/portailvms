#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

br_interface="br0"
account_portail="portailvmsd"
chain_portail="VMS-PORTAIL"
chain_input="VMS-USER-INPUT"
chain_output="VMS-USER-OUTPUT"
addresses_vm_hash="addresses_vm_hash"
tmp_dir="/tmp/portailvms/"
tmp_sessions_dir="/$tmp_dir/sessions"
tmp_sock_dir="/$tmp_dir/sock"
tmp_lock_dir="/$tmp_dir/lock"

current_script="$(readlink -f "$0")"
dir_source_script="$(dirname $current_script)/sources/"

source $dir_source_script/functions.sh

if (( $# < 1 )); then
    error "argument(s) missing"
fi

tmp_target_dir="${1}"

if [[ ! -d "$tmp_target_dir"  ]] ; then
    error "Directory '$tmp_target_dir' doesn't exist"
fi

# enable btrfs quotas (FIX docker disable it ?!? : https://github.com/docker/for-linux/issues/78)
btrfs quota enable ${tmp_target_dir} || error "Impossible to enable btrfs quotas"

chmod o= $tmp_dir

# add ipset rules
ipset create -! $addresses_vm_hash iphash

create_chain_ifnotexist $chain_portail
create_chain_ifnotexist $chain_input
create_chain_ifnotexist $chain_output

if [ -z "$(iptables -L FORWARD -n | grep $chain_portail)" ] ; then
    iptables -A FORWARD -i $br_interface -o $br_interface -m comment --comment "generated for portailvms" -j $chain_portail
fi

# user rules
if [ -z "$(iptables -L VMS-PORTAIL | grep $addresses_vm_hash)" ] ; then
    iptables -I VMS-PORTAIL -m set --match-set $addresses_vm_hash dst -m comment --comment "generated for portailvms - user rules" -j VMS-USER-INPUT
    iptables -I VMS-PORTAIL -m set --match-set $addresses_vm_hash src -m comment --comment "generated for portailvms - user rules" -j VMS-USER-OUTPUT
fi

if [ -z "$(lsmod | grep br_netfilter)" ] ; then
    modprobe br_netfilter || error "Impossible to load module br_netfilter"
fi

sysctl -w net.bridge.bridge-nf-call-iptables=1 || error "Impossible to set value on net.bridge.bridge-nf-call-iptables"
sysctl -w net.bridge.bridge-nf-call-ip6tables=1 || error "Impossible to set value on net.bridge.bridge-nf-call-ip6tables"

# fix right
# tmp directory
chown root:$account_portail $tmp_dir || error "Impossible to change owner of $tmp_dir"
chmod u=rwx,g=rx,o= $tmp_dir || error "Impossible to chmod : $tmp_dir"

# socket directory
chown $account_portail:kvm $tmp_sock_dir || error "Impossible to change owner of $tmp_sock_dir"
chmod u=x,g=wx,o= $tmp_sock_dir || error "Impossible to chmod : $tmp_sock_dir"

# lock directory
mkdir -m 750 -p $tmp_lock_dir || error "Impossible to create directory : $tmp_lock_dir"

# sessions directory
mkdir -p $tmp_sessions_dir || error "Impossible to create directory : $tmp_sessions_dir"
chmod u=rwx,g=rx,o= $tmp_sessions_dir || error "Impossible to chmod : $tmp_sessions_dir"
chown -R $account_portail:$account_portail $tmp_sessions_dir || error "Impossible to change owner of $tmp_sessions_dir"

