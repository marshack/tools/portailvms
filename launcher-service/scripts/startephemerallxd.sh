#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

# Example :
# ./startephemerallxd.sh debian-chal2-team0 01 700 1 1000 debian ed65436c-21db-11ea-a2cd-8b5be4829329 172.24.8.1 172.24.8.201 1  mysecret


br_interface="br0"
interface_name="eth0"
storage_name="default"
hostname_prefix='host-'
lxd_cmd="lxc --force-local"
tmp_dir="/tmp/portailvms/"
tmp_sock_dir="/$tmp_dir/sock"
tmp_lock_dir="/$tmp_dir/lock"

current_script="$(readlink -f "$0")"
dir_source_script="$(dirname $current_script)/sources/"
dir_xterm_scripts="$(dirname $current_script)/xterm/"

source $dir_source_script/functions.sh

function isContainerAlive {
    if [ -n "$($lxd_cmd list --format csv $1)" ] ; then
        return 0
    fi
    return 1
}

function stopContainer {
    $lxd_cmd stop --force --timeout=0 $hostname_container >/dev/null 2>&1
}

function destroyContainer {
    $lxd_cmd delete --force $hostname_container >/dev/null 2>&1 # necessary if never started
}

if (( $# < 11 )); then
    error "argument(s) missing"
fi


name="${1}"
id="${2}"
ram="${3}"
cpu="${4}"
disk="${5}"
image="${6}"
uuid="${7}"
from_address="${8}"
to_address="${9}"
with_ttyd="${10}"
ttyd_password="${11}"

hostname_container="${hostname_prefix}${uuid}"

if ! isNumeric $id; then
    error "'id' must be a number"
fi

if ! isNumeric $ram; then
    error "'ram' must be a number"
fi

if ! isNumeric $cpu; then
    error "'cpu' must be a number"
fi

if ! isNumeric $disk; then
    error "'disk' must be a number"
fi

check_uuid "$uuid" || error "'UUID' is not valid"
check_ip "$from_address" || error " IP address '$from_address' is not valid"
check_ip "$to_address" || error " IP address '$to_address' is not valid"
check_password "$ttyd_password" || error " Password is not valid"

if ! isNumeric $with_ttyd; then
    error "'with_ttyd' must be a number"
fi

if ! cmd_exist ttyd; then
    warning "ttyd missing"
    with_ttyd=0
fi

echo -e "\t name=$name
\t id=$id
\t ram=$ram
\t cpu=$cpu
\t disk=$disk
\t image=$image
\t uid=$uuid
\t hostname=$hostname_container
\t from_address=$from_address
\t to_address=$to_address
\t with_ttyd=$with_ttyd
\t ttyd_password=$ttyd_password
"


macaddr="$(getMacFromId ${id})"


# create container
$lxd_cmd init --ephemeral "${image}" "${hostname_container}" -s "${storage_name}" || warning "Impossible to create container $hostname_container"

# add iptables rules
source $dir_source_script/iptables_add_rules.sh


# attach interface to bridge and change MAC address
$lxd_cmd config device add $hostname_container $interface_name nic name=$interface_name nictype=bridged parent=$br_interface || warning "Impossible to create interface (container $hostname_container)"
$lxd_cmd config set $hostname_container volatile.${interface_name}.hwaddr "$macaddr" || warning "Impossible to change MAC adddress (container $hostname_container)"
# Quotas
$lxd_cmd config device set $hostname_container root size ${disk}MB || warning "Impossible to set disk quota (container $hostname_container)"
$lxd_cmd config set $hostname_container limits.cpu $cpu || warning "Impossible to set cpu quota (container $hostname_container and number of CPU)"
$lxd_cmd config set $hostname_container limits.cpu.allowance 10% || warning "Impossible to set cpu quota (container $hostname_container and CPU allowance)"
$lxd_cmd config set $hostname_container limits.memory ${ram}MB || warning "Impossible to set memory quota (container $hostname_container)"

$lxd_cmd start $hostname_container || warning "Impossible to start container $hostname_container"
sleep 1

# intercept signal
trap 'echo "try to stop all jobs ..." ; kill $(jobs -p) ; stopContainer' SIGINT SIGTERM SIGQUIT

if (( $with_ttyd )); then
    novnc_port="$((5900 + ${id#0}))"
    # start ttyd
    xterm_script="xterm_lxd.sh"
    source $dir_source_script/start_ttyd_service.sh
    # add iptables rules for ttyd
    source $dir_source_script/iptables_vnc_add_rules.sh
fi


# Loop, in order to manage lxc restart
# Until is stopped by 'halt' command (in container) or lxc stop
# Blocking with sleep command, if container is running
while isContainerAlive $hostname_container; do
    $lxd_cmd exec -T $hostname_container sleep infinity &
    current_job=$!
    # waits for the container to stop
    wait $current_job
    checkIfInfiniteLoop || break
    sleep 0.5
done

# clean all
stopContainer
destroyContainer

if (( $with_ttyd )); then
    # stop ttyd
    source $dir_source_script/stop_ttyd_service.sh
    # remove iptables rules for ttyd
    source $dir_source_script/iptables_vnc_remove_rules.sh
fi

# remove iptables rules
source $dir_source_script/iptables_remove_rules.sh


