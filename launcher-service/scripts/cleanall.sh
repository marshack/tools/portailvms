#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)


current_script="$(readlink -f "$0")"
script_path=$(dirname $current_script)
dir_source_script="${script_path}/sources/"

source $dir_source_script/functions.sh

if (( $# < 3 )); then
    error "argument(s) missing"
fi


tmp_target_dir="${1}"
tmp_upload_dir="${2}"
lxc_dir="${3}"

if [[ ! -d "$tmp_target_dir"  ]] ; then
    error "Directory '$tmp_target_dir' doesn't exist"
fi

if [[ ! -d "$tmp_upload_dir"  ]] ; then
    error "Directory '$tmp_upload_dir' doesn't exist"
fi

if [[ ! -d "$lxc_dir"  ]] ; then
    warning "Directory '$lxc_dir' doesn't exist"
fi

echo "# exec prune.sh"
${script_path}/prune.sh ${tmp_target_dir} ${tmp_upload_dir} ${lxc_dir}
echo "# exec initialise.sh"
${script_path}/initialise.sh ${tmp_target_dir}

