#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

# Example :
# ./startarchimulator.sh archi_chal2_team0 01 /serve/archimulator/templates/external.yaml ed65436c-21db-11ea-a2cd-8b5be4829329 172.24.8.1 172.24.8.200 1  mysecret

path_archimulator="/opt/archimulator"
path_novnc="/opt/noVNC"
account_vnc="novnc"
pem_file="$path_novnc/pem/novnc.pem"
right="root:portailvmsd:660"
tmp_dir="/tmp/portailvms/"
tmp_sock_dir="/$tmp_dir/sock"
tmp_lock_dir="/$tmp_dir/lock"

current_script="$(readlink -f "$0")"
dir_source_script="$(dirname $current_script)/sources/"

source $dir_source_script/functions.sh

if (( $# < 8 )); then
    error "argument(s) missing"
fi


name="${1}"
id="${2}"
template="${3}"
uuid="${4}"
from_address="${5}"
to_address="${6}"
with_vnc="${7}"
vnc_password="${8}"

if [[ ! -f "$template"  ]] ; then
    error "File '$template' doesn't exist"
fi

if [[ ! -d "$tmp_sock_dir"  ]] ; then
    error "Directory '$tmp_sock_dir' doesn't exist"
fi

if ! isNumeric $id; then
    error "'id' must be a number"
fi

check_uuid "$uuid" || error "'UUID' is not valid"
check_ip "$from_address" || error " IP address '$from_address' is not valid"
check_ip "$to_address" || error " IP address '$to_address' is not valid"
check_password "$vnc_password" || error " Password is not valid"

if ! isNumeric $with_vnc; then
    error "'with_vnc' must be a number"
fi

echo -e "\t name=$name
\t id=$id
\t template=$template
\t tmp_sock_dir=$tmp_sock_dir
\t uid=$uuid
\t from_address=$from_address
\t to_address=$to_address
\t with_vnc=$with_vnc
\t vnc_password=$vnc_password
"

macaddr="$(getMacFromId ${id})"

sock_filename="$tmp_sock_dir/sock_archi_${uuid}"

# add iptables rules
source $dir_source_script/iptables_add_rules.sh

if (( $with_vnc )); then
    vnc_target="$to_address:5901"
    novnc_port="$((6900 + ${id#0}))"
    # start vnc proxy web
    source $dir_source_script/start_vnc_service.sh
    # add iptables rules for novnc
    source $dir_source_script/iptables_vnc_add_rules.sh
fi

# intercept signal
trap 'echo "try to stop all jobs ..." ; kill $(jobs -p)' SIGINT SIGTERM SIGQUIT

# verbosity is required ?
if [[ "${PORTAILVMS_DEBUG_VERBOSE}" == "2" ]]; then
    echo "Verbose mode is set : 2"
    debug_arg="--debug"
else
    debug_arg=""
fi

# start archi
cd $path_archimulator
cmd="./archimulator.py $debug_arg -f $template -s "/{{ADDRESS}}/$to_address/" "/{{MAC}}/$macaddr/" "/{{PASSWORD}}/$vnc_password/" --monitor $sock_filename:$right"
echo "$cmd"
$cmd &
current_job=$!

# wait archimulator
wait $current_job
retcode=$?

# remove iptables rules
source $dir_source_script/iptables_remove_rules.sh

if (( $with_vnc )); then
    # stop vnc proxy web
    source $dir_source_script/stop_vnc_service.sh
    # remove iptables rules for novnc
    source $dir_source_script/iptables_vnc_remove_rules.sh
fi

rm -f "$sock_filename" || warning "Impossible to remove socket unix files : $sock_filename"

exit $retcode
