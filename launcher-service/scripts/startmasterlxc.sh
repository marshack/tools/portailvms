#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

# Example :
# ./startmasterlxc.sh alpine-04-chal2-team0 01 /serve/portailvmsd/lxc/ 700 1 1000 alpine ed65436c-21db-11ea-a2cd-8b5be4829329 172.24.8.1 172.24.8.201  mysecret


br_interface="br0"
interface_name="eth0"
cert_key="/opt/noVNC/pem/novnc.pem"
su_cmd="runuser portailvmsd -s /bin/sh -c "
tmp_dir="/tmp/portailvms/"
tmp_sock_dir="/$tmp_dir/sock"
tmp_lock_dir="/$tmp_dir/lock"

current_script="$(readlink -f "$0")"
dir_source_script="$(dirname $current_script)/sources/"
dir_xterm_scripts="$(dirname $current_script)/xterm/"

source $dir_source_script/functions.sh

function isContainerAlive {
    if [ -n "$(lxc-ls -P ${containers_path} --running --line --filter="^${1}$")" ] ; then
        return 0
    fi
    return 1
}

function stopContainer {
    debug "stopContainer ${image}"
    lxc-stop -P ${containers_path} -t 1 -n ${image} >/dev/null 2>&1
}

if (( $# < 11 )); then
    error "argument(s) missing"
fi

name="${1}"
id="${2}"
lxc_path="${3}"
containers_path="$(readlink -m ${lxc_path}/images/)"
ram="${4}"
cpu="${5}"
disk="${6}"
image="${7}"
uuid="${8}"
from_address="${9}"
to_address="${10}"
with_ttyd="1" # master always use ttyd
ttyd_password="${11}"

if ! isNumeric $id; then
    error "'id' must be a number"
fi

if [[ ! -d "$lxc_path"  ]] ; then
    error "Directory '$lxc_path' doesn't exist"
fi

if [[ ! -d "$containers_path"  ]] ; then
    error "Directory '$containers_path' doesn't exist"
fi

if ! isNumeric $ram; then
    error "'ram' must be a number"
fi

if ! isNumeric $cpu; then
    error "'cpu' must be a number"
fi

if ! isNumeric $disk; then
    error "'disk' must be a number"
fi

check_uuid "$uuid" || error "'UUID' is not valid"
check_ip "$from_address" || error " IP address '$from_address' is not valid"
check_ip "$to_address" || error " IP address '$to_address' is not valid"
check_password "$ttyd_password" || error " Password is not valid"

if ! cmd_exist ttyd; then
    warning "ttyd missing"
    with_ttyd=0
fi

echo -e "\t name=$name
\t id=$id
\t images path=$containers_path
\t ram=$ram
\t cpu=$cpu
\t disk=$disk
\t image=$image
\t uid=$uuid
\t from_address=$from_address
\t to_address=$to_address
\t with_ttyd=$with_ttyd
\t ttyd_password=$ttyd_password
"

hostname_container=$image
macaddr="$(getMacFromId ${id})"

# verbosity is required ?
if [[ "${PORTAILVMS_DEBUG_VERBOSE}" == "2" ]]; then
    echo "Verbose mode is set : 2"
    debug_enable=1
fi

# add iptables rules
source $dir_source_script/iptables_add_rules.sh


# configure container
sed -i -r "s/^(lxc\.net\.0\.hwaddr\s*=\s*).*/\1${macaddr}/" ${containers_path}/${image}/config || warning "Impossible to change MAC address (container ${image})"

if [ $debug_enable = 1 ]; then
    echo "Config :"
    cat ${containers_path}/${image}/config
fi

# required for systemd/dbus
export XDG_RUNTIME_DIR="/run/user/$(id -u portailvmsd)"
if [ -S ${XDG_RUNTIME_DIR}/bus ]; then
    export DBUS_SESSION_BUS_ADDRESS="unix:path=${XDG_RUNTIME_DIR}/bus"
    debug "start container ${image}"
    $su_cmd "/usr/bin/systemd-run --user --scope -p "Delegate=yes" /usr/bin/lxc-start -P ${containers_path} ${image}" || (warning "Impossible to start container ${image}" ; stopContainer)
    sleep 1
else
    warning "Impossible to start container ${image} (DBUS connection failed)"
    stopContainer
fi

# intercept signal
trap 'echo "try to stop all jobs ..." ; kill $(jobs -p) ; stopContainer' SIGINT SIGTERM SIGQUIT

if (( $with_ttyd )); then
    novnc_port="$((5900 + ${id#0}))"
    # start ttyd
    $su_cmd "containers_path=${containers_path} password=${ttyd_password} ttyd -W -m 1 -p ${novnc_port} -S -C ${cert_key} -K ${cert_key} -t titleFixed='${image} | MASTER' --url-arg $dir_xterm_scripts/xterm_lxc.sh ${image}" &
    # add iptables rules for ttyd
    source $dir_source_script/iptables_vnc_add_rules.sh
fi

# Loop, in order to manage lxc restart
# Until is stopped by 'halt' command (in container) or lxc-stop
# Blocking with lxc-wait command, if container is running
while isContainerAlive ${image}; do
    $su_cmd "lxc-wait -P ${containers_path} -s STOPPED -n ${image}" &
    current_job=$!
    # waits for the container to stop
    wait $current_job
    checkIfInfiniteLoop || break
    sleep 0.5
done

# clean all
stopContainer

if (( $with_ttyd )); then
    # stop ttyd
    source $dir_source_script/stop_ttyd_service.sh
    # remove iptables rules for ttyd
    source $dir_source_script/iptables_vnc_remove_rules.sh
fi

# remove iptables rules
source $dir_source_script/iptables_remove_rules.sh


