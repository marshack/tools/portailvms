#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

# Example :
# ./startdisposablevm.sh ubuntu_chal2_team0 01 /serve/tmp 1024 1 /serve/origin/ubuntu.qcow2 ed65436c-21db-11ea-a2cd-8b5be4829329 172.24.8.1 172.24.8.200 1 0 1 mysecret 0 fr

path_novnc="/opt/noVNC"
tmp_dir="/tmp/portailvms/"
tmp_sock_dir="/$tmp_dir/sock"
tmp_lock_dir="/$tmp_dir/lock"
account_vnc="novnc"
account_kvm="vmunpriv"
account_portail="portailvmsd"
pem_file="$path_novnc/pem/novnc.pem"
br_interface="br0"

current_script="$(readlink -f "$0")"
dir_source_script="$(dirname $current_script)/sources/"

source $dir_source_script/functions.sh
source $dir_source_script/kvm_common.sh

if (( $# < 15 )); then
    error "argument(s) missing"
fi


name="${1}"
id="${2}"
tmp_target_dir="${3}"
ram="${4}"
cpu="${5}"
source="${6}"
uuid="${7}"
from_address="${8}"
to_address="${9}"
net_virtio="${10}"
scsi_virtio="${11}"
with_vnc="${12}"
vnc_password="${13}"
guest_type="${14}"
keyboard="${15}"

if [[ ! -d "$tmp_target_dir"  ]] ; then
    error "Directory '$tmp_target_dir' doesn't exist"
fi

if [[ ! -d "$tmp_sock_dir"  ]] ; then
    error "Directory '$tmp_sock_dir' doesn't exist"
fi

if ! isNumeric $id; then
    error "'id' must be a number"
fi

if ! isNumeric $ram; then
    error "'ram' must be a number"
fi

if ! isNumeric $cpu; then
    error "'cpu' must be a number"
fi

if ! isNumeric $net_virtio; then
    error "'net_virtio' must be a number"
fi

if ! isNumeric $scsi_virtio; then
    error "'scsi_virtio' must be a number"
fi

if ! isNumeric $with_vnc; then
    error "'with_vnc' must be a number"
fi

check_uuid "$uuid" || error "'UUID' is not valid"
check_ip "$from_address" || error " IP address '$from_address' is not valid"
check_ip "$to_address" || error " IP address '$to_address' is not valid"
check_password "$vnc_password" || error " Password is not valid"

if ! isNumeric $guest_type; then
    error "'guest_type' must be a number"
fi

echo -e "\t name=$name
\t id=$id
\t tmp_target_dir=$tmp_target_dir
\t tmp_sock_dir=$tmp_sock_dir
\t ram=$ram
\t cpu=$cpu
\t source=$source
\t uid=$uuid
\t from_address=$from_address
\t to_address=$to_address
\t net_virtio=$net_virtio
\t scsi_virtio=$scsi_virtio
\t with_vnc=$with_vnc
\t vnc_password=$vnc_password
\t guest_type=$guest_type
\t keyboard=$keyboard
"

target="$tmp_target_dir/$uuid.qcow2"
sock_filename="$tmp_sock_dir/sock_${uuid}"

# CoW
cp --reflink=always  $source $target || error "Impossible to copy file ($source -> $target)"

# fix right
# for tmp qcow2 file
chown $account_kvm:root $target || error "Impossible to change owner of $target"
chmod u=rwx,g=rwx,o= $target || error "Impossible to chmod : $target"


cmd="$(makeKvmCommand ${name} ${id} ${guest_type} ${with_vnc} ${ram} ${cpu} ${net_virtio} ${scsi_virtio} ${target} ${sock_filename} ${keyboard})"

echo $cmd

# add iptables rules
source $dir_source_script/iptables_add_rules.sh

if (( $with_vnc )); then
    vnc_target="127.0.0.1:$((5900 + ${id#0}))"
    novnc_port="$((6900 + ${id#0}))"
    # start vnc proxy web
    source $dir_source_script/start_vnc_service.sh
    # add iptables rules for novnc
    source $dir_source_script/iptables_vnc_add_rules.sh
fi

# create tap interface and change owner
if ! ip link show tap${id} >/dev/null 2>&1; then
    ip tuntap add tap${id} mode tap user $account_kvm || warning "Impossible to create tap interface 'tap${id}', or change owner"
fi

# add tap interface to bridge
if [ -z "$(bridge vlan show $br_interface | grep -E "^\s*tap${id}\s")" ] ; then
    ip link set dev tap${id} master $br_interface || warning "Impossible to add tap interface 'tap${id}' to bridge '$br_interface'"
fi

# up interface
ip link set dev tap${id} up || warning "Impossible to up tap interface 'tap${id}'"

# intercept signal
trap 'echo "try to stop all jobs ..." ; kill $(jobs -p)' SIGINT SIGTERM SIGQUIT

# run kvm as 'kvm' user, in background
$cmd -runas $account_kvm &
current_job=$!

source $dir_source_script/post_startkvm.sh
# wait qemu/kvm
wait $current_job

sleep 0.3
if [ -n "$(ps -edf | grep -v grep | grep $current_job)" ]; then
    warning "KVM is alive : send SIGKILL !"
    kill -9 $current_job > /dev/null 2>&1
fi

# remove the tap interface from the bridge
ip link set dev "tap${id}" nomaster || warning "Impossible to remove the tap interface 'tap${id}' from the bridge $br_interface'"

# remove tap interface
ip link delete "tap${id}" || warning "Impossible to delete tap interface 'tap${id}'"

# remove iptables rules
source $dir_source_script/iptables_remove_rules.sh

if (( $with_vnc )); then
    # stop vnc proxy web
    source $dir_source_script/stop_vnc_service.sh
    # remove iptables rules for novnc
    source $dir_source_script/iptables_vnc_remove_rules.sh
fi

rm -f "$target" || warning "Impossible to remove tmp files : $target"
rm -f "$sock_filename" || warning "Impossible to remove socket unix files : $sock_filename"
