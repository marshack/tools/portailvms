#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

lxd_cmd="lxc --force-local"

function error {
    clear
    echo "$1" >&2
    sleep 3
    exit 1
}

function check_password {
    if [[ $1 =~ ^[A-Za-z0-9]{1,25}$ ]]; then
        return 0
    fi
    return 1
}

function isContainerAlive {
    if [ -n "$($lxd_cmd list --format csv $1)" ] ; then
        return 0
    fi
    return 1
}

if (( $# < 2 )); then
     error "argument(s) missing"
fi


hostname_container="${1}"
auth_arg="${2}"

check_password "$auth_arg" || error " Password is not valid"

if [[ "$password" != "$auth_arg" ]]; then
    error "Authentication failed"
fi

# Loop, in order to manage lxc restart
while isContainerAlive ${hostname_container}; do
    $lxd_cmd exec ${hostname_container} sh
    echo -e "\nExit. Attempting to reconnect ..."
    sleep 3
    clear
done

#clear
echo -e "\n${hostname_container} stopped. Bye"

