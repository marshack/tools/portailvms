#!/bin/bash
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

function error {
    clear
    echo "$1" >&2
    sleep 3
    exit 1
}

function check_password {
    if [[ $1 =~ ^[A-Za-z0-9]{1,25}$ ]]; then
        return 0
    fi
    return 1
}

function isContainerAlive {
    if [ -n "$(lxc-ls -P ${containers_path} --line --filter="^${1}$")" ] ; then
        return 0
    fi
    return 1
}

if (( $# < 2 )); then
     error "argument(s) missing"
fi


if [ -z "$containers_path" ]; then
    error "'containers_path' no set"
fi


if [[ ! -d "$containers_path"  ]] ; then
    error "Directory '$containers_path' doesn't exist"
fi

hostname_container="${1}"
auth_arg="${2}"

check_password "$auth_arg" || error " Password is not valid"

if [[ "$password" != "$auth_arg" ]]; then
    error "Authentication failed"
fi

# required for systemd/dbus
export XDG_RUNTIME_DIR="/run/user/$(id -u)"
export DBUS_SESSION_BUS_ADDRESS="unix:path=${XDG_RUNTIME_DIR}/bus"

# Loop, in order to manage lxc restart
while isContainerAlive ${hostname_container}; do
    if [ -S ${XDG_RUNTIME_DIR}/bus ]; then
        /usr/bin/systemd-run --quiet --user --scope -p "Delegate=yes" lxc-attach -P ${containers_path} --clear-env -q ${hostname_container}
        echo -e "\nExit. Attempting to reconnect ..."
        sleep 3
        clear
    else
        error "An error occured, please contact an Administrator"
    fi
done

clear
echo -e "\n${hostname_container} stopped. Bye"

