#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Written by erable <erable@marshack.fr>
#       Etat francais / Armee de l'Air et de l'Espace / BA118
# Portions created by the Initial Developer are Copyright (C)

import sys, os, signal, subprocess
import configparser, argparse
import base64
import threading

from common.globals import __version__
from common.globals import getConfigValue, createDirIfNotExist
from common.logs import Logs

from include.server import Server
from include.route import Route

# default values
config_app={
    'debug':False,
    'tmp_socketvms_dir':"/tmp/portailvms/sock/",
    'origin_dir': '/serve/origin/',
    'tmp_target_dir':"/serve/tmp/",
    'iso_dir':"/serve/ISO/",
    'lxc_dir':'/serve/portailvmsd/lxc/',
    'archi_templates_dir' :'/serve/archimulator/templates/',
    'archi_kvm_dir':'/serve/archimulator/kvm/',
    'upload_dir': '/serve/upload/',
    'secret':None,
    # logs
    'log_launcher_filename':None,
    'maxbytes':150000,
    'backupcount':9
}

class Main:
    def __init__(self, config):
        self.logging=None
        self.server=None
        self.config=config
        self.debug_enabled=self.config['debug']
        self.rcv_queue=[{'success': True, 'uuid': 'MAINTENANCE', 'origin': 'initialize', 'maintenance': False}]
        self.lockSend=threading.Lock()

    def debug(self, msg):
        if self.debug_enabled:
            print (str(msg))

    def logInfo(self, msg):
        if self.logging:
            self.logging.logInfo(msg)

    def logWarning(self, msg):
        if self.logging:
            self.logging.logWarning(msg)


    def sendFromFifo(self):
        self.lockSend.acquire()
        while len(self.rcv_queue) and self.server:
            try:
                data=self.rcv_queue.pop()
                if not self.server.sendData(data): raise Exception("Impossible to send data")
                self.logInfo(f"SEND: {data}")
            except:
                self.debug("Impossible to send data, retry later ...")
                self.rcv_queue.append(data)
                break
        self.lockSend.release()

    def callback_proc_finished(self, data):
        # print(f"callback_proc_finished :\n{data}")
        self.lockSend.acquire()
        self.rcv_queue.insert(0, data)
        self.lockSend.release()
        self.sendFromFifo()

    def callback_data_rcv(self, data):
        self.logInfo(f"RCV: {data}")
        self.route.launch(data)

    def execute(self, cmd):
        self.debug('Main:execute - {}'.format(" ".join(cmd)))
        try:
            proc=subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except Exception as e:
            self.debug(str(e))
            return False
        returncode=proc.wait()
        if returncode==0:
            return True
        else:
            if proc.stderr:
                self.debug(proc.stderr.read().decode().split('\n'))
            return False

    def initialise(self):
        self.debug("Main:initialise")
        self.execute(("scripts/initialise.sh", self.config['tmp_target_dir']))

    def prune(self):
        self.debug("Main:prune")
        self.execute(("scripts/prune.sh", self.config['tmp_target_dir'], self.config['upload_dir'], self.config['lxc_dir']))

    def run(self):
        sock_filename = "/{}/launcher.sock:root:portailvmsd:660".format(self.config['tmp_socketvms_dir'])
        self.server=Server(sock_filename, config_app['secret'],debug_enabled=self.debug_enabled)

        self.logging=Logs("pvms-launcher", self.config['log_launcher_filename'], self.config['maxbytes'], self.config['backupcount'])
        self.server.setCallBackRcvData(self.callback_data_rcv)
        self.server.setCallBackLogInfo(self.logInfo)
        self.server.setCallBackLogWarning(self.logWarning)
        self.server.setCallBackConnected(self.sendFromFifo) # sends pending messages (if there are any)

        self.route=Route(self.config, callback_proc_finished=self.callback_proc_finished, debug_enabled=self.debug_enabled)
    
        # initialise (iptables rules, ipset rules, ...)
        self.initialise()
        # start server
        self.server.run()
        # destroy all (vm, lxc, iptables rules, ipset rules, ...)
        self.prune()


    def signal_handler(self, signal, frame):
        self.route.stop()
        if self.server:
            self.server.stop()

if __name__ == '__main__':
    filename='/etc/portailvms/main.conf'
    if not os.path.isfile(filename):
        sys.stderr.write("Error - Configuration file doesn't exist : {}\n".format(filename))
        sys.exit(1)

    # read configuration file
    config = configparser.ConfigParser()
    try:
        config.read(filename)
        # DEFAULT section
        secret=getConfigValue(config,"DEFAULT", "secret", config_app['secret'], str)
        config_app['secret']=base64.b64decode(secret)
        # VM section
        config_app['origin_dir']=getConfigValue(config,"VM", "origin_dir", config_app['origin_dir'], str)
        config_app['tmp_target_dir']=getConfigValue(config,"VM", "tmp_target_dir", config_app['tmp_target_dir'], str)
        config_app['iso_dir']=getConfigValue(config,"VM", "iso_dir", config_app['iso_dir'], str)
        # Archi section
        config_app['archi_templates_dir']=getConfigValue(config,"ARCHI", "templates_dir", config_app['archi_templates_dir'], str)
        config_app['archi_kvm_dir']=getConfigValue(config,"ARCHI", "kvm_dir", config_app['archi_kvm_dir'], str)
        # LXC section
        config_app['lxc_dir']=getConfigValue(config,"LXC", "lxc_dir", config_app['lxc_dir'], str)
        # UPLOAD section
        config_app['upload_dir']=getConfigValue(config,"UPLOAD", "upload_dir", config_app['upload_dir'], str)
        # LOGGING section
        config_app['log_launcher_filename']=getConfigValue(config,"LOGGING", "log_launcher_filename",config_app['log_launcher_filename'], str)
        config_app['maxbytes']=getConfigValue(config,"LOGGING", "maxbytes",config_app['maxbytes'], int)
        config_app['backupcount']=getConfigValue(config,"LOGGING", "backupcount",config_app['backupcount'], int)
    except:
        sys.stderr.write("Error - Impossible to parse configuration file : {}\n".format(filename))
        sys.exit(1)

    createDirIfNotExist(config_app['tmp_socketvms_dir'])

    # Parse Args
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--version', action='version', \
        help="show version", 
        version='%(prog)s version : {version}'.format(version=__version__))
    parser.add_argument("--debug", help="Debug", action="store_true")
    args=parser.parse_args()
    config_app['debug']=args.debug

    main=Main(config_app)

    #  intercept the signals (kill)
    signal.signal(signal.SIGINT, main.signal_handler)
    signal.signal(signal.SIGTERM, main.signal_handler)
    signal.signal(signal.SIGQUIT, main.signal_handler)

    main.run()

