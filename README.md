_This program is licensed to you under the terms of the GNU General Public License version 3.0 or later_

# Présentation

PortailVMS est un outil permettant de créer/gérer/détruire des machines virtuelles, containers ou architectures **jetables**. Il s’appuie pour cela sur un système de fichiers compatible **CoW** (*Copy on Write*), de **LXC**, **Docker**, et du couple **KVM/Qemu**. L'outil expose une interface web au profit des utilisateurs et administrateurs.

Chaque environnement virtuel jetable est exécuté dans son propre "bac à sable" (*sandbox*).

## Architecture

![](docs/images/architecture.png)

Deux services sont utilisés :
 - **portailvmsd-web** : Portail web
 - **portailvmsd-launcher** : Démarrage/Arrêt des environnements virtuels

## Aperçu de l'interface web

![](docs/images/screenshot1.png)

![](docs/images/screenshot2.png)

![](docs/images/screenshot3.png)

![](docs/images/screenshot4.png)


# Installation

> PortailVMS a été testé sur Ubuntu 22.04, debian 11 et debian 12

```bash
sudo apt install iproute2 ipset psmisc rsync python3 python3-cherrypy3 python3-passlib python3-cryptography python3-lxc python3-pylxd python3-docker python3-psutil python3-magic python3-yaml make socat
```


### Créer le compte système pour le portail web
```bash
sudo groupadd -r portailvmsd
sudo useradd -rNM portailvmsd -G shadow -g portailvmsd -d /serve/portailvmsd -s /sbin/nologin -c "PortailVms system account"
sudo passwd -l portailvmsd
```

# Configuration du réseau

Un bridge est nécessaire, afin de pouvoir y raccorder les environnements virtuels. **Il est important qu'il se nomme 'br0'**.

## Netplan

*Netplan* est un utilitaire qui permet de configurer facilement le réseau sous Linux (mais vous pouvez utiliser un autre utilitaire si vous le souhaitez : *Network Manager*, *Systemd*, ...).

### Installation
```bash
sudo apt install netplan.io
```

### Configuration

Exemple de configuration (fichier */etc/netplan/01-portailvms.yaml*) :

```yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    eno1:
      dhcp4: false
      dhcp6: false

  bridges:
    br0:
      interfaces: [eno1]
      addresses: [172.24.8.1/24]
      gateway4: 172.24.8.254
      nameservers:
        addresses: [172.24.8.253]
      dhcp4: false
      dhcp6: false
```

Appliquer :

```bash
netplan generate
netplan apply
```

Vérification :

```bash
networkctl status br0
ip route
ip addr list
```
**Voir la section problèmes connus**, en cas de dysfonctionnement du bridge.

## Serveur DHCP

Un serveur DHCP est requis, afin de pouvoir attribuer des adresses IP aux machines virtuelles et containers. Les adresses MAC sont attribuées par le portailVMS, il faut associer une adresse MAC à une adresse IP (DHCP statique).

La configuration est à adapter à votre environnement.


Installation du service :

```bash
sudo apt install isc-dhcp-server
```

Éditer le fichier */etc/default/isc-dhcp-server* :

```
INTERFACESv4="br0"
```

Éditer le fichier de configuration */etc/dhcp/dhcpd.conf* :

```
option domain-name "marshack.internal";
option domain-name-servers marshack.internal;

subnet 172.24.8.0 netmask 255.255.255.0 {
  range 172.24.8.200 172.24.8.245;
  option routers 172.24.8.254;
  option subnet-mask 255.255.255.0;
  option domain-name-servers 172.24.50.253;
  deny unknown-clients;
}

host host-02-00-00-00-00-00 {
         hardware ethernet 02:00:00:00:00:00;
         fixed-address 172.24.8.200;
}

host host-02-00-00-00-00-01 {
         hardware ethernet 02:00:00:00:00:01;
         fixed-address 172.24.8.201;
}

# ...
```

Pour générer une partie de la configuration, on peut s'aider d'un script écrit en python3 :
```python
#!/usr/bin/python3
ipstart="172.24.8.200"
for c in range(0,50):
    ip=ipstart.split('.')
    ip[3]=str(int(ip[3])+c)
    txt="""
host host-02-00-00-00-00-{0:02x} {{
    hardware ethernet 02:00:00:00:00:{0:02x};
    fixed-address {1};
}}
    """.format(c, '.'.join(ip))
    print (txt)

```


Redémarrer le service :

```bash
sudo systemctl restart isc-dhcp-server
```

# Système de fichiers avec capacité 'CoW'

Le système de fichiers contenant les images disques au format *qcow2*, doit avoir la capacité de gérer la *"copie légère"* (CoW = *Copy on Write*).

**Important :** Afin d'utiliser *CoW* avec KVM, LXC et Docker, il est nécessaire d'utiliser le système de fichiers **btrFS**.


## btrFS

**btrFS** est le système de fichiers **requis**.

**Avertissements :**
 - **btrFS* est toujours en cours de développement (https://btrfs.readthedocs.io/en/latest/Status.html)
 - Il est **fortement déconseillé** d'utiliser le pilote **virtio-scsi** avec *btrFS*
 - Il est déconseillé d'activer la compression sur la partition *btrFS* avec *PortailVMS*, pour des questions de performances

Exemple d'installation et de configuration type, pour un système de fichiers *btrFS* (à adapter à votre environnement).

```bash
sudo apt install btrfs-progs

sudo mkfs.btrfs /dev/sdc1
sudo mkdir /serve
sudo chmod 755 /serve
```

Ajouter dans le fichier */etc/fstab* :
```
/dev/sdc1                     /serve         btrfs   defaults,user_subvol_rm_allowed,nodev 0     0
```

Montage du système de fichiers :
```bash
sudo mount /serve/
```

création des sous-volumes :
```bash
sudo btrfs subvolume create /serve/origin
sudo btrfs subvolume create /serve/tmp
sudo btrfs subvolume create /serve/ISO
sudo btrfs subvolume create /serve/upload
sudo btrfs subvolume list /serve
```

**Voir la section problèmes connus**, notamment en ce qui concerne la **fragmentation** et les pilotes **Virtio** (les pilotes *virtio* permettent d'accélérer les accès disques, réseaux, ... entre la machine virtuelle et la machine hôte).

# Virtualisation vs conteneurisation

*PortailVMS* peut fonctionner avec les moteurs suivants :

## Virtualisation

 - KVM/Qemu

## Conteneurisation

 - LXC
 - Docker
 - LXD/LXC (**déprécié**)

## Hybride

  - Archimulator (création d'environnements réseau et système : machines virtuelles/containers, vlans, routage, services, ...)

Ces moteurs sont à installer, en fonction de vos besoins (voir ci-dessous).

| Moteurs                               | Fonctions                                                                     |
|---------------------------------------|-------------------------------------------------------------------------------|
| [kvm](docs/kvm.md)                    | Démarrer une machine virtuelle jetable à partir d'une image                   |
| [lxc](docs/lxc.md)                    | Démarrer un container lxc jetable à partir d'une image                        |
| [lxd](docs/lxd.md)                    | Démarrer un container lxd jetable à partir d'une image                        |
| [docker](docs/docker.md)              | Démarrer un container docker jetable à partir d'une image                     |
| [archimulator](docs/archimulator.md)  | Démarrer une architecture complète et jetable (réseau, machines, vlan, ...)   |


# PortailVMS

PortailVMS, est écrit en *Python3* et *Bash*, et est l'interface utilisateur permettant de démarrer/gérer/détruire les environnements virtuels jetables (aussi appelé "instance").

## Installation

### Installer le programme, la base de données, le fichier de configuration et activer les services :

```bash
sudo make install
```

Pour n'installer que le fichier de configuration et une base de données vierge :

```bash
sudo make install_configuration
```

D'autres cibles sont disponibles, voir le fichier Makefile.


Adapter la configuration dans le fichier `/etc/portailvms/main.conf`.


### Pour (re)démarrer les services :

```bash
sudo systemctl restart portailvmsd-launcher.service
sudo systemctl restart portailvmsd-web.service
```


### Pour démarrer les programmes, en premier plan :

```bash
# le lanceur
sudo sh -c "(cd /opt/portailvms/launcher-service && python3 portailvmsd-launcher)"
# le portail web
sudo -u portailvmsd sh -c "(cd /opt/portailvms/web-service && python3 portailvmsd-web)"
```

### S'authentifier sur le portail

Par défaut, le portail est accessible en http sur le port 8080. Pour l'administrer, il est nécessaire d'avoir un compte utilisateur système. Cet utilisateur doit faire parti du groupe '*adm*' (voir fichier de configuration *main.conf*).

Afin de pouvoir utiliser le portail, les utilisateurs doivent :

 - soit faire parti du groupe système '*users*' (voir fichier de configuration *main.conf*),
 - soit avoir un compte de domaine valide (voir section LDAP ci-dessous),
 - soit doivent s'authentifier à l'aide d'un jeton via l'API (*/authenticate/token*). Le jeton est signé à l'aide de la clé présente dans le fichier de configuration de l'application. La durée de vie du jeton est limitée dans le temps, de plus il est à usage unique. Voir la section développeur pour la structure du jeton.

### Authentification LDAP/Active Directory

Installer :
```bash
sudo apt install python3-ldap
```
Renseigner ensuite la section **[LDAP]** dans le fichier de configuration *main.conf*.

Exemple avec *Active Directory* :
```ini
[LDAP]
ldap_url=ldaps://ldap.example:636
ldap_base_dn=ou=UserUnits,dc=my,dc=company,dc=com
ldap_search=sAMAccountName={username}
ldap_unique_id_field=cn
ldap_username=bind_account
ldap_credential=Password
ldap_ca_certfile=/etc/ssl/certs/ca-certificates.crt
```

Exemple avec *OpenLDAP* :
```ini
[LDAP]
ldap_url=ldaps://ldap.example:636
ldap_base_dn=ou=UserUnits,dc=my,dc=company,dc=com
ldap_search=uid={username}
ldap_unique_id_field=uid
ldap_username=cn=bind_account,ou=admin,dc=my,dc=company,dc=com
ldap_credential=Password
```

## Base de données

La base de données, est de type sqlite : */etc/portailvms/db/database.sqlite3*

Elle contient les tables suivantes :

 - *challenges* : Liste des challenges, avec leurs caractéristiques (nom, ressources nécessaires, fichier disque source, ...)
 - *current_vm* : Liste des environnements virtuels jetables existants (ou ayant existé) avec leurs caractéristiques (uuid, heure de démarrage et d'arrêt, propriétaire, ...)
 - *teams* : Liste des équipes (table peuplée par portailVMS)
 - *stats* : Statistiques


### Structure de la table 'challenges'

 - **id**	: index
 - **name** : Nom de l'environnement virtuel
 - **guest_type** : Type de machine invitée, et moteur de virtualisation
   - 0 : Moteur KVM (par défaut, convient dans la plupart des cas)
   - 1 : Moteur KVM pour une machine invitée Windows (HyperV activé)
   - 8 : Identique à 0, mais utilisation d'UEFI (menu non accessible)
   - 9 : Identique à 0, mais utilisation d'UEFI avec menu accessible
   - 16 : Moteur KVM, avec le CPU invité = CPU hôte
   - 32 : Moteur Docker
   - 64 : Moteur LXC
   - 128 : Moteur LXD
   - 256 : Moteur Archimulator
 - **file** :
   - si moteur KVM : Nom du fichier disque au format *qcow2*, se trouvant dans le dossier */serve/origin/*
   - si moteur LXD, LXC ou Docker : Nom de l'image (ex: apache2)
   - si moteur Archimulator : Nom du fichier décrivant l'architecture (format yaml)
 - **ram**	: Quantité de mémoire à réserver/allouer (en Mo)
 - **cpu**	: Nombre de CPU à allouer (moteur KVM, Docker, LXC et LXD)
 - **vol_size** : Quantité d'espace disque à réserver/allouer (en Mo)
 - **lifetime** : Durée de vie. 0 = durée illimitée, sinon saisir la durée en secondes
 - **enable**: Permet de rendre accessible l'environnement aux utilisateurs. Il le sera toujours pour les administrateurs
 - **vnc**	: Activer le déport de la console (0 = *false*, 1 = *true*)

 Les champs suivants ne sont utilisés qu'avec le moteur KVM :
 - **net_virtio**	: Activer *Virtio* pour la carte réseau de cette machine (0 = *false*, 1 = *true*)
 - **scsi_virtio**	: Activer *Virtio* pour l'accès au périphérique de stockage de cette machine (0 = *false*, 1 = *true*). **Déconseillé avec le système de fichiers btrFS**
 - **mount_iso** : Autoriser le montage de fichier ISO (CDROM) pour cette machine (0 = *false*, 1 = *true*)
 - **iso_path** : Chemin contenant les fichiers au format ISO, pour ce challenge

## Journaux d'évènements

Les journaux peuvent être visualisés au "fil de l'eau" :

 - Journal de portailvmsd-web : `sudo tail -F /var/log/portailvmsd/portailvmsd_web.log`
 - Journal de portailvmsd-launcher : `sudo tail -F /var/log/portailvmsd/portailvmsd_launcher.log`
 - Journal des requêtes HTTP : `sudo tail -F /var/log/portailvmsd/http_requests.log`
 - Journal du serveur cherrypy : `sudo tail -F /var/log/portailvmsd/http_service.log`
 - Journal des requêtes SQL : `sudo tail -F /var/log/portailvmsd/sql_requests.log`

## Pare-feu

**IMPORTANT** : Pour que le filtrage iptables fonctionne pour les *bridges*, le module **br_netfilter** doit être chargé en mémoire. Si ce n'est pas le cas :  `sudo modprobe br_netfilter`

Créer le fichier  */etc/modules-load.d/bridge.conf*, et y ajouter `br_netfilter`, afin que le module soit chargé au démarrage.

Créer également le fichier */etc/sysctl.d/80-bridge.conf*, et y ajouter :
```
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
```

Le portail ajoute automatiquement les règles de pare-feu pour que chaque instance virtuelle puissent être joignable, notamment par l'utilisateur l'ayant démarré. Si vous souhaitez ajouter vos propres règles, utilisez les chaines `VMS-USER-INPUT` et `VMS-USER-OUTPUT`. Exemple :

```
# regles VMS utilisateur en entree
iptables -A VMS-USER-INPUT -p icmp --icmp-type 8 -j ACCEPT
iptables -A VMS-USER-INPUT -j DROP

# regles VMS utilisateur en sortie
iptables -A VMS-USER-OUTPUT -p icmp --icmp-type 0 -j ACCEPT
iptables -A VMS-USER-OUTPUT -j DROP
```

### Fonctionnement d'Iptables avec un bridge

![](docs/images/bridge2b.png)

- Des échanges entre deux machines virtuelles raccordées sur le même *bridge* : Les paquets vont transiter dans la chaine **FORWARD** (requête et réponse),
- Des échanges entre une machine virtuelle raccordée sur le *bridge*, et une machine distante : Les paquets vont transiter dans la chaine **FORWARD**,
- Des échanges entre une machine virtuelle raccordée sur le *bridge*, et un service tournant sur la machine hôte : Les paquets vont transiter dans les chaines **INPUT** et **OUTPUT**

## Durcissement

Il est possible de confiner le portail, à l'aide de **appArmor** :

```bash
sudo apt install apparmor-utils
sudo make enable_apparmor
```

## Section Développeur

Pour exécuter en mode *debug*, ajouter l'option `--debug`.

Si vous souhaitez plus de verbosité, créer la variable d'environnement `PORTAILVMS_DEBUG_VERBOSE`, en la positionnant à **1** (verbosité moyenne) ou **2** (verbosité maximale).

### Exemple pour le "launcher"

```bash
cd /opt/portailvms/launcher-service/
sudo ./main.py --debug
# + verbeux
sudo PORTAILVMS_DEBUG_VERBOSE=1 ./main.py --debug
# verbosité maximale
sudo PORTAILVMS_DEBUG_VERBOSE=2 ./main.py --debug
```

### Exemple pour le portail web
```bash
sudo -u portailvmsd sh -c "(cd /opt/portailvms/web-service && python3 portailvmsd-web --debug)"
# verbosité maximale (avec requêtes SQL)
sudo -u portailvmsd sh -c "(cd /opt/portailvms/web-service && PORTAILVMS_DEBUG_VERBOSE=2 python3 portailvmsd-web --debug)"
```

### Structure du jeton

Les jetons envoyés via une méthode GET ou POST, sont encodés en base64. Actuellement, deux types de jeton :
 - dans la variable *ta* (jeton authentifiant)
 - dans la variable *tr* (jeton anti-rejeu).

Une fois décodé, voici sa structure :

```python
    bytes salt[6];
    bytes hmac_sha1[20];
    bytes * data;
```

Exemple <http://10.0.2.15:8080/start/1?ta=F2SfxIPUewMFeTsQMtz4FfqKFTqZK4aKpm8xMjA1LzE1NzY5NDE2OTYuNzAzMjI=>

Si l'utilisateur n'est pas déjà authentifié, la requête sera redirigée afin qu'il puisse l'être. **Le jeton est à usage unique et peut avoir une durée de vie limitée dans le temps** (selon comment il a été créé). De plus, il est possible d'activer le chiffrement de la donnée.


## Problèmes connus

### btrFS et fragmentation

Des **latences** ont été constatées lorsque les **fichiers sont fragmentés** sur la partition *btrFS*. Cela peut poser problème lors du démarrage simultané de machines virtuelles ayant la même image source. Les fichiers disques présents dans le dossier `/serve/origin`, doivent être uniques (non dupliqué via *Copy-on-Write*).

Remédiation (peut consommer plus d'espace disque à l'issue):
```bash
sudo btrfs filesystem defragment -rv /serve/origin/
```

### btrFS et virtio

Le système de fichiers **btrFS** est toujours en cours de développement et il est fortement déconseillé de l'utiliser avec le pilote **virtio**. Source : <https://www.linux-kvm.org/page/Tuning_KVM>

Il est tout à fait possible d'installer les pilotes **Virtio** pour le réseau uniquement.

### ESX et mode promiscuité

Si vous virtualisez le *PortailVMS* sur un ESX, l'interface de la machine virtuelle raccordée sur le vSwitch, doit être autorisée à passer en **mode promiscuité**.

### ESX et Bridge

Si vous raccordez un *bridge* sur un autre *bridge* (par exemple un bridge présent dans une VM, raccordé sur un **vSwitch ESX**), il se peut que le bridge dans la machine virtuelle dysfonctionne (problème d'apprentissage des adresses MAC sur le Switch). Source : <http://www.microhowto.info/troubleshooting/troubleshooting_ethernet_bridging_on_linux.html>

- Solution 1 : Raccorder l'interface virtuelle de la VM, directement sur l'interface physique  (*network passthrough*)
- Solution 2 (solution de contournement) : `sudo brctl setageing br0 0`

### LXD et répertoire utilisateur

Si le fichier *~portailvmsd/.config/lxc/cookies* existe, et qu'il n'appartient pas à l'utilisateur *portailvmsd*, il pourrait y avoir des latences à chaque utilisation de la commande *lxc* par *PortailVMS*. Remédiation :
```bash
chown portailvmsd:portailvmsd ~portailvmsd/.config/lxc/ -R
```

### Cache ARP sur switch

*PortailVMS* expose des environnements virtuels jetables, qui peuvent potentiellement exposer de **nombreuses adresses MAC différentes**, en fonction des environnements qui ont été démarrés.

Si le switch a un *timeout* du cache ARP trop long, ce dernier peut ne pas correctement le mettre à jour, si l'environnement virtuel n'envoie aucun paquet.

 - Solution 1 : Forcer les adresses IP et les adresses MAC dans vos squelettes *.yaml*, si vous utilisez Archimulator
 - Solution 2 : Réduire la durée de vie du cache ARP de l'équipement réseau. Exemple pour un Cisco (cache de 5 minutes) :
```
interface GigabitEthernet 1/0/1
arp timeout 300

show interfaces GigabitEthernet 1/0/1 | include ARP
```

### Protocole STP

Si le **protocole STP** est activé sur le *bridge* de la machine hôte, l'interface de l'environnement virtuel ne sera pas accessible tant que la découverte de la nouvelle topologie ne sera pas finalisée (10 à 20 secondes environ).

