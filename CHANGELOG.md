# Changelog

## [1.3.4] - 2024-10-17

 - LXD est déprécié dans PortailVMS
 - corrections de bugs/améliorations

## [1.3.3] - 2024-03-08

 - Possibilité de configurer la disposition du clavier pour les KVM
 - Amélioration des scripts de conversion des containers Docker vers LXC
 - Amélioration de l'affichage des erreurs (au profit des administrateurs)
 - De nombreuses corrections de bugs/améliorations

## [1.3.2] - 2023-06-13

 - Mise à jour de maintenance : Corrections de bugs et amélioration de la sécurité

## [1.3.1] - 2023-05-19

 - Possibilité d'afficher une console tty pour les moteurs LXC, LXD et Docker
 - Il est maintenant possible de modifier une image LXC
 - Nouveau formulaire de téléversement de fichiers (visualisation de la progression)
 - Le masque du réseau des machines virtuelles jetables n'est plus limité à 24 bits
 - Ne proposer de démarrer que les challenges qui ont un moteur de virtualisation/containérisation disponible (KVM, LXC, LXD, ...)
 - De nombreuses corrections de bugs/améliorations


## [1.3.0] - 2023-02-02

 - Premier commit (ouverture du code)

